//
//  Page6.m
//  Little Mermaid
//
//  Created by Pipedream on 01/09/12.
//
//

#import "Page6.h"

@implementation Page6

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page6 *layer = [Page6 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}



-(void)moveCloud:(int)tag{
    id move_Clouds = [CCMoveBy actionWithDuration:10 position:CGPointMake(200, 0)];
    id action = [CCRepeatForever actionWithAction:move_Clouds];
    [[self getChildByTag:tag] runAction:action];
    
}

-(void)waterripple{
    id waves = [CCWaves actionWithDuration:10 size:ccg(24, 32) waves:5 amplitude:5 horizontal:NO vertical:YES];
      [[self getChildByTag:7] runAction: [CCRepeatForever actionWithAction: waves]];
}


-(void)animateThunderStorm{
    
    id show = [CCFadeTo actionWithDuration:0 opacity:255];
    id hide = [CCFadeTo actionWithDuration:0 opacity:0];
    
    NSMutableArray *runFrames1 = [NSMutableArray array];
   
		for(int i = 1; i <= 12; i++) {
		[runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"tbolt%i.png", i]]];
	}
      
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.2]];
    [[self getChildByTag:25]  runAction:[CCSequence actions:show, forward, hide, nil]];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"P7_1_Thunder.mp3"];
       
}


-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:6])) {
          
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    [self moveCloud:4];
    [self waterripple];
    
    world->SetGravity(b2Vec2(0.0,-9.8));
       
}

-(void) tick: (ccTime) dt{
    [super tick:dt];
}

-(void)animateSun
{
        if ([[self getChildByTag:1] numberOfRunningActions]>0) {
            return;
        }
        id sun_set = [CCMoveTo actionWithDuration:4 position:ccp(627,500)];
        id sun_rise = [CCMoveTo actionWithDuration:4 position:ccp(627,640)];
        
        //   flag=0;
        
        id action = [CCSequence actions:sun_set,sun_rise,nil];
        
        [[self getChildByTag:1] runAction:action];
  

}

-(void) animateSky
{
    NSMutableArray *runFrames1 = [NSMutableArray array];
    for(int i = 2; i <= 6; i++) {
        [runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"sky%i.png", i]]];
    }
    id anim = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.3]];
//    id animate_sky = [CCSpeed actionWithAction:forward2 speed:2.4];
    [[self getChildByTag:0]  runAction:[CCSequence actions:anim,[anim reverse],nil]];
    
    [self animateThunderStorm];
}


-(void) popObjects
{  //Ground Touched
    short sequence=rand()%4;
    NSString *Objects[4] = {@"crab.png",@"crab1.png",@"jevel.png",@"star.png"};
    CCSprite * random_sprite = [CCSprite spriteWithSpriteFrameName:Objects[sequence]];
    
    random_sprite.position=location;
    random_sprite.scale = 0;
    
    [self addChild:random_sprite z:5];
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id gap = [CCActionInterval actionWithDuration:1];
    id hide = [CCScaleTo actionWithDuration:0.2 scale:0];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)random_sprite];
    
    id pop_sprite = [CCSequence actions:scaleup, scaledown, gap, hide, remove, nil];
    [random_sprite runAction:pop_sprite];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P7_4_Hidden pictures appears.mp3"];
   
}


-(void)magicSparkle{
	
//	CCParticleSmoke *leaf_Particles = [[[CCParticleSmoke alloc]init] autorelease] ;
//	leaf_Particles.texture = [[CCSprite spriteWithFile:@"sparkle.png"] texture] ;
//	leaf_Particles.autoRemoveOnFinish = YES;
//	leaf_Particles.position = ccp([self getChildByTag:19].position.x-20,[self getChildByTag:19].position.y+30 );
//    leaf_Particles.posVar = ccp(30,10);
//	leaf_Particles.totalParticles = 50;
//	leaf_Particles.emissionRate = 15;
//    ccColor4F strtcolor = {10,10,10,0};
//    leaf_Particles.startColor = strtcolor;
//	leaf_Particles.life = 1.0;
//	leaf_Particles.lifeVar = 0;
//	leaf_Particles.startSize = 0;
//	leaf_Particles.startSizeVar = 0;
//	leaf_Particles.endSize = 25;
//	leaf_Particles.endSizeVar = 0;
//    leaf_Particles.gravity = ccp(0,0);
//    [leaf_Particles setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
//	[self addChild:leaf_Particles z:9];
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    CCParticleSystemQuad *jevel = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    jevel.tag = 25;
    jevel.texture = [[CCSprite spriteWithFile:[NSString stringWithFormat:@"sparkle.png"]] texture] ;
	jevel.emitterMode = kCCParticleModeGravity;
	jevel.duration=3;
	jevel.startColor = startColor;
	jevel.endColor = startColor;
    jevel.startColorVar = startColorVar;
	jevel.endColorVar = startColorVar;
	jevel.gravity = ccp(-3.0,0);
	jevel.autoRemoveOnFinish = YES;
	jevel.position = ccp([self getChildByTag:19].position.x-30,[self getChildByTag:19].position.y+30 );
	jevel.posVar = ccp(30,30);
	jevel.totalParticles = 50;
	jevel.emissionRate = 50;
	jevel.life = 3;
	jevel.lifeVar = 1;
	jevel.startSize = 10;
	jevel.startSizeVar = 20;
	jevel.endSize = 0;
	jevel.endSizeVar = 5;
    jevel.angle = 40;
    jevel.angleVar =20;
    
	[self addChild:jevel z:8];
    [[SimpleAudioEngine sharedEngine] playEffect:@"P3_2_pearl mussels.mp3"];

    
}


-(void) animateMermaid
{   //mermaid tapped
    
    
    CCSprite * tail = [CCSprite spriteWithSpriteFrameName:@"tail1.png"];
    tail.position=ccp(550,520);
    tail.scale = 0.6;
//    tail.rotation=10.0;
    tail.tag=50;
    
    CGPoint mermaidPosition = [self getChildByTag:19].position;
    
    id hide_mermaid=[CCMoveTo actionWithDuration:0 position:ccp(mermaidPosition.x,mermaidPosition.y+500)];
    id gap = [CCActionInterval actionWithDuration:9];
    id show_mermaid=[CCMoveTo actionWithDuration:0 position:ccp(mermaidPosition.x,mermaidPosition.y)];
  
    [self magicSparkle];
    
    [[self getChildByTag:19] runAction:[CCSequence actions:hide_mermaid,gap,show_mermaid, nil]];
    [self addChild:tail z:7];
    
    id scalein = [CCScaleTo actionWithDuration:7 scale:0];
    id move_tail=[CCMoveTo actionWithDuration:6 position:ccp(320,580)];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)tail];
    id hide_tail = [CCSequence actions:move_tail,remove,nil];
    
    NSMutableArray *runFrames1 = [NSMutableArray array];
    for(int i = 2; i <= 12; i++) {
        [runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"tail%i.png", i]]];
    }
    id anim = [CCRepeat actionWithAction:[CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.1]] times:7];
    
    [tail runAction:anim];
    [tail runAction:hide_tail];
    [tail runAction:scalein];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P7_4_mermaid to the sea.mp3"];
    
}


-(void)awwSound
{
[[SimpleAudioEngine sharedEngine] playEffect:@"P7_3_Hit and Aww coconut.mp3"];

}

-(void)coconutFall:(b2Body*)body
{
    
    body->SetType(b2_dynamicBody);
    if(body->IsFixedRotation())
    {
        id gap1 = [CCActionInterval actionWithDuration:1];
        id rotate_hand=[CCRotateTo actionWithDuration:0.5 angle:-60.0];
        id rotate_back1=[CCRotateBy actionWithDuration:0.2 angle:15.0];
        id rotate_hand1=[CCRotateBy actionWithDuration:0.2 angle:-15.0];
        id rotate_back=[CCRotateBy actionWithDuration:0.5 angle:60.0];
        id action = [CCSequence actions:gap1,rotate_hand,rotate_back1,rotate_hand1,rotate_back1,rotate_hand1,gap1,rotate_back, nil];
        [[self getChildByTag:24] runAction:action];
    }
    
    body->SetFixedRotation(false);
    id place_back = [CCCallFuncND actionWithTarget:self selector:@selector(placecoconuts:data:) data:(b2Body*)body];
    id gap2 = [CCActionInterval actionWithDuration:8];
    [self runAction:[CCSequence actions:gap2,place_back,nil]];
    
    [self performSelector:@selector(awwSound) withObject:nil afterDelay:1.0f];
    
    }




-(void)placecoconuts:(id)dummy data:(b2Body *) body
{
    body->SetType(b2_staticBody);
    int index=((CCSprite *)body->GetUserData()).tag;
    if(index==10)
        body->SetTransform(b2Vec2(195.0/PTM_RATIO, 630.0/PTM_RATIO), 0.0);
    else{
        body->SetTransform(b2Vec2(131.0/PTM_RATIO, 678.0/PTM_RATIO), 0.0);
    }
    body->SetFixedRotation(true);
    
}

-(void)animatePeople{
        id move = [CCMoveBy actionWithDuration:1 position:ccp(60, 10)];
        id moveBack = [CCMoveBy actionWithDuration:1 position:ccp(-60, -10)];
        [[self getChildByTag:12] runAction:[CCSequence actions:move,moveBack, nil]];
   
     [[SimpleAudioEngine sharedEngine] playEffect:@"P7_2_People_talking.mp3"];
}


- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
 
	if (touchedSprite) {
        switch(touchedSprite.tag){
            case 1:    //Sun Touched
                if ([touchedSprite numberOfRunningActions]<= 0) {
                   [self animateSun];
                }
                break;
          
        
            case 0:   //Sky Touched
                if ([touchedSprite numberOfRunningActions]<= 0) {
                    [self animateSky];
                }
                break;
                
            case 6:  //Ground Touched
                if (location.x<820 && location.y>200) {
                    [self popObjects];
                }
               
                break;

            case 19: //mermaid tapped
                if ([touchedSprite numberOfRunningActions]<= 0) {
                    [self animateMermaid];
                    [self animatePeople];
                }
                break;
           
        
            case 10:case 11:
                if (touchedBody->GetType() == b2_staticBody) {
                    [self coconutFall:touchedBody];
                }
                 break;
                
   
     }
    }
    touchedBody = NULL;
    touchedSprite = NULL;
}


- (void) dealloc
{
    
	[super dealloc];
}

@end
