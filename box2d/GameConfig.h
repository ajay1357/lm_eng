//
//  GameConfig.h
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


#ifndef __GAME_CONFIG_H
#define __GAME_CONFIG_H

//CUSTOM SETTINGS

#define PTM_RATIO 32


typedef enum{
	NEXT = 49,
	PRE = 50,
	HOME = 48,
	HELP = 47,
	CREDITS = 46,
    PLAY = -145,
    STOP = -144,
    REPLAY = -146,
    RESUME = -147,
    RESTART = -148,
    BG=-143,
    NARRATEON = 43,
    NARRATEOFF = 42,
    TWEET = 41,
    FB = 42,
    WEB=43,
    GOHOME=199,
    PAGEICON=-313,
    NARATIONOFF = 101,
    NARATIONON = 111,
    TEXTON = 103,
    TEXTOFF = 133,
    CREDITICON = 114,
}TEXT;


//
// Supported Autorotations:
//		None,
//		UIViewController,
//		CCDirector
//
#define kGameAutorotationNone 0
#define kGameAutorotationCCDirector 1
#define kGameAutorotationUIViewController 2

//
// Define here the type of autorotation that you want for your game
//

// 3rd generation and newer devices: Rotate using UIViewController. Rotation should be supported on iPad apps.
// TIP:
// To improve the performance, you should set this value to "kGameAutorotationNone" or "kGameAutorotationCCDirector"
#if defined(__ARM_NEON__) || TARGET_IPHONE_SIMULATOR
#define GAME_AUTOROTATION kGameAutorotationUIViewController

// ARMv6 (1st and 2nd generation devices): Don't rotate. It is very expensive
#elif __arm__
#define GAME_AUTOROTATION kGameAutorotationNone


// Ignore this value on Mac
#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

#else
#error(unknown architecture)
#endif

#endif // __GAME_CONFIG_H

