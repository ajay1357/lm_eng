//
//  HelloWorldLayer.mm
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "Page8.h"
//#import "ControlLayer.h"


// HelloWorldLayer implementation
@implementation Page8
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//ControlLayer *controlls = [[ControlLayer alloc] initWithLayer];
//	[scene addChild:controlls z:5];
	// 'layer' is an autorelease object.
	Page8 *layer = [Page8 node];

	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}


//PAGE 8 BEGINS
-(void)moveHand{
    id rotate = [CCRepeat actionWithAction: [CCSequence actions:[CCRotateBy actionWithDuration:1 angle: -20], [CCRotateBy actionWithDuration:1 angle: 20], nil]times:3];
    [[[self getChildByTag:3] getChildByTag:21] runAction:rotate];
}

-(void)popMermaidBubble:(CGPoint)pos{
    TRBox2D *item;
    item = [[[TRBox2D alloc] initWithSpriteFrameName:@"bubble.png"] autorelease];
    [item setVertexZ:20] ;
    item.position = pos;
    item.scale = 0.5;
    item.tag = 30;
    //		item.flipX = YES;
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true 
              fixedRotation:true
                     bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"bubble"];
    [item body]->SetLinearDamping(0.1);
    [self addChild:item z:20];
    
    b2Vec2 position = b2Vec2(pos.x/PTM_RATIO, touchedSprite.position.y/PTM_RATIO);
    //          [self throwBodies:touched direction:b2Vec2(arc4random()%5,arc4random()%10) position:position tbody:[item body] impulse:b2Vec2((arc4random()%5)-10,(arc4random()%15)+10)];
    [self throwBodies:b2Vec2(1,1) position:position tbody:[item body] impulse:b2Vec2(float(arc4random()%20)-10,(float)(arc4random()%5)+38)];
    
    id scale = [CCScaleTo actionWithDuration:2 scale:1];
    [item runAction:scale];
    
}

-(void)fountain{
    NSMutableArray *runFrames = [NSMutableArray array];
    for(int i = 1; i <= 30; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"fountain%i.png", i]]];
	}
    id animate = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.05]];
    [[self getChildByTag:4] runAction:[CCSpeed actionWithAction:
                                       [CCRepeatForever actionWithAction:animate] speed:1.5]];
    id animate1 = [[animate copy] autorelease];
    [[self getChildByTag:23] runAction:[CCSpeed actionWithAction:
                                        [CCRepeatForever actionWithAction:animate1] speed:1.5]];
}

-(void)carpet{
    id show = [CCFadeIn actionWithDuration:1];
    [[self getChildByTag:6] runAction:show];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P10_1_cartpet slide1.mp3"];
}

//PAGE 8 ENDS

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    
    if(touchedSprite.tag==1 || touchedSprite.tag == 2 )
    {
        [[SimpleAudioEngine sharedEngine] playEffect:@"P10_3_King and Queen.mp3"];
        
    }
    
    [super ccTouchesEnded:touches withEvent:event];
    
  
    
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:8])) {
//        self.scale = 2;
        
		// enable touches
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)waterFountainSound:(ccTime)dt
{
[[SimpleAudioEngine sharedEngine] playEffect:@"P10_Running water sound.mp3"];  

}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    [self fountain];
    [self schedule:@selector(waterFountainSound:) interval:30.0f];
}

-(void) tick: (ccTime) dt{
    [super tick:dt];
    
    BOOL destroyleft = NO;
    BOOL destroyright = NO;
    
    if ([self getChildByTag:16].position.x > 940) {
        destroyright = YES;
    }
    
    if ([self getChildByTag:19].position.x < 120) {
        destroyleft = YES;
    }
    
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
            if (item.tag == 30) {
                if (item.position.x < 55 || item.position.y<80 || item.position.y>715) {
                    [item destroyBodyInWorld:world];
                    [self removeChild:item cleanup:YES];
                }
                
            }
            
            else if (((item.tag >=7 && item.tag <=10) || (item.tag >=15 && item.tag <=18)) && destroyright) {
                NSInteger ind = [self.interections indexOfObject:(CCSprite*)[item body]->GetUserData()];
                if (ind != NSNotFound) {
                    [self.interections removeObjectAtIndex:ind];
                }
                [item destroyBodyInWorld:world];
                [self removeChild:item cleanup:YES];
            
            }else if (((item.tag >=11 && item.tag <=14) || (item.tag >=19 && item.tag <=22)) && destroyleft) {
                NSInteger ind = [self.interections indexOfObject:(CCSprite*)[item body]->GetUserData()];
                if (ind != NSNotFound) {
                    [self.interections removeObjectAtIndex:ind];
                }
                [item destroyBodyInWorld:world];
                [self removeChild:item cleanup:YES];
            }
		}
    }
}


-(void)popAnimal:(int)tag{
    short sequence=rand()%3;
    NSString *Objects[3] = {@"mouse.png",@"Racoon.png",@"Squirrel.png"};
    CCSprite * random_sprite = [CCSprite spriteWithSpriteFrameName:Objects[sequence]];
    
    if(tag==1){
    random_sprite.position=ccp(849, 580);
    }else
    {
     random_sprite.position=ccp(971,355);
    }
    
    random_sprite.scale = 1;
    
    [self addChild:random_sprite z:7];
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.1];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:0.8];
    id gap = [CCActionInterval actionWithDuration:1];
    id hide = [CCScaleTo actionWithDuration:0.2 scale:0];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)random_sprite];
    
    id pop_sprite = [CCSequence actions:scaleup, scaledown, gap, hide, remove, nil];
    [random_sprite runAction:pop_sprite];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page16_3_hidden.mp3"];
}


- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
	if (touchedSprite) {
        switch(touchedSprite.tag){
           
            case 5: {
                 [interections removeObject:touchedSprite];
                 [self carpet];
                 break;
            }
                
            case 3:{if(location.y>100){
            [self popMermaidBubble:CGPointMake(touchedSprite.position.x+60, touchedSprite.position.y+130)];
                [[SimpleAudioEngine sharedEngine] playEffect:@"P10_5_Mermaid Bubble.mp3"];
            }
            
            if ([[[self getChildByTag:3] getChildByTag:21] numberOfRunningActions] > 0) {
                break;
            }
            [self moveHand];
                break;
            
            }
        
           case 30: {
            [self world]->DestroyBody(touchedBody);
            [self removeChild:touchedSprite cleanup:YES];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1 Bubble bursting.mp3"];
               break;
             }
                
            case 25: {
                [self popAnimal:1];
                break;
            }
        
            case 4: case 23: {
            [[SimpleAudioEngine sharedEngine] playEffect:@"P10_Running water sound.mp3"];
            
                break;
            }
        }
        
    }
    else if(location.x>900&&location.y<335&&location.y>255)
    {
        [self popAnimal:2];
    }
    else if(location.x>270&&location.y<700&&location.y>610&&location.x<350)
    {
       [[SimpleAudioEngine sharedEngine] playEffect:@"P10_2_Spears tapped.mp3"];
    }
    else if(location.x<200&&location.y<700&&location.y>610&&location.x>100)
    {
        [[SimpleAudioEngine sharedEngine] playEffect:@"P10_2_Spears tapped.mp3"];
    }

    
    
    touchedSprite = NULL;
    touchedBody=NULL;
    

}


- (void) dealloc
{

	[super dealloc];
}
@end
