//
//  Page21.m
//  Little Mermaid
//
//  Created by Pipedream on 26/09/12.
//
//

#import "Page16.h"

@implementation Page16

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page16 *layer = [Page16 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:16])) {
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)glitter
{
    id fadeout = [CCFadeOut actionWithDuration:0.7];
    id fadein = [CCFadeIn actionWithDuration:0.5];
    
    id glitter_mirror = [CCRepeatForever actionWithAction:[CCSequence actions:fadeout,fadein, nil]];
    
    [[self getChildByTag:5] runAction: glitter_mirror];
}


-(void)eyeblink:(int)parent childTag:(int)child{
    
    id blink = [CCRepeat actionWithAction:[CCBlink actionWithDuration:1 blinks:1] times:5];
    
    id action = [CCRepeatForever actionWithAction:[CCSequence actions:blink,nil]];
    
    if(child < 0 )
    {
        [[self getChildByTag:parent] runAction:action];
    }
    else{
        [[[self getChildByTag:parent] getChildByTag:child] runAction:action];
    }
}

-(void)knifeFall
{
    CCSprite *oldKnife = (CCSprite*)[[self getChildByTag:6] getChildByTag:1] ;
    
    CCSprite *knife = [CCSprite spriteWithSpriteFrameName:@"knife.png"];
    knife.position =  oldKnife.position;
    knife.rotation =  oldKnife.rotation;
        
    id rotateKnife = [CCRotateBy actionWithDuration:1 angle:-40];
    id fallKnife = [CCMoveTo actionWithDuration:1 position:ccp(knife.position.x+50,knife.position.y-250)];
    
    [self addChild:knife z:5];
    
    [oldKnife setVisible:NO];
          
    [knife runAction:rotateKnife];
    [knife runAction:fallKnife];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_2_Rainbow_v1.mp3"];
    

}
-(void)animateMermaid
{
    
    CCSprite *mermaid = (CCSprite *)[self getChildByTag:6];
   
    CGPoint mermaid_position = CGPointMake(mermaid.position.x, mermaid.position.y);
    
    id move = [CCMoveTo actionWithDuration:4 position:ccp(mermaid_position.x,mermaid_position.y-100)];
    id enlarge = [CCScaleTo actionWithDuration:4 scale:1];
    
    [mermaid runAction:move] ;
    [mermaid runAction:enlarge] ;
    
 [[SimpleAudioEngine sharedEngine] playEffect:@"Page19_1_walking_in.mp3"];
    
      
    
}

-(void)glitterKnife
{
CCSprite *knifeShine = (CCSprite *)[[[self getChildByTag:6] getChildByTag:1] getChildByTag:1];
   
    id fadein=[CCFadeIn actionWithDuration:0.6];
    id fadeout=[CCFadeOut actionWithDuration:0.3];
    
    id glow = [CCRepeatForever actionWithAction:[CCSequence actions:fadein,fadeout,nil]];
    
    [knifeShine runAction:glow];

    

}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
  
    [self sparkling:[self getChildByTag:0] from:1 till:10];
    [self eyeblink:6 childTag:2];
    
    [self animateMermaid];
    
    [self glitterKnife];
    
}


-(void)moonGagSound
{
 [[SimpleAudioEngine sharedEngine] playEffect:@"Page19_2_angrymoon.mp3"];
}

-(void)animateMoon
{
    
 NSMutableArray *runFrames = [NSMutableArray array];
    
  for(int i = 1; i <= 31; i++) {
 [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"moon%i.png", i]]];
	}
  id moon_gag = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.2]];
    
    [[self getChildByTag:1] runAction:moon_gag];
    
   
[self performSelector:@selector(moonGagSound) withObject:nil afterDelay:2.0f];

}

-(void) burnCandle
{
    
id waves = [CCWaves actionWithDuration:5 size:ccg(32,24) waves:3 amplitude:1 horizontal:NO vertical:YES];
    [[self getChildByTag:3] runAction: [CCRepeatForever actionWithAction: waves]];
    
 id fadein = [CCFadeIn actionWithDuration:2];
    [[self getChildByTag:3] runAction:fadein];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"Page19_3_fire.mp3"];

}

-(void)popZBody:(CGPoint)pos{
    
    TRBox2D *item, *item1, *item2;
    
    item1 = [[[TRBox2D alloc] initWithSpriteFrameName:@"zzz.png"] autorelease] ;   //Left Z Body
    [item1 setVertexZ:14] ;
    item1.position = pos;
    item1.scale = 0.4;
    item1.tag = 21;
    //item.isTouchEnabled=false;
    //		item.flipX = YES;
    // b2BodyType type = b2_dynamicBody;
    
    [item1 createBodyInWorld:world
                  b2bodyType:b2_dynamicBody
                       angle:0.0
                  allowSleep:true
               fixedRotation:true
                      bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item1 body] forShapeName:@"zzz"];
    [self addChild:item1 z:9];

    
    
    item = [[[TRBox2D alloc] initWithSpriteFrameName:@"zzz.png"] autorelease]; ///Middle Z Body
    [item setVertexZ:14] ;
    item.position = pos;
    item.scale = 0.4;
    item.tag = 20;
    //item.isTouchEnabled=false;
    //		item.flipX = YES;
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true
              fixedRotation:true
                     bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"zzz"];
    
    [self addChild:item z:9];
    
    
        
    item2 = [[[TRBox2D alloc] initWithSpriteFrameName:@"zzz.png"] autorelease];  //Right Z Body
    [item2 setVertexZ:14] ;
    item2.position = pos;
    item2.scale = 0.4;
    item2.tag = 22;
    //item.isTouchEnabled=false;
    //		item.flipX = YES;
    // b2BodyType type = b2_dynamicBody;
    
    [item2 createBodyInWorld:world
                 b2bodyType:b2_dynamicBody
                      angle:0.0
                 allowSleep:true
              fixedRotation:true
                     bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item2 body] forShapeName:@"zzz"];
    [self addChild:item2 z:9];
    
    [interections addObject:item1];
    [interections addObject:item];
    [interections addObject:item2];
     
    [item1 body]->ApplyLinearImpulse(b2Vec2(-5,5),b2Vec2(arc4random()%8 - 2.0,arc4random()%3 - 2.0));
    [item body]->ApplyLinearImpulse(b2Vec2(-5,5),b2Vec2(arc4random()%8 - 2.0,arc4random()%3 - 2.0));
    [item2 body]->ApplyLinearImpulse(b2Vec2(-5,5),b2Vec2(arc4random()%8 - 2.0,arc4random()%3 - 2.0));
   
    
}


- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    //static int flag=1;
	if (touchedSprite) {
        switch(touchedSprite.tag){
            case 1:
                if ((location.x>657 && location.x<737 && location.y>581 && location.y<661)&&([touchedSprite numberOfRunningActions] <=0)) {
                    [self animateMoon];
                }
                
                break;
                
            case 3:
                [self burnCandle];
                break;
                
            case 6:
                [self knifeFall];
                [interections removeObject:[self getChildByTag:6]];
                break;
                
            case 20:
            case 21:
            case 22:
                 TRBox2D *item = (TRBox2D*)[self getChildByTag:20];
                 TRBox2D *item1 = (TRBox2D*)[self getChildByTag:21];
                 TRBox2D *item2 = (TRBox2D*)[self getChildByTag:22];
                 item1.tag=121;
                 item.tag=120;
                 item2.tag=122;
                 [item1 body]->ApplyLinearImpulse(b2Vec2(-5,5),b2Vec2(arc4random()%8 - 2.0,arc4random()%3 - 2.0));
                 [item body]->ApplyLinearImpulse(b2Vec2(5,-5),b2Vec2(arc4random()%8 - 2.0,arc4random()%3 - 2.0));
                 [item2 body]->ApplyLinearImpulse(b2Vec2(5,5),b2Vec2(arc4random()%8 - 2.0,arc4random()%3 - 2.0));
                 
                 break;
                       
                                    
                }
            
                    }
    else if(location.x>960&&location.x<1000&&location.y>635)
    {
    [self burnCandle];
    }
    else if(location.x>675.00&&location.x<850.0&&location.y>100.00&&location.y<=400.00){
        
        [self popZBody:CGPointMake(location.x, location.y)];
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page19_4_snorring_female_bride.mp3"];
    }else if(location.x>500.00&&location.x<=675.000&&location.y>100.00&&location.y<400.00){
        
        [self popZBody:CGPointMake(location.x, location.y)];
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page19_4_snorring_male_prince.mp3"];
    }

        
    
    touchedSprite = NULL;
    touchedBody = NULL;
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super ccTouchesMoved:touches withEvent:event];
    if (touchedSprite) {
        if (touchedSprite.tag>119) {
            touchedSprite.position = location;
        }
        
    }
}
-(void) tick: (ccTime) dt{
    [super tick:dt];
  	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
            if (item.tag == 20 || item.tag == 21 || item.tag == 22 || item.tag == 120 || item.tag == 121 || item.tag == 122) {
                if (item.position.x < 50 || item.position.y<50 || item.position.y>715) {
                    [item destroyBodyInWorld:world];
                    [self removeChild:item cleanup:YES];
                }
            }
		}
    }
    
}


- (void) dealloc
{  
	[super dealloc];
}




@end
