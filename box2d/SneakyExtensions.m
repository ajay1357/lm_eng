//
//  SneakyExtensions.m
//  Tutorial1
//
//  Created by Alex Brown on 29/06/2011.
//  Copyright 2011 none. All rights reserved.
//

#import "SneakyExtensions.h"

//Extensions to the SneakyJoystick package, these are cocos2d like initialisers.
//This class was made from the book Learn iPhone and iPad cocos2d Game Development 
//by Steffan Itterheim, which created a nice and simple way of using the sneakyJoystick
//package
@implementation SneakyJoystick (Extension)
+(id) joystickWithRect:(CGRect)rect
{
	return [[[SneakyJoystick alloc] initWithRect:rect] autorelease];
}
@end

@implementation SneakyJoystickSkinnedBase (Extension)
+(id) skinnedJoystick
{
	return [[[SneakyJoystickSkinnedBase alloc] init] autorelease];
}
@end

@implementation SneakyButton (Extension)
+(id) button
{
	return [[[SneakyButton alloc] initWithRect:CGRectZero] autorelease];
}

+(id) buttonWithRect:(CGRect)rect
{
	return [[[SneakyButton alloc] initWithRect:rect] autorelease];
}
@end

@implementation SneakyButtonSkinnedBase (Extension)
+(id) skinnedButton
{
	return [[[SneakyButtonSkinnedBase alloc] init] autorelease];
}
@end
