//
//  HelloWorldLayer.mm
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "Page3.h"
//#import "ControlLayer.h"


// HelloWorldLayer implementation
@implementation Page3
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//ControlLayer *controlls = [[ControlLayer alloc] initWithLayer];
//	[scene addChild:controlls z:5];
	// 'layer' is an autorelease object.
	Page3 *layer = [Page3 node];

	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}




-(void)moveHand{
    id rotate = [CCRepeatForever actionWithAction: [CCSequence actions:[CCRotateBy actionWithDuration:1 angle: -10], [CCRotateBy actionWithDuration:1 angle: 10], nil]];
    [[[self getChildByTag:3] getChildByTag:18] runAction:rotate];
}




-(void)jevel{
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    CCParticleSystemQuad *jevel = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    jevel.tag = 25;
	jevel.texture = [[CCSprite spriteWithFile:@"jevel.png"] texture] ;
	jevel.emitterMode = kCCParticleModeGravity;
	jevel.duration=1.00;
	jevel.startColor = startColor;
	jevel.endColor = startColor;
    jevel.startColorVar = startColorVar;
	jevel.endColorVar = startColorVar;
	jevel.gravity = ccp(0,-1.5);
	jevel.autoRemoveOnFinish = YES;
	jevel.position = ccp(500, 750);
	jevel.posVar = ccp(500, 0);
	jevel.totalParticles = 30;
	jevel.emissionRate = 5;
	jevel.life = 70;
	jevel.lifeVar = 20;
	jevel.startSize = 20;
	jevel.startSizeVar = 10;
	jevel.endSize = 50;
	jevel.endSizeVar = 5;
    jevel.angle = 40;
    jevel.angleVar =20;
    //    bees3.rotation = -2434;
	[self addChild:jevel z:102];
}


//PAGE 3

-(void)fishes{
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    CCParticleSystemQuad *fishes = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    fishes.tag = 25;
	fishes.texture = [[CCSprite spriteWithFile:@"fish.png"] texture] ;
	fishes.emitterMode = kCCParticleModeGravity;
	fishes.duration=-1.00;
	fishes.startColor = startColor;
	fishes.endColor = startColor;
    fishes.startColorVar = startColorVar;
	fishes.endColorVar = startColorVar;
	fishes.gravity = ccp(-1.5,0);
	fishes.autoRemoveOnFinish = YES;
	fishes.position = ccp(900, 600);
	fishes.posVar = ccp(0, 100);
	fishes.totalParticles = 10;
	fishes.emissionRate = 2;
	fishes.life = 23;
	fishes.lifeVar = 0;
	fishes.startSize = 40;
	fishes.startSizeVar = 0;
	fishes.endSize = 40;
	fishes.endSizeVar = 0;
    fishes.angleVar =20;
	[self addChild:fishes];
}

-(void)animateNotes:(id)sender data:(NSNumber*)i{
    CCSprite* note;
    ccBezierConfig bezier;

    if ([i integerValue] %2 == 0) {
        int random1 = (int)(arc4random()%6);
        note = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"note%d.png", random1]];
        note.position = ccp(509, 583);
        note.tag = 40;
        [self addChild:note];
        bezier.controlPoint_1 = ccp(699, 720);
        bezier.controlPoint_2 = ccp(870, 500);
        bezier.endPosition = ccp(922, 520);
    }else{
        int random2 = (int)(arc4random()%6);
        note = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"note%d.png", random2]];
        note.position = ccp(509, 553);
        [self addChild:note];
        bezier.controlPoint_1 = ccp(639, 620);
        bezier.controlPoint_2 = ccp(870, 340);
        bezier.endPosition = ccp(922, 460);
    }
    id bezierAction = [CCBezierTo actionWithDuration:4 bezier:bezier];
    id scale = [CCScaleTo actionWithDuration:4 scale:0.3];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)note];
    [note runAction:[CCSequence actions:bezierAction, remove, nil]];
    [note runAction:scale];
}


-(void)createNotes{
    for (int i=1; i<13; i++) {
        id animate = [CCCallFuncND actionWithTarget:self selector:@selector(animateNotes:data:) data:(NSNumber *)[NSNumber numberWithInt:i]];
        id delay = [CCActionInterval actionWithDuration:i/4.0];
        [self runAction:[CCSequence actions:delay, animate, nil]];
    }
}




//PAGE 3 ENDS


// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:3])) {
//        self.scale = 2;
        
		// enable touches
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}



-(void)eyeBlink
{
    id fadein = [CCFadeIn actionWithDuration:0];
    id gap1 = [CCActionInterval actionWithDuration:5];
    id gap = [CCActionInterval actionWithDuration:0.5];
    id fadeout = [CCFadeOut actionWithDuration:0];
    
    
    id blink = [CCRepeatForever actionWithAction:[CCSequence actions:fadein,gap,fadeout,gap,fadein,gap,fadeout,gap1,nil]];
    
    [[self getChildByTag:35] runAction:blink];

}

-(void)waterripple{
    id waves = [CCWaves actionWithDuration:5 size:ccg(32, 24) waves:3 amplitude:3 horizontal:YES vertical:NO];
    [[self getChildByTag:36] runAction: [CCRepeatForever actionWithAction: waves]];
}


-(void)animateJevel
{
    id scaleup = [CCScaleTo actionWithDuration:2 scale:1];
    id scaledown = [CCScaleTo actionWithDuration:0.5 scale:0.5];
    
    id glitter = [CCRepeatForever actionWithAction:[CCSequence actions:scaleup,scaledown,nil]];
    
    [[self getChildByTag:34] runAction:glitter];
}

-(void)animateHand
{

id rotate1=[CCRotateBy actionWithDuration:1 angle:-15.0];
//id rotate2=[CCRotateBy actionWithDuration:1 angle:15.0];

    id hand_Move = [CCRepeatForever actionWithAction:[CCSequence actions:rotate1,[rotate1 reverse],nil]];
    
    [[[self getChildByTag:3] getChildByTag:1] runAction:hand_Move];
    
}



-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];

    [self sparkling:CGPointMake(440, 225) tag:20 sprite:NULL startSize:40 posvar:CGPointMake(50, 85) life:5 erate:15 tItem:70];    

    [self sparkling:self from:18 till:31];
    [self animate:[self getChildByTag:11] till:10 from:1 frame:@"Weed" forever:YES speed:1.2];
    [self animate:[self getChildByTag:40] till:10 from:1 frame:@"Weed" forever:YES speed:1.2];
    [self fishes];
    [self createNotes];
    [self eyeBlink];
    [self moveHand];
    [self waterripple];
    [self animateJevel];
    [self animateHand];
}

-(void) tick: (ccTime) dt{
    [super tick:dt];
    
    
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super ccTouchesMoved:touches withEvent:event];

    
}

-(void)animateOctopus
{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = 1; i <= 12; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"octo%i.png", i]]];
	}
    
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    
    [[self getChildByTag:2] runAction:forward];
    [[SimpleAudioEngine sharedEngine] playEffect:@"P3_6_octopus jump.mp3"];
    
}

-(void)animateStar
{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = 1; i <= 12; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"star%i.png", i]]];
	}
    
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    
    [[self getChildByTag:17] runAction:forward];


}

-(void)crabSound
{
 [[SimpleAudioEngine sharedEngine] playEffect:@"P3_3_crab.mp3"];

}

- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    if (touchedSprite == NULL) {
        [self jevel];
        [[SimpleAudioEngine sharedEngine] playEffect:@"P3_2_pearl mussels.mp3"];
       
    }else{
    
	switch(touchedSprite.tag) {
        case 3:
            if([touchedSprite numberOfRunningActions]<=0){
            [self createNotes];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P3_4_Mermaid_tune.mp3"];
            }
            break;
        case 5:
            if ([touchedSprite numberOfRunningActions]<=0) {
                [self animate:touchedSprite till:12 from:1 frame:@"crab" forever:NO speed:1.2];
               
                [self performSelector:@selector(crabSound) withObject:nil afterDelay:0.5f];
                 [self performSelector:@selector(crabSound) withObject:nil afterDelay:1.0f];
                
            }
            break;
            
        case 1:
        case 6:{
            id scaledown = [CCScaleTo actionWithDuration:0 scale:0];
            [[self getChildByTag:6] runAction:scaledown];
            id scaleup = [CCScaleTo actionWithDuration:0 scale:1];
            [[self getChildByTag:12] runAction:scaleup];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P3_5_glass tap.mp3"];
            
            break;}
            
        
        case 12:{
            id scaledown = [CCScaleTo actionWithDuration:0 scale:0];
            [[self getChildByTag:12] runAction:scaledown];
            id scaleup = [CCScaleTo actionWithDuration:0 scale:1];
            [[self getChildByTag:13] runAction:scaleup];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P3_5_glass tap.mp3"];
            break;}
            
        
        case 13:{
            id scaledown = [CCScaleTo actionWithDuration:0 scale:0];
            [[self getChildByTag:13] runAction:scaledown];
            id scaleup = [CCScaleTo actionWithDuration:0 scale:1];
            [[self getChildByTag:6] runAction:scaleup];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P3_5_glass tap.mp3"];
            break;}
        
        case 2: if ([touchedSprite numberOfRunningActions]<=0){
            [self animateOctopus];
            
        }
             break;
            
        case 17:if ([touchedSprite numberOfRunningActions]<=0){
            [self animateStar];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P3_1_Starfish happy sound2.mp3"];
        }
             break;
       
        
    }
    }
    touchedBody = NULL;
    touchedSprite = NULL;
}


- (void) dealloc
{

	[super dealloc];
}
@end
