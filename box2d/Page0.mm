//
//  Page0.m
//  Little Mermaid
//
//  Created by Pipedream on 08/10/12.
//
//

#import "Page0.h"
#import "GlobalSet.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation Page0


GlobalSet *global1;


+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	Page0 *layer = [Page0 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	
    
    // return the scene
    
	return scene;
}


-(id) init
{
	if( (self=[super initWithPage:0])) {
        global1 = [GlobalSet globalSettings];
        
    }
    
	return self;
}



-(void)startPage
{
    [self removeChildByTag:0 cleanup:YES];
    
    id gap = [CCActionInterval actionWithDuration:3];
    id play = [CCCallFuncN actionWithTarget:self selector:@selector(animateCharacter)];
    [self runAction:[CCSequence actions:gap, play, nil]];

}



-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
    [self sparkling:[self getChildByTag:1] from:1 till:17];
    
//    if(global1.FIRST_ATTEMPT){
//            
//        id gap = [CCActionInterval actionWithDuration:2];
//        id play = [CCCallFuncN actionWithTarget:self selector:@selector(animate)];
//        [self runAction:[CCSequence actions:gap, play, nil]];
//        global1.FIRST_ATTEMPT = NO;
//        
//    }else{
//        [self startPage];
//    }
    [self animateCharacter];
    global1.MENU = NO;
}



-(void)animateCharacter
{
    
    NSMutableArray *runFramesMouth = [NSMutableArray array] ;
    
    for(int i = 1; i <= 6; i++) {
        [runFramesMouth addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"m%i.png", i]]];
    }
    
    id animateMouth =  [CCRepeat actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFramesMouth delay:0.24]]  times:2];;
    
    [[self getChildByTag:18] runAction:animateMouth];
    
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:3.0];
    [[SimpleAudioEngine sharedEngine] playEffect:@"INTRO.mp3"];
    
}





- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    
//    if(touchedSprite)
//    {
//        
//        if(touchedSprite.tag==3 &&([[touchedSprite getChildByTag:3] numberOfRunningActions]<1))
//        {
//            [self animateCharacter];
//            
//        }
//        
//    }
   touchedSprite=NULL;
    
}




- (void) dealloc
{
   
   [[SimpleAudioEngine sharedEngine] setEffectsVolume:1.0];
    global1 = nil;
    
	[super dealloc];
}

@end
