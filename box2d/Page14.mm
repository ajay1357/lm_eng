//
//  Page17.m
//  Little Mermaid
//
//  Created by Pipedream on 15/09/12.
//
//

#import "Page14.h"

@implementation Page14

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page14 *layer = [Page14 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:14])) {
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}


-(void)eyeblink:(int)parent childTag:(int)child{
    
    id blink = [CCRepeat actionWithAction:[CCBlink actionWithDuration:1 blinks:1] times:5];
    id start_gap= [CCActionInterval actionWithDuration:(arc4random()%3)];
    id action = [CCRepeatForever actionWithAction:[CCSequence actions:start_gap,blink,nil]];
    
    if(child < 0 )
    {
        [[self getChildByTag:parent] runAction:action];
    }
    else{
    [[[self getChildByTag:parent] getChildByTag:child] runAction:action];
    }
}


-(void)glowButtons
{
    
    id fadein=[CCFadeIn actionWithDuration:0.6];
    id fadeout=[CCFadeOut actionWithDuration:0.3];
    
    id glow = [CCRepeatForever actionWithAction:[CCSequence actions:fadein,fadeout,nil]];
    
    [[self getChildByTag:9] runAction:glow];
    [[self getChildByTag:10] runAction:[[glow copy] autorelease]];
    [[self getChildByTag:11] runAction:[[glow copy] autorelease]];
  
    
}

-(void)ladyVeilAnimate
{
    
        NSMutableArray *runFrames = [NSMutableArray array];
        
        for(int i = 2; i <= 6; i++) {
            [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"lady%i.png", i]]];
        }
        
        id action = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.3]];
    id veilAnimate = [CCSequence actions:action,[action reverse], nil];
        
        [[self getChildByTag:14] runAction:[CCRepeatForever actionWithAction:veilAnimate]];
    
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
   
    [self eyeblink:1 childTag:-1];
    [self eyeblink:2 childTag:-1];
    [self eyeblink:3 childTag:1];
    [self ladyVeilAnimate];
    
    [self glowButtons];
    
    
}


-(void)placeButtons:(id)dummy data:(b2Body *) body
{
    body->SetType(b2_staticBody);
    int index=((CCSprite *)body->GetUserData()).tag;
    [self getChildByTag:index].scale=0.3;
    body->SetFixedRotation(true);
    if(index==6){
        body->SetTransform(b2Vec2(360.0/PTM_RATIO, 448.0/PTM_RATIO), 0.0);
        
    }
    else if (index==7){
        body->SetTransform(b2Vec2(364.0/PTM_RATIO, 382.0/PTM_RATIO), 0.0);
    }
    else{
       body->SetTransform(b2Vec2(366.0/PTM_RATIO, 311.0/PTM_RATIO), 0.0);
    
    }
    [[self getChildByTag:(index + 3)] resumeSchedulerAndActions];
    [[self getChildByTag:(index + 3)] setVisible:YES];
    
}

-(void)popButtons :(b2Body *)Body
{
    if(Body->GetType() == b2_staticBody){
    
    Body->SetType(b2_dynamicBody);
    Body->ApplyLinearImpulse(b2Vec2(arc4random()%6-3,arc4random()%6-3),b2Vec2(arc4random()%6-3,arc4random()%6-3));
     
    id place_back = [CCCallFuncND actionWithTarget:self selector:@selector(placeButtons:data:) data:(b2Body*)Body];
    id gap2 = [CCActionInterval actionWithDuration:10];
    [self runAction:[CCSequence actions:gap2,place_back,nil]];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page17_1_pinballbuttons.mp3"];
        
    }
    
}

-(void)flowerFall{
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    int i = arc4random()%3 + 1;
    CCParticleSystemQuad *jevel = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    jevel.tag = 25;
    jevel.texture = [[CCSprite spriteWithFile:[NSString stringWithFormat:@"Jflower%i.png",i]] texture] ;
	jevel.emitterMode = kCCParticleModeGravity;
	jevel.duration=1.00;
	jevel.startColor = startColor;
	jevel.endColor = startColor;
    jevel.startColorVar = startColorVar;
	jevel.endColorVar = startColorVar;
	jevel.gravity = ccp(0,-10);
	jevel.autoRemoveOnFinish = YES;
	jevel.position = ccp(500, 750);
	jevel.posVar = ccp(500, 0);
	jevel.totalParticles = 100;
	jevel.emissionRate = 10;
	jevel.life = 70;
	jevel.lifeVar = 20;
	jevel.startSize = 20;
	jevel.startSizeVar = 10;
	jevel.endSize = 50;
	jevel.endSizeVar = 5;
    jevel.angle = 40;
    jevel.angleVar =20;
    
	[self addChild:jevel z:10];
}


-(void)mermaidSobbing
{
    if([[self getChildByTag:12] numberOfRunningActions] == 0 )
    {
    NSMutableArray *runFrames = [NSMutableArray array];
    
    for(int i = 1; i <= 10; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"mermaid%i.png", i]]];
	}
    
    id sobbing = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    
    
        [[self getChildByTag:12] runAction:[CCSequence actions:sobbing,[sobbing reverse],nil ]];
        
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page17_2_sobbing mermaid.mp3"];

    }

}


-(void)animateRing
{
    
    id fadein=[CCFadeIn actionWithDuration:0.6];
    id fadeout=[CCFadeOut actionWithDuration:0.3];
    
    id glow = [CCRepeat actionWithAction:[CCSequence actions:fadein,fadeout,nil] times:10];
    
    [[[self getChildByTag:4] getChildByTag:2] runAction:glow];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page17_3_ringprince.mp3"];


}

-(void)animatePrinceHand
{
    if([[self getChildByTag:4] numberOfRunningActions] == 0 )
    {
    id gap1 = [CCActionInterval actionWithDuration:4];
    id rotate_hand=[CCRotateTo actionWithDuration:1 angle:-35.0];
        id rotate_back=[CCRotateBy actionWithDuration:1 angle:35.0];
    
    id action = [CCSequence actions:rotate_hand,gap1,rotate_back, nil];
    
    [[self getChildByTag:4] runAction:action];
 
        [self animateRing];
        
    }


}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
	//Add a new body/atlas sprite at the touched location
    
	for( UITouch *touch in touches ) {
		location = [touch locationInView: [touch view]];
		
		location = [[CCDirector sharedDirector] convertToGL: location];
        //		[self getChildByTag:100].visible = false;
        
		if (mouseJoint != NULL) return;
		
		//noticed that in this case the location is not trasformed to box2d coordinates
		[self setTouched:location];
        
    
    
}
}



-(void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node
{
    [super tapped:recognizer node:node];
    if (touchedSprite) {
        switch (touchedSprite.tag) {
            case 6:
            case 7:
            case 8:
                touchedSprite.scale=1;
                [[self getChildByTag:(touchedSprite.tag+3)] pauseSchedulerAndActions];
                [[self getChildByTag:(touchedSprite.tag+3) ] setVisible:NO];
                if (touchedBody) {
                    [self popButtons:touchedBody];
                }
                
                break;
            case 5:
            case 12:
                [self mermaidSobbing];
                break;
                
            case 3:
                [self animatePrinceHand];
                break;
                
            default:
                [self flowerFall];
                [[SimpleAudioEngine sharedEngine] playEffect:@"P3_2_pearl mussels.mp3"];
                break;
        }
    }
   
   touchedSprite = NULL;
   touchedBody = NULL;
}

- (void) dealloc
{
    
	[super dealloc];
}



@end
