//
//  ControlLayer.h
//  Tutorial1
//
//  Created by Alex Brown on 29/06/2011.
//  Copyright 2011 none. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SneakyJoystick.h"
#import "SneakyExtensions.h"
#import "SneakyJoystickSkinnedBase.h"
#import "SneakyButtonSkinnedBase.h"

@interface ControlLayer : CCLayer {
		
    //create an instance of the SneakyJoystick class
    SneakyJoystick* joystick;
    //create an instance of the SneakyJoystickSkinnedBase class
    SneakyJoystickSkinnedBase* JSSkin;

    SneakyButton* jBtn;
    SneakyButtonSkinnedBase* jBtnSkin;    
    
    SneakyButton* aBtn;
    SneakyButtonSkinnedBase* aBtnSkin;

    SneakyButton* tBtn;
    SneakyButtonSkinnedBase* tBtnSkin;
    

    BOOL jmpBtn;
    BOOL actBtn;
	BOOL taskBtn;
    CGPoint joyStickPos;
    float joyStickRotation;
}

-(id)initWithLayer;
+(id)control;
-(void)addJoystick;
-(void)addJBtn;
-(void)addABtn;
-(void)addTBtn;
+(ControlLayer*)getInstance;
@property(nonatomic, assign) BOOL jmpBtn;
@property(nonatomic, assign) BOOL actBtn;
@property(nonatomic, assign) BOOL taskBtn;
@property(nonatomic, assign) CGPoint joyStickPos;
@property(nonatomic, assign) float joyStickRotation;

@end
