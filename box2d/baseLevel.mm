 //
//  baseLevel.m
//  box2d
//
//  Created by MacBook on 24/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "baseLevel.h"
#import "GlobalSet.h"



@implementation baseLevel
@synthesize world;
@synthesize groundBody;
@synthesize interections;
@synthesize location;
@synthesize parallax;
b2BodyType mbodytype;
NSDictionary *pageData;

CDLongAudioSource* rightChannel = NULL;
CDLongAudioSource* leftChannel = NULL;



// SCROLL VIEW PAGE SIZE
CGFloat kScrollHeight	= 798;
CGFloat kScrollWidth	= 1024;

#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5
#define THUMB_HEIGHT 154
#define THUMB_V_PADDING 5
#define THUMB_H_PADDING 5
#define AUTOSCROLL_THRESHOLD 30



GlobalSet * global ;


-(bool)isIpad
{
#ifndef __MAC_OS_X_VERSION_MAX_ALLOWED
	
	
#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
#if __IPHONE_3_2 <= __IPHONE_OS_VERSION_MAX_ALLOWED
	
	UIDevice* thisDevice = [UIDevice currentDevice];
	
	if ([thisDevice respondsToSelector:@selector(userInterfaceIdiom)]) 
		{
		if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad)
			{
			return true;
			}
		}
	else
		{
		return false;
		}
#else
	return false;
#endif
	
#endif
	
#else
	return true;
#endif
	return false;
}

-(void) initworld{
	// Define the gravity vector.
	b2Vec2 gravity;
	gravity.Set(0.0f, -0.5f);
	
	// Do we want to let bodies sleep?
	// This will speed up the physics simulation
	bool doSleep = true;
	
	// Construct a world object, which will hold and simulate the rigid bodies.
    
	world = new b2World(gravity);
	world->SetAllowSleeping(doSleep);
	world->SetContinuousPhysics(true);
	// Debug Draw functions
//	m_debugDraw = new GLESDebugDraw( PTM_RATIO );
//	world->SetDebugDraw(m_debugDraw);
//	
//	uint32 flags = 0;
//	flags += b2Draw::e_shapeBit;
//	flags += b2Draw::e_jointBit;
//	flags += b2Draw::e_aabbBit;
//	flags += b2Draw::e_pairBit;
//	flags += b2Draw::e_centerOfMassBit;
//	m_debugDraw->SetFlags(flags);	
}


-(void)sparkling:(CCNode*)parent from:(int)from till:(int)till{
    
       
    for (int i=from; i<=till; i++) {
        float pscale = ((float)(arc4random()%100+150)) / 100.0f;
        id scalel = [CCScaleTo actionWithDuration:pscale/2 scale:1];
        id scales = [CCScaleTo actionWithDuration:pscale/2 scale:0];
        id scale = [CCSequence actions:scalel, scales, nil];
        [[parent getChildByTag:i] runAction:[CCRepeatForever actionWithAction: scale]];
    }
}


-(void)animateRepeat:(CCNode*)node till:(int)till from:(int)from frame:(NSString*)frame forever:(BOOL)forever speed:(float)speed{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = from; i <= till; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"%@%i.png",frame, i]]];
	}
    float delay = speed/(till-from+1);
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:delay]];
    id animate;
    if(forever == NO){
        animate = [CCSpeed actionWithAction:forward speed:speed];
    }else{
        animate = [CCSpeed actionWithAction:
                  [CCRepeatForever actionWithAction:forward] speed:1.2];
    }
    [node runAction:animate];
    
}

-(void)animate:(CCNode*)node till:(int)till from:(int)from frame:(NSString*)frame forever:(BOOL)forever speed:(float)speed{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = from; i <= till*2; i++) {
        if (i>till) {
            [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"%@%i.png",frame, till*2+1-i]]];
        }else{
            [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"%@%i.png",frame, i]]];
        }
	}
    float delay = speed/(till-from+1);
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:delay]];
    id animate;
    if(forever == NO){
        animate = [CCSpeed actionWithAction:forward speed:speed];
    }else{
        animate = [CCSpeed actionWithAction:
                   [CCRepeatForever actionWithAction:forward] speed:1];
    }
    [node runAction:animate];
    
        
}


-(void)loadPageData{
    NSString *errorDesc = nil;
	NSPropertyListFormat format;
	NSString *plistPath;
	NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    
    
    //    Load Custom level info: Position n all
	plistPath = [rootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"data%d.plist", currentPage]];
	if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
		plistPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"data%d", currentPage] ofType:@"plist"];
	}
	NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
	NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
	if (!temp) {
		NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
        return;
	}
    
	pageData= [temp objectForKey:@"root"];
    
    parallax = NULL;
    if ([[temp objectForKey:@"isParallax"]boolValue]) {
        parallax = [[[CCParallaxScrollNode alloc] init] autorelease];
    }
    	textData = [temp objectForKey:@"texts"];
        mediaData = [temp objectForKey:@"media"];
        
    
      
    switch(global.language)//Select Narration Language
    {
        case 1:
            narrationSound = (NSString *)[[mediaData valueForKey:@"Narration"] valueForKey:@"English"];
            break;
        case 2:
            narrationSound = (NSString *)[[mediaData valueForKey:@"Narration"] valueForKey:@"Dansk"];
            break;
        case 3:
            narrationSound = (NSString *)[[mediaData valueForKey:@"Narration"] valueForKey:@"Svensk"];
            break;
        case 4:
            narrationSound = (NSString *)[[mediaData valueForKey:@"Narration"] valueForKey:@"NORSK"];
            break;
    }
    
    
        const char *c = [narrationSound UTF8String];
        strncpy(narrationArray , c, [narrationSound length]);
            
        bgSound = (NSString *)[mediaData valueForKey:@"BgSound"];
    
    
}

-(void)loadLevelData{
	
	[[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrames];
	//Load physics related info
	[[GB2ShapeCache sharedShapeCache] addShapesWithFile:[NSString stringWithFormat:@"level%d.plist", currentPage]];
    
    
//	Load images info
	CCSpriteBatchNode* batch;
	[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:@"ldata%d.plist", currentPage]];
    
	batch = [[[CCSpriteBatchNode alloc] initWithFile:[NSString stringWithFormat:@"ldata%d.png", currentPage] capacity:50] autorelease];
    
	[self addChild:batch];

}


-(void)removeitem:(id)sender data:(CCSprite*)item{
    [self removeChild:item cleanup:YES];
}


-(void) initground{
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	groundBodyDef.type = b2_staticBody;
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;	
	// bottom
    b2FixtureDef groundFD;
	groundBox.Set(b2Vec2(0,0), b2Vec2(screenSize.width/PTM_RATIO,0));
    
    if(currentPage == 1 ){
        groundFD.filter.maskBits = 0x0000;
        groundFD.filter.categoryBits = 0x0000;
    }else{
    groundFD.filter.maskBits = 0xFFFF;
    groundFD.filter.categoryBits = 0xFFFF;
    }
    groundFD.shape = &groundBox;
	groundBody->CreateFixture(&groundFD);
	
    
	// top
	groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO));
	groundBody->CreateFixture(&groundFD);
	
	// left
	groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(0,0));
	groundBody->CreateFixture(&groundFD);
	
	// right
	groundBox.Set(b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,0));
	
	groundBody->CreateFixture(&groundFD);
}	

-(void) AddGestures{
    UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
     action:@selector(tapped:node:)];
    
    UISwipeGestureRecognizer *recognizerLeft;
    recognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizerLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    recognizerLeft.numberOfTouchesRequired = 1;
    
    UISwipeGestureRecognizer *recognizerRight;
    recognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    recognizerRight.numberOfTouchesRequired = 1;
    
    [[[CCDirector sharedDirector] view] addGestureRecognizer:recognizerLeft];
    [[[CCDirector sharedDirector] view] addGestureRecognizer:recognizerRight];
    [[[CCDirector sharedDirector] view] addGestureRecognizer:recognizer];
}

-(BOOL)springJoint:(b2Body*)bodyA bodyB:(b2Body*)bodyB join:(NSMutableDictionary*)joint{
	if (bodyB==NULL) {
        b2BodyDef myBodyDef;
        myBodyDef.type = b2_staticBody; //this will be a static body
        myBodyDef.position.Set(bodyA->GetPosition().x, bodyA->GetPosition().y); //slightly lower position
        bodyB = world->CreateBody(&myBodyDef); //add body to world
        
        b2PolygonShape boxShape;
        boxShape.SetAsBox(0.1,0.1);
        b2FixtureDef boxFixtureDef;
        boxFixtureDef.filter.categoryBits = 0x0000;
        boxFixtureDef.filter.maskBits = 0x0000;
        
        boxFixtureDef.shape = &boxShape;
        boxFixtureDef.density = 1;
        bodyB->CreateFixture(&boxFixtureDef);
        
    }
    jointUserData *djud = new jointUserData;
	djud->tag = PULL_ACTOR; //JOINT Tag
	b2DistanceJointDef djd;
	djd.bodyA=bodyA; //define bodies
	djd.bodyB=bodyB;
    
    float anchorAx = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"x"] floatValue];
    float anchorAy = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"y"] floatValue];
	djd.localAnchorA = b2Vec2(anchorAx,anchorAy); //define anchors
    float anchorBx = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"x"] floatValue];
    float anchorBy = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"y"] floatValue];
	djd.localAnchorB = b2Vec2(anchorBx,anchorBy);
	
    
//    djd.userData = djud;
//	jd.maxLength= 2; //define max length of joint = current distance between bodies
    djd.dampingRatio = [[joint valueForKey:@"damping"] floatValue];
    djd.collideConnected=YES;
    djd.frequencyHz=[[joint valueForKey:@"frequency"] floatValue];
    djd.length = 0;
    bodyB->SetFixedRotation(YES);
	world->CreateJoint(&djd);
    
    
    jointUserData *pulljud = new jointUserData;
	pulljud->tag = PULL_ACTOR; // Joint Tag
	b2RopeJointDef rjd;
	rjd.bodyA=bodyA; //define bodies
	rjd.bodyB=bodyB;
	rjd.localAnchorA = b2Vec2(anchorAx,anchorAy); //define anchors
	rjd.localAnchorB = b2Vec2(anchorBx,anchorBy);
//	rjd.userData = pulljud;
	rjd.maxLength= [[joint valueForKey:@"length"] floatValue]; //define max length of joint = current distance between bodies
	world->CreateJoint(&rjd);
    
    
	return YES;
}


-(BOOL)ropeJoint:(b2Body*)bodyA bodyB:(b2Body*)bodyB join:(NSMutableDictionary*)joint{
    float anchorAx = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"x"] floatValue];
    float anchorAy = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"y"] floatValue];
    float anchorBx = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"x"] floatValue];
    float anchorBy = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"y"] floatValue];
    
    b2RopeJointDef rjd;
	rjd.bodyA=bodyA; //define bodies
	rjd.bodyB=bodyB;
	rjd.localAnchorA = b2Vec2(anchorAx,anchorAy); //define anchors
	rjd.localAnchorB = b2Vec2(anchorBx,anchorBy);
    //	rjd.userData = pulljud;
	rjd.maxLength= [[joint valueForKey:@"length"] floatValue]; //define max length of joint = current distance between bodies
	world->CreateJoint(&rjd);
    return YES;
}

-(BOOL)weldJoint:(b2Body*)bodyA bodyB:(b2Body*)bodyB join:(NSMutableDictionary*)joint{
    float anchorAx = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"x"] floatValue];
    float anchorAy = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"y"] floatValue];
    float anchorBx = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"x"] floatValue];
    float anchorBy = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"y"] floatValue];
    

    b2RopeJointDef wjd;
    wjd.bodyA = bodyA;
    wjd.bodyB = bodyB;
    wjd.localAnchorA =b2Vec2(anchorAx,anchorAy);
	wjd.localAnchorB = b2Vec2(anchorBx,anchorBy);
    wjd.maxLength = 0.1;
    wjd.collideConnected = true;
    world->CreateJoint(&wjd);
    return true;
    
}

-(BOOL)revoluteJoint:(b2Body*)bodyA bodyB:(b2Body*)bodyB join:(NSMutableDictionary*)joint{
    float anchorAx = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"x"] floatValue];
    float anchorAy = [[[joint valueForKey:@"anchorPontA"] valueForKey:@"y"] floatValue];
    float anchorBx = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"x"] floatValue];
    float anchorBy = [[[joint valueForKey:@"anchorPontB"] valueForKey:@"y"] floatValue];

    b2RevoluteJointDef rjd;
    rjd.bodyA = bodyA;
    rjd.bodyB = bodyB;
    rjd.localAnchorA =b2Vec2(anchorAx,anchorAy);
	rjd.localAnchorB = b2Vec2(anchorBx,anchorBy);
    
    rjd.maxMotorTorque = 0.1;
    rjd.collideConnected = true;
    if ([joint valueForKey:@"limit"]) {
        float lowerLimit = [[[joint valueForKey:@"limit"] valueForKey:@"lower"] floatValue];
        float uperLimit = [[[joint valueForKey:@"limit"] valueForKey:@"uper"] floatValue];
        rjd.lowerAngle = -lowerLimit*b2_pi;
        rjd.upperAngle = uperLimit*b2_pi;
        rjd.enableLimit = YES;
    }else{
        rjd.enableLimit = NO;
    }

    rjd.enableMotor = YES;
    
    world->CreateJoint(&rjd);
    
    return true;
    
}



-(BOOL)prismaticJoint:(b2Body*)bodyA bodyB:(b2Body*)bodyB join:(NSMutableDictionary*)joint{
    b2PrismaticJointDef jointDef;
    b2Vec2 worldAxis(1.0f, 0.0f);
    jointDef.collideConnected = true;
    jointDef.Initialize(bodyA, bodyB, 
                        bodyA->GetWorldCenter(), worldAxis);
    jointDef.enableMotor = YES;
    jointDef.maxMotorForce = 0.1;
    world->CreateJoint(&jointDef);
    return YES;
}

-(void)createMouseJoint:(CGPoint)point{
	if(0 == touchedBody)
		return ;
	b2MouseJointDef md;
	md.bodyA = groundBody;
	md.bodyB = touchedBody;
	b2Vec2 locationWorld = b2Vec2(point.x/PTM_RATIO, point.y/PTM_RATIO);
	md.target = locationWorld;
	md.collideConnected = true;
	md.maxForce = 1000.0f * touchedBody->GetMass();
	touchedBody->SetAwake(true);
	mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
}

//-(BOOL)createWallJoint:(b2Body*)bodyA bodyB:(b2Body*)bodyB join:(NSMutableDictionary*)joint{
//}


-(void)createBubbles:(CGPoint)loc index:(int)index sprite:(TRBox2D*)sprite endsize:(float)endSize{
	CCParticleSmoke *bubbles = [[[CCParticleSmoke alloc]init] autorelease];
	bubbles.texture = [[CCSprite spriteWithFile:@"bubble.png"] texture] ;
	bubbles.autoRemoveOnFinish = YES;
	bubbles.position = ccp(loc.x, loc.y);
	bubbles.posVar = ccp(0, 0);
	bubbles.totalParticles = 20;
	bubbles.emissionRate = 1;
	ccColor4F strtcolor = {0xFF/255.0,0xFF/255.0,0xFF/255.0,0x00/255.0};
	bubbles.startColor = strtcolor;
	bubbles.endColor = strtcolor;
//	ccColor4F strtcolorv = {0xFF/255.0, 0xC2/255.0, 0xA0/255.0, 0x00/255.0};
//	bubbles.startColorVar = strtcolorv;
//	bubbles.endColorVar = strtcolorv;
	bubbles.life = 15;
	bubbles.lifeVar = 6;
	bubbles.startSize = 5;
	bubbles.startSizeVar = 0;
	bubbles.endSize = endSize;
	bubbles.endSizeVar = 20;
	bubbles.gravity = ccp(0,0.5);
	[bubbles setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
    if(!sprite){
        [self addChild:bubbles z:index];
    }else{
        [sprite addChild:bubbles z:index];
    }
}

-(void)evilBubbles:(CGPoint)loc index:(int)index sprite:(TRBox2D*)sprite endsize:(float)endSize{
    ccColor4F startColor;
    startColor.r = 0.45f;
    startColor.g = 0.95f;
    startColor.b = 0.3f;
    startColor.a = 1.0f;
    
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    ccColor4F endcolor;
    endcolor.r = 0.0f;
    endcolor.g = 0.0f;
    endcolor.b = 0.0f;
    endcolor.a = 0.0f;
    
	CCParticleSmoke *evilbubbles = [[[CCParticleSmoke alloc]init] autorelease];
	evilbubbles.texture = [[CCSprite spriteWithFile:@"bubble.png"] texture] ;
	evilbubbles.autoRemoveOnFinish = YES;
	evilbubbles.position = ccp(loc.x, loc.y);
    evilbubbles.startColor = startColor;
    evilbubbles.startColorVar = startColorVar;
    evilbubbles.endColor = endcolor;
    evilbubbles.endColorVar = endcolor;
	evilbubbles.posVar = ccp(0, 0);
	evilbubbles.totalParticles = 20;
	evilbubbles.emissionRate = 1;
	evilbubbles.life = 15;
	evilbubbles.lifeVar = 6;
	evilbubbles.startSize = 5;
	evilbubbles.startSizeVar = 0;
	evilbubbles.endSize = endSize;
	evilbubbles.endSizeVar = 20;
	evilbubbles.gravity = ccp(0,0.5);
	[evilbubbles setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
    if(!sprite){
        [self addChild:evilbubbles z:index];
    }else{
        [sprite addChild:evilbubbles z:index];
    }
}

-(void)sparkling:(CGPoint)loc tag:(int)tag sprite:(TRBox2D*)sprite startSize:(float)startSize posvar:(CGPoint)posvar life:(float)life erate:(float)erate tItem:(float)tItem{
	
	ccColor4F strtcolor = {0xFF/255.0,0x38/255.0,0x0D/255.0,0xFF/255.0};
    
	ccColor4F strtcolorv = {0xFF/255.0, 0xC2/255.0, 0xA0/255.0, 0xFF/255.0};
    
    
    CCParticleSystemQuad *bees3 = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
	bees3.texture = [[CCSprite spriteWithFile:@"sstar.png"] texture] ;
	bees3.emitterMode = kCCParticleModeGravity;
	bees3.duration=-1.00;
	bees3.startColor=strtcolor;
	bees3.endColor = strtcolor;
    bees3.startColorVar = strtcolorv;
	bees3.endColorVar = strtcolorv;
	bees3.gravity = ccp(0,0);
	bees3.autoRemoveOnFinish = YES;
	bees3.position = loc;
	bees3.posVar = posvar;
	bees3.totalParticles = tItem;
	bees3.emissionRate = erate;
	bees3.life = life;
	bees3.lifeVar = 1;
	bees3.startSize = startSize;
	bees3.startSizeVar = 5;
	bees3.endSize = 0;
	bees3.endSizeVar = 0;
    [bees3 setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
    if (sprite == NULL) {
        [self addChild:bees3 z:50];
    }else{
        [sprite addChild:bees3 z:50];
    }
}


-(void)water:(CGPoint)loc gravity:(float)gravity{
    ccColor4F strtcolor = {0x05/255.0, 0x05/255.0, 0x05/255.0, 0x66/255.0};
    
	ccColor4F strtcolorv = {0x00/255.0, 0x00/255.0, 0x00/255.0, 0x66/255.0};
    
    
    CCParticleSystemQuad *bees3 = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
	bees3.texture = [[CCSprite spriteWithFile:@"smoke.png"] texture] ;
	bees3.emitterMode = kCCParticleModeGravity;
	bees3.duration=-1.00;
	bees3.startColor=strtcolor;
	bees3.endColor = strtcolor;
//    bees3.startColorVar = strtcolorv;
	bees3.endColorVar = strtcolorv;
	bees3.gravity = ccp(gravity,0);
	bees3.autoRemoveOnFinish = YES;
	bees3.position = loc;
	bees3.posVar = ccp(300, 0);
	bees3.totalParticles = 100;
	bees3.emissionRate = 20;
	bees3.life = 20;
	bees3.lifeVar = 1;
	bees3.startSize = 150;
	bees3.startSizeVar = 55;
	bees3.endSize = 400;
	bees3.endSizeVar = 0;
    bees3.angleVar =20;
//    bees3.rotation = -2434;
    [bees3 setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
	[self addChild:bees3 z:99];
    
}


-(void)smoke:(CGPoint)loc radius:(float)radius{
    ccColor4F strtcolor;
    strtcolor.r = 1.0f;
    strtcolor.g = 1.0f;
    strtcolor.b = 1.0f;
    strtcolor.a = 1.0f;
    ccColor4F strtcolorv;
    strtcolorv.r = 0.0f;
    strtcolorv.g = 0.0f;
    strtcolorv.b = 0.0f;
    strtcolorv.a = 0.0f;

    CCParticleSystemQuad *smke = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
	smke.texture = [[CCSprite spriteWithFile:@"smoke.png"] texture] ;
	smke.emitterMode = kCCParticleModeRadius;
	smke.duration=-1.00;
	smke.startColor=strtcolor;
	smke.endColor = strtcolor;
    smke.startColorVar = strtcolorv;
	smke.endColorVar = strtcolorv;
    smke.startRadius = 0;
    smke.startRadiusVar = 400;
    smke.endRadius = 600;
    smke.endRadiusVar = 400;
	smke.autoRemoveOnFinish = YES;
	smke.position = loc;
	smke.posVar = ccp(300, 0);
	smke.totalParticles = 400;
	smke.emissionRate = 20;
	smke.life = 10;
	smke.lifeVar = 1;
	smke.startSize = 250;
	smke.startSizeVar = 55;
	smke.endSize = 400;
	smke.endSizeVar = 0;
    smke.angle = 260;
    smke.angleVar =180;
    smke.rotatePerSecond = 0.5;
    smke.rotatePerSecondVar = 40.58;
    smke.rotation = -2434;
    
//    [bees3 setBlendFunc:(ccBlendFunc) {GL_SRC_ALPHA, GL_ONE}];
	[self addChild:smke z:99];
    
}

-(void)throwBodies:(b2Vec2)direction position:(b2Vec2)position tbody:(b2Body*)tbody impulse:(b2Vec2)impulse{
	if (tbody==NULL) {
        CCSprite* sprite = [CCSprite spriteWithFile:@"bubble.png"];
        sprite.position = CGPointMake(position.x, position.y);
        [self addChild:sprite z:10];
        b2BodyDef bodyDef7;
        bodyDef7.type = b2_dynamicBody;
        bodyDef7.allowSleep = YES;
        bodyDef7.position = position;
        bodyDef7.userData = sprite;
        tbody = world->CreateBody(&bodyDef7);
        b2PolygonShape kinematicBox7;
        kinematicBox7.SetAsBox(0.3f, 0.3f);
        b2FixtureDef fixtureDef7;
        fixtureDef7.shape = &kinematicBox7;	
        fixtureDef7.density = 0.0f;
        fixtureDef7.friction = 0.0f;
        fixtureDef7.filter.categoryBits = 0;
        fixtureDef7.filter.maskBits = 0;
        tbody->CreateFixture(&fixtureDef7);
    }
//    NSLog(@"IMPULSE: %f, %f", impulse.x, impulse.y);
	tbody->ApplyLinearImpulse(impulse, direction);
}

-(void)create:(NSMutableDictionary*)data tag:(int)tag parent:(CCSprite*)parent{
    if ([[ data valueForKey:@"physics"] boolValue] && [[ data valueForKey:@"active"] boolValue]) {
        [self makeBody:data bodyTag:tag parent:parent];
    }else{
        if (![[ data valueForKey:@"active"] boolValue]){
            return;
        }
        if ([data valueForKey:@"parallax"] && parallax!=NULL) {
            [self addParallax:data spriteTag:tag parent:parent];
        }else{
            [self makeSprite:data spriteTag:tag parent:parent];
        }
    }
}


-(CCSprite*)addSpriteData:(NSMutableDictionary*)data sprite:(CCSprite*)sprite{
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    if ([ data valueForKey:@"rotate"]) {
		sprite.rotation = [[ data valueForKey:@"rotate"] integerValue];
	}
	if ([ data valueForKey:@"scale"]) {
		sprite.scale = [[ data valueForKey:@"scale"] floatValue];
	}
	
	if ([ data valueForKey:@"opacity"]) {
		sprite.opacity = [[ data valueForKey:@"opacity"] integerValue];
	}
	if (![[ data valueForKey:@"visible"] boolValue]) {
		sprite.opacity = 0;
	}
	if ([ data valueForKey:@"anchor"]) {
		if([[[ data valueForKey:@"anchor"] valueForKey:@"x"] integerValue] != -1){
			sprite.anchorPoint = ccp([[[ data valueForKey:@"anchor"] valueForKey:@"x"] floatValue], [[[ data valueForKey:@"anchor"] valueForKey:@"y"] floatValue]);
		}
	}
	if ([ data valueForKey:@"position"]) {
		if([[[ data valueForKey:@"position"] valueForKey:@"x"] integerValue] == 0){
			sprite.position = ccp(winSize.width/2, winSize.height/2);
		}else{
			sprite.position = ccp([[[ data valueForKey:@"position"] valueForKey:@"x"] floatValue], [[[ data valueForKey:@"position"] valueForKey:@"y"] floatValue]);
		}
	}
    if ([[ data valueForKey:@"isInterective"] boolValue]) {
        [interections addObject:sprite];
    }
    return sprite;
}

-(void)makeSprite:(NSMutableDictionary*)spriteData spriteTag:(int)tag parent:(CCSprite*)parent{
    //	NSLog(@"sprite: %@", name);
	
	NSString *frameName ;
	frameName	= [NSString stringWithFormat:@"%@", (NSString*)[ spriteData valueForKey:@"name"]];
	CCSprite *item	 = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:frameName]];
	item.tag = tag;
    
	[self addSpriteData:spriteData sprite:item];    
    
	[[item texture] setAliasTexParameters];
    if (parent) {
        if ([ spriteData valueForKey:@"index"]) {
            [parent addChild:item z:[[ spriteData valueForKey:@"index"] integerValue]];
        }else{
            [parent addChild:item z:0];
        }
    }else{
        if ([ spriteData valueForKey:@"index"]) {
            [self addChild:item z:[[ spriteData valueForKey:@"index"] integerValue]];
        }else{
            [self addChild:item z:0];
        }
    }
    
    if ([ spriteData valueForKey:@"child"]) {
        for(id key in [ spriteData valueForKey:@"child"]){
            NSMutableDictionary *data=  (NSMutableDictionary *) [[ spriteData valueForKey:@"child"] valueForKey:key];
            [self create:data tag:[key integerValue] parent:item];
        }
    }
}

-(void)addParallax:(NSMutableDictionary*)spriteData spriteTag:(int)tag parent:(CCSprite*)parent{
    
	NSString *frameName ;
	frameName	= [NSString stringWithFormat:@"%@", (NSString*)[ spriteData valueForKey:@"name"]];
	CCSprite *item1	 = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:frameName]];
    CCSprite *item2	 = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:frameName]];
	item1.tag = item2.tag = tag;
	
    [self addSpriteData:spriteData sprite:item1];
    [self addSpriteData:spriteData sprite:item2];
    
	[[item1 texture] setAliasTexParameters];
    [[item2 texture] setAliasTexParameters];
    
    int z = [[ spriteData valueForKey:@"index"] integerValue];
    NSDictionary* parallaxData = [ spriteData valueForKey:@"parallax"];
    CGPoint ratio = ccp([[[parallaxData valueForKey:@"ratio"] valueForKey:@"x"] floatValue], [[[parallaxData valueForKey:@"ratio"] valueForKey:@"y"] floatValue]);
    CGPoint pos = ccp([[[ parallaxData valueForKey:@"position"] valueForKey:@"x"] floatValue], [[[ parallaxData valueForKey:@"position"] valueForKey:@"y"] floatValue]);
    
    [parallax addInfiniteScrollXWithZ:z Ratio:ratio Pos:pos Objects:item1, item2, nil];
    
    if ([ spriteData valueForKey:@"child"]) {
        for(id key in [ spriteData valueForKey:@"child"]){
            NSMutableDictionary *data=  (NSMutableDictionary *) [[ spriteData valueForKey:@"child"] valueForKey:key];
            if (![[ data valueForKey:@"active"] boolValue]){
                continue;
            }
            if([[ data valueForKey:@"physics"] boolValue]) {
                [self makeBody:data bodyTag:[key integerValue] parent:NULL];
            }
            
            [self makeSprite:data spriteTag:[key integerValue] parent:item1];
            [self makeSprite:data spriteTag:[key integerValue] parent:item2];
        }
    }
}






-(void)makeBody:(NSMutableDictionary*)bodyData bodyTag:(int)tag parent:(CCSprite*)parent{
//	NSLog(@"BODY: %@", name);
	CGSize winSize = [[CCDirector sharedDirector] winSize];
    NSString* name = (NSString*)[bodyData valueForKey:@"name"];
	NSString *frameName ;
	frameName	= [NSString stringWithFormat:@"%@", name];
    TRBox2D *item;
    if ([[ bodyData valueForKey:@"active"] boolValue]) {
        item = [[[TRBox2D alloc] initWithSpriteFrameName:frameName] autorelease];
    }else{
        item = [[[TRBox2D alloc] init] autorelease] ;
    }
//    item = [[TRBox2D alloc] init] ;
	item.tag = tag;
   
//    NSLog(@"BODY TAG: %d", tag);
    float angle =0.0;
	if ([ bodyData valueForKey:@"rotate"]) {
		item.rotation = [[ bodyData valueForKey:@"rotate"] integerValue];
        angle = [[ bodyData valueForKey:@"rotate"] floatValue]*DEGTORAD;
        [[item texture] setAliasTexParameters];
	}
	
	if ([ bodyData valueForKey:@"scale"]) {
		item.scale = [[ bodyData valueForKey:@"scale"] floatValue];
	}
    
    if (![[ bodyData valueForKey:@"visible"] boolValue]) {
		item.opacity = 0;
	}
    
	if ([ bodyData valueForKey:@"opacity"]) {
		item.opacity = [[ bodyData valueForKey:@"opacity"] integerValue];
	}
	
	if ([ bodyData valueForKey:@"index"]) {
        [item setVertexZ:[[ bodyData valueForKey:@"index"]floatValue]] ;
        
	}
    if ([ bodyData valueForKey:@"anchor"]) {
		if([[[ bodyData valueForKey:@"anchor"] valueForKey:@"x"] integerValue] != -1){
			item.anchorPoint = ccp([[[ bodyData valueForKey:@"anchor"] valueForKey:@"x"] floatValue], [[[ bodyData valueForKey:@"anchor"] valueForKey:@"y"] floatValue]);
		}
	}
    
	
	if ([ bodyData valueForKey:@"position"]) {
		if([[[ bodyData valueForKey:@"position"] valueForKey:@"x"] integerValue] == 0){
			item.position = ccp(winSize.width/2, winSize.height/2);
		}else{
			item.position = ccp([[[ bodyData valueForKey:@"position"] valueForKey:@"x"] floatValue], [[[ bodyData valueForKey:@"position"] valueForKey:@"y"] floatValue]);
		}
	}

    
//		item.flipX = YES;
	b2BodyType type = b2_dynamicBody;
	if ([[NSString stringWithFormat:@"%@",[ bodyData valueForKey:@"b_type"]] isEqualToString: @"static"]) {
		type = b2_staticBody;
	}else if([[NSString stringWithFormat:@"%@",[ bodyData valueForKey:@"b_type"]] isEqualToString: @"kinamatic"]) {
		type = b2_kinematicBody;
	}
	name = [name stringByReplacingOccurrencesOfString:@".png" withString:@""];
    name = [name stringByReplacingOccurrencesOfString:@".jpg" withString:@""];
		[item createBodyInWorld:world
								 b2bodyType:type
											angle:angle
								 allowSleep:true 
							fixedRotation:true
										 bullet:false
         ];
	[[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:name];
    if(parent ==NULL){
        [self addChild:item z:[[bodyData valueForKey:@"index"]integerValue]];
    }else{
        [parent addChild:item z:[[bodyData valueForKey:@"index"]integerValue]];
    }
    
    if ([[ bodyData valueForKey:@"isInterective"] boolValue]) {
        [interections addObject:(CCSprite *)[item body]->GetUserData()];
    }
    

    
    
//    Make Joints
    if ([ bodyData valueForKey:@"joints"]){
        for(id key in [ bodyData valueForKey:@"joints"]){
            NSMutableDictionary *join = (NSMutableDictionary *)[(NSMutableDictionary *)[bodyData valueForKey:@"joints"] valueForKey:key];
//            NSLog(@"Join details: %@",join);
            b2Body* bodyB = NULL;
            if ([[join valueForKey:@"B"] integerValue]>=0) {
                bodyB=[(TRBox2D*)[self getChildByTag:[[join valueForKey:@"B"] integerValue]] body];
            }
            if (bodyB || [[join valueForKey:@"B"] integerValue]<0) {
                
                if (bodyB == NULL) {
                    b2BodyDef myBodyDef;
                    myBodyDef.type = b2_staticBody; //this will be a static body
                    myBodyDef.position.Set([[[join valueForKey:@"pos"] valueForKey:@"x"] floatValue]/PTM_RATIO, [[[join valueForKey:@"pos"] valueForKey:@"y"] floatValue]/PTM_RATIO); //slightly lower position
                    bodyB = world->CreateBody(&myBodyDef); //add body to world
                    
                    
                    
                    
                    b2PolygonShape boxShape;
                    boxShape.SetAsBox(0.1,0.1);
                    b2FixtureDef boxFixtureDef;
                    boxFixtureDef.filter.categoryBits = 0x0000;
                    boxFixtureDef.filter.maskBits = 0x0000;
                    
                    boxFixtureDef.shape = &boxShape;
                    boxFixtureDef.density = 1;
                    bodyB->CreateFixture(&boxFixtureDef);
                }
                
                if ([[NSString stringWithFormat:@"%@",[ join valueForKey:@"type"]] isEqualToString: @"spring"]) {
//                    NSLog(@"string joint");
                    b2Body* bodyA = [item body];
                    [self springJoint:bodyA bodyB:bodyB join:join];
                    
                }else if ([[NSString stringWithFormat:@"%@",[ join valueForKey:@"type"]] isEqualToString: @"weld"]) {
//                    NSLog(@"weld joint");
                    b2Body* bodyA = [item body];
                    [self weldJoint:bodyA bodyB:bodyB join:join];
                }else if ([[NSString stringWithFormat:@"%@",[ join valueForKey:@"type"]] isEqualToString: @"rope"]) {
//                    NSLog(@"rope joint");
                    b2Body* bodyA = [item body];
                    [self ropeJoint:bodyA bodyB:bodyB join:join];
                    
                }else if([[NSString stringWithFormat:@"%@",[ join valueForKey:@"type"]] isEqualToString: @"prismatic"]){
                    [self prismaticJoint:[item body] bodyB:groundBody join:join];
                    
                }else if([[NSString stringWithFormat:@"%@",[ join valueForKey:@"type"]] isEqualToString: @"revolute"]){
                    if([[join valueForKey:@"fixedRotation"] boolValue]){
                        [item body]->SetFixedRotation(false);
                    }else{
                        bodyB->SetFixedRotation(false);
                    }
                    [self revoluteJoint:[item body] bodyB:bodyB join:join];
                }
            }
        }
    }
//    Make Bubbles
    if ([ bodyData valueForKey:@"bubbles"]) {
        CGPoint localPosition = CGPointMake([[[ bodyData valueForKey:@"bubbles"] valueForKey:@"x"]floatValue], [[[ bodyData valueForKey:@"bubbles"] valueForKey:@"y"]floatValue]);
        float endSize = [[[ bodyData valueForKey:@"bubbles"] valueForKey:@"size"]floatValue];
        [self createBubbles:CGPointMake([item body]->GetPosition().x+localPosition.x, [item body]->GetPosition().y+localPosition.y) index:2 sprite:item endsize:endSize];
    }
    
    if ([ bodyData valueForKey:@"child"]) {
        for(id key in [ bodyData valueForKey:@"child"]){
            NSMutableDictionary *data=  (NSMutableDictionary *) [[ bodyData valueForKey:@"child"] valueForKey:key];
            [self create:data tag:[key integerValue] parent:item];
        }
    }
}



-(void)initBodies{
    
    interections = NULL;
    interections = [[CCArray alloc]init];

	for(id key in pageData){
        NSMutableDictionary *data=  (NSMutableDictionary *) [pageData valueForKey:key];
        [self create:data tag:[key integerValue] parent:NULL];
	}
    
    if (parallax) {
        [self addChild:parallax];
    }

	[self schedule: @selector(tick:)];
}

-(id)initWithPage:(int)pgNo
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if(self=[super init]) {
        canChangeScene = NO;
        [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrames];
        [[CCTextureCache sharedTextureCache] removeAllTextures];
          
         [[CCDirector sharedDirector] purgeCachedData]; //Line added by Pritesh
        
        global = [GlobalSet globalSettings];
        // enable touches
        self.touchEnabled = YES;
		
		// enable accelerometer
        [self setAccelerometerEnabled:YES];
        [CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
        screenSize = [CCDirector sharedDirector].winSize;
        currentPage = pgNo;
        
        NSLog(@"init%d",currentPage);
        }
	return self;
}


-(void)canSwitch
{
    canChangeScene = YES;
}

-(void)onEnterTransitionDidFinish{
    
	//[[CCTextureCache sharedTextureCache] removeAllTextures];
    NSLog(@"enter transition");
    id gap = [CCActionInterval actionWithDuration:1];
    id play = [CCCallFuncN actionWithTarget:self selector:@selector(canSwitch)];
    [self runAction:[CCSequence actions:gap, play, nil]];
    
    
    [self initworld];
    _contactListener = new MyContactListener();
    world->SetContactListener(_contactListener);
    
    [self initground];
    
    if (currentPage == 50) {
        return;
    }
    
    
    //LOAD LEVEL SPECIFIC DATA
    [self loadPageData];//load data(pagedata,textdata,mediaData) from dataX.plist file
	[self loadLevelData];//load levelX.plist, ldataX.plist, & ldataX.png files
//    Render Items
	[self initBodies];
    [self AddGestures];
	llable = [CCLabelTTF labelWithString:@"" fontName:@"arial" fontSize:24];
    alable = [CCLabelTTF labelWithString:@"" fontName:@"arial" fontSize:24];
	llable.color = ccRED;
	llable.tag = 100;
    alable.color = ccBLACK;
	[self addChild:llable z:1000];
    [self addChild:alable z:102];
    
    
    if(currentPage==0 || currentPage==19)
    {[self menuSparkle ];}
    
    
    CCSprite *prev = [CCSprite spriteWithFile:@"arrow1.png"];
	if(NSClassFromString([NSString stringWithFormat:@"Page%d", currentPage-1]) != nil && currentPage!=13){
        prev.position = ccp(50, 30);
        prev.flipX = YES;
        prev.scale = 0.7;
        [self addChild:prev z:100];
        prev.tag = PRE;
        [interections addObject:prev];
    }
    
	if((NSClassFromString([NSString stringWithFormat:@"Page%d", currentPage+1]) != nil && currentPage!=13)){
		CCSprite *next = [CCSprite spriteWithTexture:prev.texture];
		if (currentPage ==0) {
			next = [CCSprite spriteWithFile:@"arrow1.png"];
			id move = [CCMoveBy actionWithDuration:0.2 position:CGPointMake(-10, 0)];
			[next runAction:[CCRepeatForever actionWithAction:[CCSequence actions:move, [move reverse], nil]]];
		}
        
		next.position = ccp(980, 30);
        next.scale = 0.7;
		[self addChild:next z:100];
		[interections addObject:next];
		next.tag = NEXT;
        
	}
    if (currentPage!=0) {
        [self addText];
        [self addButtons];  
    }
 
    if(global.MUSIC)
    {
    [self playSound];
    }
 
    if (global.NARRATEME&&currentPage!=13) {

        id gap = [CCActionInterval actionWithDuration:2];
        id play = [CCCallFuncN actionWithTarget:self selector:@selector(NarrateMe)];
        [self runAction:[CCSequence actions:gap, play, nil]];
    }
  
    [self  createSlideUpViewIfNecessary];
    
    
    CCSprite *menuIcon = [CCSprite spriteWithFile:@"home.png"];
	     menuIcon.position = ccp(72,667);
         
         [self addChild:menuIcon z:100];
         menuIcon.tag = 1311;
         [interections addObject:menuIcon];
    if (currentPage!=0 && currentPage!=19) {
        menuIcon.scale = 0.6;
        menuIcon.opacity=130;
         menuIcon.position = ccp(72,720);
    }else{
        menuIcon.scale = 1;
        
    }
   

}


-(void)showButtons:(BOOL)show
{
    if(!show){
        [[self getChildByTag:PLAY] setVisible:NO];
        [[self getChildByTag:STOP] setVisible:NO];
        [[self getChildByTag:REPLAY] setVisible:NO];
        [[self getChildByTag:-300] setVisible:NO];
        [[self getChildByTag:-301] setVisible:NO];
        [[self getChildByTag:-302] setVisible:NO];
    }
    
    else{
        if(global.NARRATEME){
            [[self getChildByTag:PLAY] setVisible:NO];
            [[self getChildByTag:STOP] setVisible:YES];
            [interections addObject:[self getChildByTag:STOP]];
        }else{
            [[self getChildByTag:PLAY] setVisible:YES];
            [[self getChildByTag:STOP] setVisible:NO];
            [interections addObject:[self getChildByTag:PLAY]];
            
        }
    }
    
}


-(void)textMoveOut:(int)time{
    
    
    float sign1=1.0;
    float sign2=1.0;
    int signSelect1 = arc4random()%2;
    int signSelect2 = arc4random()%2;
    
    if(signSelect1)
    {
        sign1=-1.0;
    }
    if(signSelect2)
    {
        sign2=-1.0;
    }
    
    CGPoint pos= CGPointMake((2000.00)*sign1 ,(768.00)*sign2);
    
    id moveText = [CCMoveTo actionWithDuration:time position:pos];
    
    [[self getChildByTag:-200] runAction:moveText];
    [[self getChildByTag:-201] runAction:[[moveText copy] autorelease]];
    
    
}


-(void)textMoveIn
{
    
    id moveText = [CCMoveTo actionWithDuration:1 position:CGPointMake(textXPos, textYPos)];
    
    [[self getChildByTag:-200] runAction:moveText];
    [[self getChildByTag:-201] runAction:[[moveText copy] autorelease]];
    
    [self getChildByTag:-200].visible=YES;
    [self getChildByTag:-201].visible=YES;
    
}


-(void)cdAudioSourceDidFinishPlaying:(CDLongAudioSource *)audioSource{
    
    [self getChildByTag:STOP].visible = NO;
    
    if ([self getChildByTag:-200].visible) {
        [self textMoveOut:1];
    }
}

-(void)addButtons
{
    if (global.TEXT&&(currentPage!=13)) {
//        [self textMoveIn];
        [self performSelector:@selector(textMoveIn) withObject:nil afterDelay:0.8f];
        
    }
    
    CCSprite* bg = [CCSprite spriteWithFile:@"bg.png"];
    bg.position = ccp(512,384);
    bg.tag = BG;
    bg.scale = 0;
    [self addChild:bg z:111];
    [interections addObject:bg];
    
    CCSprite* resume = [CCSprite spriteWithFile:@"resume.png"];
    resume.position = ccp(522,448);
    resume.tag = RESUME;
    resume.scale = 0;
    [self addChild:resume z:112];
    [interections addObject:resume];
    CCSprite* restart = [CCSprite spriteWithFile:@"restart.png"];
    restart.position = ccp(531,322);
    restart.tag = RESTART;
    restart.scale = 0;
    [self addChild:restart z:112];
    [interections addObject:restart];
   
    
    CCSprite* stop = [CCSprite spriteWithFile:@"pause.png"];
    stop.position = ccp(920,731);
    stop.scale =0.9;
    stop.tag = STOP;
    [self addChild:stop z:110];
    
    CCSprite* replay = [CCSprite spriteWithFile:@"replay.png"];
    replay.position = ccp(990,731);
    replay.scale = 1;
    replay.tag = REPLAY;
    [self addChild:replay z:110];
    [interections addObject:replay];
   
    if(!global.NARRATEME ){
        stop.visible = NO;
     }
    

    

}

-(void)toggleResumeWindow:(BOOL)show{
    id bg, resume, restart;
    if (show) {
        bg = [CCScaleTo actionWithDuration:0.1 scale:1];
        resume = [CCScaleTo actionWithDuration:0.1 scale:1];
        restart = [CCScaleTo actionWithDuration:0.1 scale:1];
    }else{
        bg = [CCScaleTo actionWithDuration:0.1 scale:0];
        resume = [CCScaleTo actionWithDuration:0.1 scale:0];
        restart = [CCScaleTo actionWithDuration:0.1 scale:0];
    }
    [[self getChildByTag:BG] runAction:bg];
    [[self getChildByTag:RESUME] runAction:resume];
    [[self getChildByTag:RESTART] runAction:restart];
}



-(void)addText
{


   CCLabelTTF *text;
    for(id key in textData){
		
		NSMutableDictionary *data=  (NSMutableDictionary *) [textData valueForKey:key];
        
        //SET Dimensions
		float width = [[[ data valueForKey:@"dimension"] valueForKey:@"width"] floatValue];
		float height = [[[ data valueForKey:@"dimension"] valueForKey:@"height"] floatValue];
		CGSize dimension = CGSizeMake(width, height);
        
        //Set Position
		CGPoint position = ccp([[[ data valueForKey:@"position"] valueForKey:@"x"] floatValue], [[[ data valueForKey:@"position"] valueForKey:@"y"] floatValue]);
       
        textXPos = position.x;
        textYPos = position.y;
        
		NSString *font = @"Doctor-Soos-Bold";//Default Font
     //   NSString *font = @"arial";
		if ([ data valueForKey:@"font"]) {
			font = [[ data valueForKey:@"font"] valueForKey:@"font1"]; //Selected Font
		}
		
        
		ccColor3B color = ccc3(0, 0, 0);
		if ([ data valueForKey:@"color"]) {
			color = ccc3([[[ data valueForKey:@"color"] valueForKey:@"r"] integerValue], [[[ data valueForKey:@"color"] valueForKey:@"g"] integerValue], [[[ data valueForKey:@"color"] valueForKey:@"b"] integerValue]);
		}
		
		float fontSize = 28;
		if ([ data valueForKey:@"fontSize"]) {
			fontSize = [[ data valueForKey:@"fontSize"] floatValue] ;
		}
		
		CCTextAlignment  alignment = kCCTextAlignmentLeft;
		if ([ data valueForKey:@"alignment"]) {
            //			NSLog(@"%@", [ data valueForKey:@"alignment"]);
			if ([[NSString stringWithFormat:@"%@",[ data valueForKey:@"alignment"]] isEqualToString: @"right"]) {
				alignment = kCCTextAlignmentRight;
			}else if([[NSString stringWithFormat:@"%@",[ data valueForKey:@"alignment"]] isEqualToString: @"center"]){
                alignment = kCCTextAlignmentCenter;
			}
		}
        NSString* string = @"";
        
        
        
		
		switch(global.language)//Select Text Language
        {
            case 1:
		        string = [NSString stringWithString:[ [data valueForKey:@"text"] valueForKey:@"English"]];
                break;
            case 2:
                string = [NSString stringWithString:[ [data valueForKey:@"text"] valueForKey:@"Dansk"]];
                break;
            case 3:
                string = [NSString stringWithString:[ [data valueForKey:@"text"] valueForKey:@"Svensk"]];
                break;
            case 4:
                string = [NSString stringWithString:[ [data valueForKey:@"text"] valueForKey:@"NORSK"]];
                break;
        }
        
        
        	//	NSLog(@"text: %@", string);
        
		text = [CCLabelTTF labelWithString:string fontName:font fontSize:fontSize dimensions:dimension hAlignment:alignment];
		text.position = position;
		text.color = color;
        text.tag = -200;
        text.visible=NO;
              
        
     
        
        CCSprite *textBG = (CCSprite *)[CCSprite spriteWithFile:@"textbg.png"];
         
        float scalex = (text.boundingBox.size.width)/(textBG.boundingBox.size.width) + 0.18;
        float scaley = (text.boundingBox.size.height)/(textBG.boundingBox.size.height) + 0.4;
        
        textBG.position = position ;
        
        textBG.scaleX = scalex;
        textBG.scaleY = scaley;
     //   textBG.opacity = 180;
        textBG.tag = -201;
        textBG.visible=NO;
        
        
		[self addChild:text z:56];
        [interections addObject:text];

        [self addChild:textBG z:55];
        [interections addObject:textBG];
        
        [self textMoveOut:0];
        
     
	
    }

}




-(void)NarrateMe{
    
    narrationSound = [NSString stringWithCString:narrationArray encoding:NSASCIIStringEncoding];
    
    if(narrationSound){
        NSLog(@"narration sound initialized");
       
    [self getChildByTag:PLAY].visible = NO;
    [interections removeObject:[self getChildByTag:PLAY]];
    
    [self getChildByTag:STOP].visible = YES;
    [interections addObject:[self getChildByTag:STOP]];
    
    if (!rightChannel) {
        rightChannel = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Right];
        rightChannel.delegate = self;
    
        [rightChannel load:narrationSound];
      [rightChannel setVolume:10];  
    }
       
    [rightChannel play];
    
      }
   else { NSLog(@"narration sound not initialize");}
    
   }



-(void)stopNarration{
    if (rightChannel) {
         
        [rightChannel stop];
               
      //  rightChannel = NULL;
        
        [self getChildByTag:STOP].visible = NO;
        [interections removeObject:[self getChildByTag:STOP]];
        
        [self getChildByTag:PLAY].visible = YES;
        [interections addObject:[self getChildByTag:PLAY]];
        
       

    }
    
}

-(void)playSound{
    
    if(bgSound){
        
        NSLog(@"Background sound initialized");
    leftChannel = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
  
    [leftChannel load:bgSound];
    [leftChannel setBackgroundMusic:YES];
    [leftChannel setNumberOfLoops:-1];
    [leftChannel setVolume:0.5];
    [leftChannel play];
    
    
    }else{NSLog(@"Background sound not initialize"); }
    
}





-(void)draw
{
    [super draw];
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
    kmGLPushMatrix();
    self.world->DrawDebugData();
    kmGLPopMatrix();
    
//	// Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
//	// Needed states:  GL_VERTEX_ARRAY, 
//	// Unneeded states: GL_TEXTURE_2D, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
//	glDisable(GL_TEXTURE_2D);
//	//glDisableClientState(GL_COLOR_ARRAY);
//	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
//	
////	world->DrawDebugData();
//	
//	// restore default GL states
//	glEnable(GL_TEXTURE_2D);
//	//glEnableClientState(GL_COLOR_ARRAY);
//	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	
}

-(void) tick: (ccTime) dt
{
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
	//Iterate over the bodies in the physics world
    if (parallax!=NULL) {
        [parallax updateWithVelocity:ccp(-4,0) AndDelta:dt];
    }
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
		{
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body

			
			CCSprite *myActor = (CCSprite*)b->GetUserData();
			//			NSLog(@"velocity: %f", b->GetLinearVelocity().x);
			if ((myActor.position.x<=0)&&(currentPage !=1)) {
				b->SetTransform(b2Vec2(screenSize.width/PTM_RATIO, b->GetPosition().y ), 0);
			}
			myActor.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
			myActor.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
            }	
		}
 
  
}



-(void)setTouched:(CGPoint)point{
    
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		b2Fixture* stFix = b->GetFixtureList();
        while(stFix != 0)
        {
			if(stFix->TestPoint(b2Vec2(point.x/PTM_RATIO, point.y/PTM_RATIO)))
            {
                CCSprite* sprite = (CCSprite*)b->GetUserData();
                if(sprite.zOrder> touchedSprite.zOrder){
                    touchedBody = b;
                    touchedSprite = sprite;
                }
            }
			stFix = stFix->GetNext();
        }
    }
        //    LOOK FOR NONPHYSICAL INTERACTIONS
        for (CCSprite *sprite in self.interections) {
            point = location;
            if ([sprite.parent isKindOfClass:[CCSprite class]]) {
                point = [sprite.parent convertToNodeSpace: point];
            }
            
            if (CGRectContainsPoint([sprite boundingBox], point)) {
                if (!touchedSprite || (touchedSprite && sprite.zOrder > touchedSprite.zOrder)) {
                    touchedSprite = sprite;
//                    NSLog(@"Selected %d", sprite.tag);
                    touchedBody = NULL;
                }
                if (sprite.tag == NEXT || sprite.tag == PRE || sprite.tag == HOME) {
                    break;
                }
                //			[anchorx setValue:sprite.anchorPoint.x];
                //			[anchory setValue:sprite.anchorPoint.y];
            }
        }

}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

	//Add a new body/atlas sprite at the touched location
    NSLog(@"touched");
    
    
	for( UITouch *touch in touches ) {
		location = [touch locationInView: [touch view]];
		
		location = [[CCDirector sharedDirector] convertToGL: location];
//		[self getChildByTag:100].visible = false;
        
		if (mouseJoint != NULL) return;
		
		//noticed that in this case the location is not trasformed to box2d coordinates
		[self setTouched:location];
        
        if (touchedBody && [self.interections containsObject:touchedSprite]) {
            mbodytype = touchedBody->GetType();
            touchedBody->SetType(b2_dynamicBody);
        }
        
        if(touchedBody && touchedBody->GetType() == b2_dynamicBody && [self.interections containsObject:touchedSprite])
			{
			//notice how the location is not transformed in box2d coordinates
			[self createMouseJoint:location];
			}
	}
   


}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
	UITouch *myTouch = [touches anyObject];
	location = [myTouch locationInView:[myTouch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
//    if (touchedSprite) {
//        touchedSprite.position = location;
//        NSLog(@"Tag %d", touchedSprite.tag);
//    }
//    [self getChildByTag:100].position = location;
//    [self getChildByTag:100].visible = true;
    if (mouseJoint){
	//BOX2d WAY - WILL NOT BEHAVE THE SAME ON ALL DEVICES
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        mouseJoint->SetTarget(locationWorld);//
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
//    CGPoint pos = location;//[touchedSprite.parent convertToNodeSpace: touchedSprite.position];
//    [(CCLabelTTF *)[self getChildByTag:100] setString:[NSString stringWithFormat: @"X=%.2f, Y=%.2f", pos.x, pos.y]];

//
	if (mouseJoint)
    {
        b2Body* body = mouseJoint->GetBodyB();
		world->DestroyJoint(mouseJoint);
		mouseJoint = NULL;
        body->SetType(mbodytype);
    }
    
    
    if(thumbViewShowing){
        [self toggleThumbView];}
       
    touchedSprite=NULL;
    touchedBody = NULL;
  
}


-(void)sceneChange:(CCSprite*)touchd{
    
    if(touchd.tag == NEXT){
        
        [self sceneChangeTo:currentPage+1];
        
    }else if(touchd.tag	== PRE){
        [self sceneChangeTo:currentPage-1];
    }
  
}


-(void)beforeSceneChange
{
    self.touchEnabled = NO;
    [self stopAllGestureRecognizers];
    global.MENU =NO;
    [upperView removeFromSuperview];
    [upperView release];
    upperView = NULL;
    [thumbScrollView removeFromSuperview];
    [thumbScrollView release];
    thumbScrollView = NULL;
    [slideUpView removeFromSuperview];
    [slideUpView release];
    slideUpView = NULL;
    
    
    [self stopAllActions];
    [rightChannel stop];
    rightChannel = NULL;
    [leftChannel stop];
    leftChannel = NULL;
    
    [self unscheduleAllSelectors];
    [[[CCDirector sharedDirector] actionManager] removeAllActions];
    
    
    
    delete world;
    world = NULL;
    delete m_debugDraw;
    delete _contactListener;
   
   


}

-(void)sceneChangeTo:(int)pageNo
{
    if (canChangeScene == NO) {
        return;
    }
    NSLog(@"Current Page before change %d", currentPage);
    [self beforeSceneChange];
    Class next=nil;
    next = NSClassFromString([NSString stringWithFormat:@"Page%d",pageNo]);

//  [[CCDirector sharedDirector] replaceScene:[ CCTransitionMoveInR transitionWithDuration:0.1 scene:[next scene]]];
     [[CCDirector sharedDirector] replaceScene:[next scene]];
     touchedSprite = NULL;

}

-(void)replay{
    if (global.NARRATEME) {
        [self stopNarration];
        rightChannel = NULL;
        //   global.NARRATEME = YES;
        narrationSound = [NSString stringWithCString:narrationArray encoding:NSASCIIStringEncoding];
        [self NarrateMe];
    }
    if (global.TEXT && [self getChildByTag:-200]) {
        [self textMoveIn];
    }
}

-(void)playbutton{
    [[SimpleAudioEngine sharedEngine] playEffect:@"playbutton.mp3"];
    touchedSprite=NULL;
    touchedBody=NULL;
}


-(void)menuSparkle
{
    
    if(global.MENU)
    {
       
        [self sparkling:self from:4 till:17];
        
        for(int i = 4; i<=17;i++)
        {
            [[self getChildByTag:i] setVisible:TRUE];
        }
        
    }else{
        for(int i = 4; i<=17;i++)
        {
            [[self getChildByTag:i] setVisible:FALSE];
            [[self getChildByTag:i] stopAllActions];
        }
    }
    
}


-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer {
    
    CCSprite* sprite = nil;
    
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight && (NSClassFromString([NSString stringWithFormat:@"Page%d", currentPage-1]) != nil && currentPage!=13)) {
        sprite = (CCSprite *)[self getChildByTag:PRE];
    }
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft && (NSClassFromString([NSString stringWithFormat:@"Page%d", currentPage+1]) != nil && currentPage!=13)) {
        sprite = (CCSprite *)[self getChildByTag:NEXT];
    }

    if (sprite.tag == NEXT || sprite.tag == PRE) {
        
        [self sceneChange:sprite];
        [[SimpleAudioEngine sharedEngine] playEffect:@"P2_1_book paper.mp3"];
    }
}


- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    
//    CGPoint pos = location;// [touchedSprite.parent convertToNodeSpace: location];
//    if (touchedSprite) {
//        [(CCLabelTTF *)[self getChildByTag:100] setString:[NSString stringWithFormat: @"X=%.2f, Y=%.2f", pos.x, pos.y]];
//        [self getChildByTag:100].visible = true;
//    }

    
	if (mouseJoint)
    {
        b2Body* body = mouseJoint->GetBodyB();
		world->DestroyJoint(mouseJoint);
		mouseJoint = NULL;
        body->SetType(mbodytype);
    } 


if([self getChildByTag:-320] || [self getChildByTag:BG].scale >0)
  {
   if ([self getChildByTag:-320]) {
      [self toggleCredits];
      touchedSprite=NULL;
    }
  else if ([self getChildByTag:BG].scale>0 && touchedSprite){
      if (touchedSprite.tag == RESTART) {
                [self sceneChangeTo:0];
                [self playbutton];
            }else if(touchedSprite.tag == RESUME){
                [self NarrateMe];
                [self toggleResumeWindow:NO];
                [self playbutton];
            }
    }
    
}

else if (touchedSprite){
        
 switch(touchedSprite.tag){
            
   case NEXT:case PRE:{  //NEXT OR PREVIOUS Arrow tapped
       
     [self sceneChange:touchedSprite];
     [[SimpleAudioEngine sharedEngine] playEffect:@"P2_1_book paper.mp3"];
     break;
     }
        
   case PLAY:{  //Play Button Tapped
       narrationSound = [NSString stringWithCString:narrationArray encoding:NSASCIIStringEncoding];
       //    global.NARRATEME = YES;
       [self NarrateMe];
       [self playbutton];
       break;
     }
            
   case STOP:{     //Stop Button Tapped
       //   global.NARRATEME = NO;
       [self stopNarration];
       [self toggleResumeWindow:YES];
       [self playbutton];
        break;
     }
                
   case REPLAY:{    //Replay Button Tapped
        [self replay];
        [self playbutton];
        break;

        }
    
   case -302:{
        [self toggleThumbView];
        [self playbutton];
         break;

        }
        
   case 1311:{  //Application Icon tapped
                    
        [self pressIcon:1311];
           if(!global.MENU){      //If menu not showing then show it
               [self showHomeMenu];
               global.MENU = YES;
               [self playbutton];
              }
            else{                   //if menu showing hide it.
               global.MENU = NO;
               [self hideHomeMenu];
               //[self removeLanguages];
               [self playbutton];
              }
        
        
           if(currentPage==0 || currentPage==19){ //Menu sparkling only for 0th and 19th Page
                [self menuSparkle];
                }
              
         break;
        }
                
  case NARATIONOFF: case NARATIONON: {  //Narration Option tapped from menu.
        [self playbutton];
           if (global.NARRATEME) {   //If Narration on then set it off
                global.NARRATEME = NO;
                [self pressIcon:101];
                [[self getChildByTag:111] setVisible:NO];  //Narration ON option hide
                [[self getChildByTag:101] setVisible:YES]; //Narration OFF option show
               
                [interections addObject:[self getChildByTag:101]];   //Narration OFF option set interactive ON.
                [interections removeObject:[self getChildByTag:111]];//Narration ON option set interactive off.
             
                if(currentPage!=0 && currentPage!=19)  //No narrations and buttons for 0th and 19th Page
                { 
                    [self stopNarration];
                    [self updateButtons];
                    
                }
                
             }else{              //If Narration OFF then set it ON
                 global.NARRATEME = YES;
               // global.TEXT = YES;
                
                [self pressIcon:111];
                [[self getChildByTag:111] setVisible:YES];   //Narration ON option show
                [[self getChildByTag:101] setVisible:NO];    //Narration OFF option hide
                 
                [interections addObject:[self getChildByTag:111]];    //Narration ON option set interactive ON.
                [interections removeObject:[self getChildByTag:101]]; //Narration OFF option set interactive OFF.
              
                 if(currentPage!=0 && currentPage!=19)  //No narrations and buttons for 0th and 19th Page
                  { [self NarrateMe];
                    [self updateButtons];
                    }
            }
           
          break;
        }
                    
            
  case 133: case 103:{   //TEXT ON/OFF Toggle
        
            [self toggleText];
            [self playbutton];
            [self updateButtons];
            break;
        }
    
           
  case 114:{
            [self pressIcon:114];
            [self toggleCredits];
            [self playbutton];
                break;

        }
            
  case-313: {  //CHOOSE PAGE
            [self pressIcon:-313];
            [self toggleThumbView];
            [self playbutton];
            break;
        }
         
                
  case 199:{
            [self pressIcon:199];
            [self sceneChangeTo:0];
            [self playbutton];
                break;
        }

  case -200:case -201:{
            [self textMoveOut:1];
            [self playbutton];
                break;
        }

      } //End Of Switch
   } //End of IF(touchedSprite)

}//End of Tapped

- (NSArray *)imageNames {
	
	// the filenames are stored in a plist in the app bundle, so create array by reading this plist
	NSString *path = [[NSBundle mainBundle] pathForResource:@"Images" ofType:@"plist"];
	NSData *plistData = [NSData dataWithContentsOfFile:path];
	NSString *error; NSPropertyListFormat format;
	NSArray *imageNames = [NSPropertyListSerialization propertyListFromData:plistData
                                                           mutabilityOption:NSPropertyListImmutable
                                                                     format:&format
                                                           errorDescription:&error];
	if (!imageNames) {
		NSLog(@"Failed to read image names. Error: %@", error);
	}
	
	return imageNames;
}


- (void)createThumbScrollViewIfNecessary {
	if (!thumbScrollView) {
		float scrollViewHeight = THUMB_HEIGHT + THUMB_V_PADDING;
		thumbScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kScrollWidth, scrollViewHeight)];
		[thumbScrollView setCanCancelContentTouches:YES];
		[thumbScrollView setClipsToBounds:NO];
		thumbScrollView.scrollEnabled = YES;
		thumbScrollView.userInteractionEnabled = YES;
		// now place all the thumb views as subviews of the scroll view
		// and in the course of doing so calculate the content width
		float xPosition = THUMB_H_PADDING;
		for (NSString *name in [self imageNames]) {
			UIImage *thumbImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_thumb.jpg", name]];
			if (thumbImage) {
				ThumbImageView *thumbView = [[[ThumbImageView alloc] initWithImage:thumbImage] autorelease];
				[thumbView setDelegate:self];
				[thumbView setImageName:name];

				CGRect frame = [thumbView frame];
				frame.origin.y = THUMB_V_PADDING;
				frame.origin.x = xPosition;
				[thumbView setFrame:frame];
				[thumbView setHome:frame];
				[thumbScrollView addSubview:thumbView];
				xPosition += (frame.size.width + THUMB_H_PADDING);
			}
		}
		[thumbScrollView setContentSize:CGSizeMake(xPosition, scrollViewHeight)];
        
        
	}
}

- (void)createSlideUpViewIfNecessary {
	if (!slideUpView) {
        
        
        
		[self createThumbScrollViewIfNecessary];
		//		CGRect bounds = [[self view] bounds];
		float thumbHeight = [thumbScrollView frame].size.height;
		// create container view that will hold scroll view and label
		CGRect frame = CGRectMake(0, kScrollHeight, kScrollWidth, thumbHeight);
		slideUpView = [[UIView alloc] initWithFrame:frame];
		[slideUpView setBackgroundColor:[UIColor blackColor]];
		[slideUpView setOpaque:NO];
		[slideUpView setAlpha:0.75];
        
        [[[CCDirector sharedDirector] view] addSubview:slideUpView];
		// add subviews to container view
		[slideUpView addSubview:thumbScrollView];
        
        
	}
}


- (void)toggleThumbView
{
    
	[self createSlideUpViewIfNecessary]; // no-op if slideUpView has already been created
	CGRect frame = [slideUpView frame];
	if (thumbViewShowing) {
		frame.origin.y += frame.size.height ;
        [self AddGestures];
	} else {
		frame.origin.y -= frame.size.height ;
        [self stopAllGestureRecognizers];
	}
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[slideUpView setFrame:frame];
	[UIView commitAnimations];
	thumbViewShowing = !thumbViewShowing;
    
    
}





- (void)hideThumbView {
	//	[self createSlideUpViewIfNecessary]; // no-op if slideUpView has already been created
	if (thumbViewShowing) {
		CGRect frame = [slideUpView frame];
		frame.origin.y += frame.size.height;
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3];
		[slideUpView setFrame:frame];
		[UIView commitAnimations];
		thumbViewShowing = NO;
	}
}


- (void)thumbImageViewWasTapped:(ThumbImageView *)tiv {
	int pageNo = nil;
    pageNo = [[tiv imageName] intValue];
	[self toggleThumbView];
	NSLog(@"PAGE NO TAPPED %d", pageNo);
    
    [self sceneChangeTo:pageNo];
}


-(void)updateButtons
{
    
    
    [[self getChildByTag:PLAY] setVisible:NO];
    [[self getChildByTag:STOP] setVisible:NO];
    [[self getChildByTag:REPLAY] setVisible:NO];
       

     CCSprite *play =(CCSprite *)[self getChildByTag:PLAY];
     CCSprite *stop =(CCSprite *)[self getChildByTag:STOP];
    CCSprite *replay =(CCSprite *)[self getChildByTag:REPLAY];
   
   if(global.NARRATEME && global.TEXT){
       stop.visible = YES;
       replay.visible = YES;
//       [interections addObject:stop];
//       [interections addObject:replay];
   }else if(global.NARRATEME){
       stop.visible = YES;
       replay.visible = YES;
//       [interections addObject:stop];
//       [interections addObject:replay];
    }else if(global.TEXT){
        stop.visible = NO;
        replay.visible = YES;
//        [interections removeObject:stop];
//        [interections addObject:replay];
    }else{
        stop.visible = NO;
        replay.visible = NO;
//        [interections removeObject:stop];
//        [interections removeObject:replay];
    }


}


-(void)showHomeMenu
{
    
    float y=70;
    
//    CCSprite *title =(CCSprite *)[CCSprite spriteWithFile:@"MenuIcon.png"];
//    title.position = ccp(66,620);
//    title.tag=-18;
//    [self addChild: title z:100];
    
    if (currentPage!=0) {
        CCSprite *home =(CCSprite *)[CCSprite spriteWithFile:@"MenuIcon.png"];
        home.position = ccp(200,560);
        home.tag=GOHOME;
        [interections addObject:home];
        [self addChild: home z:100];
        y=0;
    }
    
    CCSprite *pages =(CCSprite *)[CCSprite spriteWithFile:@"pageIcon.png"];
    pages.position = ccp(200,477+y);
    pages.tag=PAGEICON;
    [interections addObject:pages];
    [self addChild: pages z:100];
    
    
    
    CCSprite *icon1 = [CCSprite spriteWithFile:@"NarrationOff.png"] ;
    icon1.position = ccp(200,399+y);
    icon1.tag = NARATIONOFF;
    
    CCSprite *icon11 = [CCSprite spriteWithFile:@"NarrationOn.png"];
    icon11.tag = NARATIONON;
    icon11.position = ccp(200,399+y);
    
    [icon1 setVisible:NO];
    
    if (global.NARRATEME) {
        [icon1 setVisible:NO];
        [icon11 setVisible:YES];
        [interections addObject:icon11];
    }else{
        
        [icon11 setVisible:NO];
        [icon1 setVisible:YES];
        [interections addObject:icon1];
    }
    [self addChild:icon1 z:100];
    [self addChild:icon11 z:100];
    
    
    
    CCSprite *icon3 = [CCSprite spriteWithFile:@"TextOn.png"];
    icon3.position = ccp(200,319+y);
    icon3.tag=TEXTON;
    [self addChild:icon3 z:100];
    
    CCSprite *icon33 = [CCSprite spriteWithFile:@"TextOff.png"];
    icon33.position = ccp(200,319+y);
    icon33.tag=TEXTOFF;
    [self addChild: icon33 z:100];
    
    
    if (global.TEXT) {
        [icon33 setVisible:NO];
        [icon3 setVisible:YES];
        [interections addObject:icon3];
    }else{
        [icon3 setVisible:NO];
        [icon33 setVisible:YES];
        [interections addObject:icon33];
        
        
    }
    
    
    
    CCSprite *credits = [CCSprite spriteWithFile:@"CreditIcon.png"];
    credits.position = ccp(200,244+y);
    credits.tag=CREDITICON;
    [interections addObject:credits ];
    
    [self addChild: credits z:100];
    
    
}


-(void)hideHomeMenu
{
    if (currentPage!=0) {
        [interections removeObject:[self getChildByTag:199]];
        [self removeChildByTag:199 cleanup:YES];
    }
    
    [interections removeObject:[self getChildByTag:101]];//Narration
    [self removeChildByTag:101 cleanup:YES];
    [interections removeObject:[self getChildByTag:111]];
    [self removeChildByTag:111 cleanup:YES];
    
    [interections removeObject:[self getChildByTag:103]];//Text
    [self removeChildByTag:103 cleanup:YES];
    [interections removeObject:[self getChildByTag:133]];
    [self removeChildByTag:133 cleanup:YES];
    
    [interections removeObject:[self getChildByTag:114]]; //Credits
    [self removeChildByTag:114 cleanup:YES];
    
    [interections removeObject:[self getChildByTag:-18]]; //Title
    [self removeChildByTag:-18 cleanup:YES];
    
    
    
    [interections removeObject:[self getChildByTag:-313]];
    [self removeChildByTag:-313 cleanup:YES];
}






-(void)pressIcon:(int)tag
{
    
    id scaleIn = [CCScaleTo actionWithDuration:0.1 scale:0.9];
    id scaleOut = [CCScaleTo actionWithDuration:0.05 scale:1];
    if (tag ==1311 && currentPage !=0) {
        scaleIn = [CCScaleTo actionWithDuration:0.1 scale:0.5];
        scaleOut = [CCScaleTo actionWithDuration:0.05 scale:0.6];
    }
    
    id pressAction = [CCSequence actions:scaleIn,scaleOut,nil];
    
    [[self getChildByTag:tag] runAction:pressAction];
    
}






-(void)toggleText
{
    if(global.TEXT)
    {
        [[self getChildByTag:103] setVisible:NO];
        [[self getChildByTag:133] setVisible:YES];
        [self pressIcon:133];
        global.TEXT=NO;
        [self textMoveOut:1];
    }else{
        [[self getChildByTag:103] setVisible:YES];
        [[self getChildByTag:133] setVisible:NO];
        [self pressIcon:103];
        [self textMoveIn];
        global.TEXT=YES;
    }
    
}


-(void)toggleCredits
{
    
    if([self getChildByTag:-320])
    {
        id scaleIn = [CCScaleTo actionWithDuration:0.3 scale:0];
        id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)[self getChildByTag:-320]];
        id action = [CCSequence actions:scaleIn,remove, nil];
        
        [[self getChildByTag:-320] runAction:action];
        
    }else{
        
        CCSprite *Credits = [CCSprite spriteWithFile:@"Credits.png"];
        Credits.position = ccp(600,384);
        Credits.tag=-320;
        Credits.scale=0;
        [self addChild:Credits z:10];
        id scaleOut = [CCScaleTo actionWithDuration:0.3 scale:0.8];
        
        [Credits runAction:scaleOut];
        
    }
    
    
}

- (void) stopAllGestureRecognizers{
    
    NSArray *grs = [[[CCDirector sharedDirector] view] gestureRecognizers];
    
    for (UIGestureRecognizer *gesture in grs){
        if([gesture isKindOfClass:[UITapGestureRecognizer class]] or [gesture isKindOfClass:[UISwipeGestureRecognizer class]]){
            [[[CCDirector sharedDirector] view] removeGestureRecognizer:gesture];
            
        }
    }
}


- (void) dealloc
{
    

    groundBody = NULL;
    mouseJoint = NULL;
    parallax = NULL;
    pageData = NULL;
    textData = NULL;
    mediaData = NULL;
    touchedBody = NULL;
    touchedSprite = NULL;
    parallax = NULL;
    
    [interections release];
    interections = NULL;
    
   
    
    //*******
    [self stopAllActions];
    
    [self stopAllGestureRecognizers];
	
    [self removeAllChildrenWithCleanup:YES];
    
	// don't forget to call "super dealloc"
	[super dealloc];
    
    
    
}

@end
