//
//  Global.h
//  The Perfect Shape-Children's Story Book
//
//  Created by Rohit on 25/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalSet : NSObject
{
 
    bool NARRATEME;
    bool TEXT;
    bool MUSIC;
    int language;
    bool MENU;
    bool FIRST_ATTEMPT;
    
    

}
    
+(GlobalSet *)globalSettings;



@property(assign, nonatomic)bool MUSIC;
@property(assign, nonatomic)int language;
@property(assign, nonatomic)bool NARRATEME;
@property(assign, nonatomic)bool TEXT;
@property(assign, nonatomic)bool MENU;
@property(assign, nonatomic)bool FIRST_ATTEMPT;





@end