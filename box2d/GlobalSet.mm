//
//  Global.m
//  The Perfect Shape-Children's Story Book
//
//  Created by Rohit on 25/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GlobalSet.h"
#import "GameConfig.h"


@implementation GlobalSet
@synthesize NARRATEME;
@synthesize TEXT;
@synthesize MUSIC;
@synthesize language;
@synthesize MENU;
@synthesize FIRST_ATTEMPT;



static GlobalSet *_globalSettings;

+(GlobalSet *)globalSettings
{
    
    if (!_globalSettings){
        _globalSettings = [[GlobalSet alloc] init];
        _globalSettings.NARRATEME=YES;
        _globalSettings.TEXT=YES;
        _globalSettings.MUSIC=YES;
        _globalSettings.language=1;
        _globalSettings.MENU = NO;
        _globalSettings.FIRST_ATTEMPT=YES;
        

    }
    return _globalSettings;
}






@end