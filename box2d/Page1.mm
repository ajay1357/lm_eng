//
//  HelloWorldLayer.mm
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "Page1.h"
//#import "ControlLayer.h"


// HelloWorldLayer implementation
@implementation Page1
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//ControlLayer *controlls = [[ControlLayer alloc] initWithLayer];
//	[scene addChild:controlls z:5];
	// 'layer' is an autorelease object.
	Page1 *layer = [Page1 node];

	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}


//PAGE 1 BEGINS

-(void)zoomOut{
    id scale = [CCScaleTo actionWithDuration:4 scale:1];
    id showall = [CCCallFunc actionWithTarget:self selector:@selector(showAll)];
    [[self getChildByTag:0] runAction:[CCSequence actions:scale, showall, nil]];
}

-(void)popup:(id)item{
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id scaleflower = [CCSequence actions:scaleup, scaledown, nil];
    [item runAction:scaleflower];
    
}

-(void)showAll{
    
    for (int i=3; i<9; i++) {
        id fadein = [CCFadeIn actionWithDuration:1];
        if (i==8) {
            id wait = [CCActionInterval actionWithDuration:10];
            id showdoughter = [CCCallFunc actionWithTarget:self selector:@selector(showDoughter)];
            id sparkling = [CCCallFunc actionWithTarget:self selector:@selector(sparkling)];
            [[self getChildByTag:i] runAction:[CCSequence actions:fadein, sparkling, wait, showdoughter, nil]];
        }else{
            [[self getChildByTag:i] runAction:fadein];
        }
        
    }
     id fadein = [CCFadeIn actionWithDuration:0];
     [[[self getChildByTag:3] getChildByTag:33] runAction:fadein];
}



-(void)showDoughter{
    id fadein = [CCFadeIn actionWithDuration:0.5];
    [[self getChildByTag:2] runAction:fadein];
}


-(void)sparkling{
    [self sparkling:CGPointMake(250, 125) tag:20 sprite:(TRBox2D *)[self getChildByTag:3] startSize:40 posvar:CGPointMake(50, 65) life:5 erate:15 tItem:50];
    
    CCSprite *kingSprite = (CCSprite *)[self getChildByTag:3];
    
    id fadein = [CCFadeTo actionWithDuration:0 opacity:255];
    id scalel = [CCScaleTo actionWithDuration:1 scale:1];
    id scales = [CCScaleTo actionWithDuration:1 scale:0];
    id scale = [CCSequence actions:fadein, scalel, scales, nil];
    
    for (int i=15; i<33; i++) {
        
        [[kingSprite getChildByTag:i] runAction:[CCRepeatForever actionWithAction:[[scale copy] autorelease]]];
    }
    
}

-(void)popBubble:(CGPoint)pos{
    TRBox2D *item;
    item = [[[TRBox2D alloc] initWithSpriteFrameName:@"bubble.png"] autorelease] ;
    [item setVertexZ:14] ;
    item.position = pos;
    item.scale = 0.5;
    item.tag = 20;
    //		item.flipX = YES;
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true 
              fixedRotation:true
                     bullet:false
     ];
    [item setVertexZ:14];
    
        
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"bubble"];
    
    [self addChild:item z:14];
    
    b2Vec2 position = b2Vec2(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    //          [self throwBodies:touched direction:b2Vec2(arc4random()%5,arc4random()%10) position:position tbody:[item body] impulse:b2Vec2((arc4random()%5)-10,(arc4random()%15)+10)];
    [self throwBodies:b2Vec2(1,1) position:position tbody:[item body] impulse:b2Vec2(float(arc4random()%1)-1.3,(float)(arc4random()%2)+0.8)];
    
    id scale = [CCScaleTo actionWithDuration:2 scale:1];
    [item runAction:scale];
  
}




// on "init" you need to initialize your instance
-(id) init
{
	if( (self=[super initWithPage:1])) {
        
        
	}
	return self;
}


-(void)EyeBlink
{
    CCSprite * octo_eyes = (CCSprite*)[[[self getChildByTag:0] getChildByTag:12] getChildByTag:1];
    
    CCSprite * jelly_eyes = (CCSprite*)[[[self getChildByTag:0] getChildByTag:14] getChildByTag:1];
    
    id action = [CCRepeatForever actionWithAction:[CCBlink actionWithDuration:1 blinks:1]];
    
    [octo_eyes runAction:action];
    [jelly_eyes runAction:[[action copy] autorelease]];

}

-(void)animateKing
{

    NSMutableArray *runFrames = [NSMutableArray array] ;
    
    for(int i = 3; i <= 9; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"King%i.png", i]]];
    }
    
    id action = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    id animate_king = [CCSequence actions:action,[action reverse], nil];
    
    [[self getChildByTag:3] runAction:[CCRepeatForever actionWithAction:animate_king]];
    
    
}

-(void)coralripple{
    
    CCSprite *coralParent = (CCSprite *)[self getChildByTag:0];
    
    id waves = [CCWaves actionWithDuration:5 size:ccg(32, 24) waves:3 amplitude:3 horizontal:NO vertical:YES];
    [[coralParent getChildByTag:9] runAction: [CCRepeatForever actionWithAction: waves]];
    [[coralParent getChildByTag:10] runAction: [CCRepeatForever actionWithAction:[[waves copy] autorelease]]];
    [[coralParent getChildByTag:11] runAction: [CCRepeatForever actionWithAction:[[waves copy] autorelease]]];
}



-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
   
    [self water:ccp(-30, 240) gravity:30];
    [self water:ccp(1054, 540) gravity:-30];
    [self zoomOut];
    
    //Blink Octopus eyes
    [self EyeBlink];
    [self animateKing];
    [self coralripple];
  
}

-(void) tick: (ccTime) dt{
    [super tick:dt];

	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
            if (item.tag == 20) {
                if (item.position.x < -10 || item.position.y<-30 || item.position.y>800) {
                    [item destroyBodyInWorld:world];
                    [self removeChild:item cleanup:YES];
                }
            }
		}	
    }
}


-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    
   if(touchedSprite.tag==3)
   {
   [[SimpleAudioEngine sharedEngine] playEffect:@"P1_2 hinge_spring_2.mp3"];
   
   }
   else if(touchedSprite.tag == 2 || (touchedSprite.tag > 3 && touchedSprite.tag < 9))
   {
       [[SimpleAudioEngine sharedEngine] playEffect:@"P1_2_hinge_spring.mp3"];
       
   }
    
    [super ccTouchesEnded:touches withEvent:event];
  
}


-(void)popSound
{

     
    switch(arc4random()%3)
    {
            
        case 0:
    
      [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1_pop1.mp3"];
            break;
    
        case 1:
      [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1_pop2.mp3"];
            break;
            
        default:
      [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1_pop3.mp3"];
            break;

    }
}

- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
	if (touchedSprite) {
        switch(touchedSprite.tag){
            case 9: {
            [self popBubble:CGPointMake(touchedSprite.position.x, touchedSprite.position.y+300)];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1 Bubble bursting.mp3"];
                break;
            }
                
            case 10:{
            [self popBubble:CGPointMake(location.x, touchedSprite.position.y+300)];
               [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1 Bubble bursting.mp3"];  
                break;
                }
                
            case 11:{
             [self popBubble:CGPointMake(touchedSprite.position.x-30, touchedSprite.position.y+240)];
             [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1 Bubble bursting.mp3"];
                break;
                }
            
            case 20:{
                if(location.y>110){
             [self world]->DestroyBody(touchedBody);
             [self removeChild:touchedSprite cleanup:YES];
             [self popSound];}
                break;
            }
              
        }
        
    }
    NSLog(@"tapped");
    NSLog(@"%f, %f", location.x, location.y);
    if (CGRectContainsPoint(CGRectMake(170, -10, 490, 100), location)) {
        [self popBubble:CGPointMake(location.x, 100)];
        [[SimpleAudioEngine sharedEngine] playEffect:@"P1_1 Bubble bursting.mp3"];
        
    }
    
    touchedBody = NULL;
    touchedSprite = NULL;
    
}



- (void) dealloc
{
    
	[super dealloc];
}
@end
