//
//  SneakyExtensions.h
//  Tutorial1
//
//  Created by Alex Brown on 29/06/2011.
//  Copyright 2011 none. All rights reserved.
//

#import <Foundation/Foundation.h>

// SneakyInput headers
#import "SneakyJoystick.h"
#import "SneakyJoystickSkinnedBase.h"
#import "SneakyButton.h"
#import "SneakyButtonSkinnedBase.h"

@interface SneakyJoystick (Extension)
+(id) joystickWithRect:(CGRect)rect;
@end

@interface SneakyJoystickSkinnedBase (Extension)
+(id) skinnedJoystick;
@end

@interface SneakyButton (Extension)
+(id) button;
+(id) buttonWithRect:(CGRect)rect;
@end

@interface SneakyButtonSkinnedBase (Extension)
+(id) skinnedButton;
@end
