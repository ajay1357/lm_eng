//
//  Page0.m
//  Little Mermaid
//
//  Created by Pipedream on 08/10/12.
//
//

#import "Page19.h"
#import "GlobalSet.h"


@implementation Page19


GlobalSet *global2;


+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	Page19 *layer = [Page19 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	
    
    // return the scene
    
	return scene;
}


-(id) init
{
	if( (self=[super initWithPage:19])) {
        global2 = [GlobalSet globalSettings];
        
    }
    
	return self;
}



-(void)startPage
{
    [self removeChildByTag:0 cleanup:YES];
    
    if([[touchedSprite getChildByTag:3] numberOfRunningActions]<1){
        id gap = [CCActionInterval actionWithDuration:3];
        id play = [CCCallFuncN actionWithTarget:self selector:@selector(animateCharacter)];
        [self runAction:[CCSequence actions:gap, play, nil]];}
    
}





-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
    [self sparkling:[self getChildByTag:1] from:1 till:15];
    
//    if(global2.FIRST_ATTEMPT){
//        
//        id gap = [CCActionInterval actionWithDuration:4];
//        id play = [CCCallFuncN actionWithTarget:self selector:@selector(startPage)];
//        [self runAction:[CCSequence actions:gap, play, nil]];
//        global2.FIRST_ATTEMPT = NO;
//        
//    }else{
//        [self startPage];
//    }
    [self startPage];
    global2.MENU = NO;
    
}




-(void)animateCharacter
{
    
    CCSprite *characterSprite = (CCSprite *)[self getChildByTag:3];
    
    
    NSMutableArray *runFramesMouth = [NSMutableArray array] ;
    
    for(int i = 1; i <= 2; i++) {
        [runFramesMouth addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"Mouth%i.png", i]]];
    }
    
    id animateMouth =  [CCRepeat actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFramesMouth delay:0.25]]  times:1];;
    
    [[characterSprite getChildByTag:1] runAction:animateMouth];
    
    
    NSMutableArray *runFramesEyes = [NSMutableArray array] ;
    
    for(int i = 8; i >= 7; i--) {
        [runFramesEyes addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"eye%i.png", i]]];
    }
    
    id animateEyes = [CCRepeat actionWithAction:[CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFramesEyes delay:0.4]] times:1];
    
    [[characterSprite getChildByTag:2] runAction:animateEyes];
    
    
    NSMutableArray *runFramesLeftHand = [NSMutableArray array] ;
    
    for(int i = 14; i >=13; i--) {
        [runFramesLeftHand addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"lh%i.png", i]]];
    }
    
    id animateLeftHand = [CCRepeat actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFramesLeftHand delay:0.3]]  times:1];
    
    [[characterSprite getChildByTag:3] runAction:animateLeftHand];
    
    
    
    NSMutableArray *runFramesRytHand = [NSMutableArray array] ;
    
    for(int i = 8; i >=7; i--) {
        [runFramesRytHand addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"rh%i.png", i]]];
    }
    
    id animateRytHand = [CCRepeat actionWithAction: [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFramesRytHand delay:0.3]] times:1];
    
    [[characterSprite getChildByTag:4] runAction:animateRytHand];
    
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:5.0];
    [[SimpleAudioEngine sharedEngine] playEffect:@"OUTRO.mp3"];
    
    
    
}






- (void) dealloc
{
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:1.0];
    
    global2 = nil;
    
	[super dealloc];
}

@end
