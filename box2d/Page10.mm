//
//  Page13.m
//  Little Mermaid
//
//  Created by Pipedream on 10/09/12.
//
//

#import "Page10.h"

@implementation Page10
BOOL startDrops;
BOOL flag;
BOOL drop;
//UIAccelerometer *accelerometer;

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page10 *layer = [Page10 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:10])) {
        [self setAccelerometerEnabled:YES];
        startDrops=FALSE;
        flag = FALSE;
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
//        accelerometer = [UIAccelerometer sharedAccelerometer];
//        accelerometer.updateInterval = 0.077;
//        accelerometer.delegate = self;
        
	}
	return self;
}


-(void)waterripple{
    id waves = [CCWaves actionWithDuration:15 size:ccg(32, 24) waves:5 amplitude:3 horizontal:YES vertical:YES];
    [[self getChildByTag:2] runAction: [CCRepeatForever actionWithAction: waves]];
}

-(void)popFish{
    CCSprite * fish = [CCSprite spriteWithSpriteFrameName:@"fish.png"];
    fish.position = location;
    fish.scale = 0;
    
    [self addChild:fish z:5];
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id gap = [CCActionInterval actionWithDuration:1];
    id hide = [CCScaleTo actionWithDuration:0.2 scale:0];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)fish];
    
    id popfish = [CCSequence actions:scaleup, scaledown, gap, hide, remove, nil];
    [fish runAction:popfish];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P5_3_Fish out from water.mp3"];

}

-(void)removeDress
{

    [interections removeObject:[self getChildByTag:18]];
    [self removeChildByTag:18 cleanup:YES];

}

-(void)animateDress{
        
    if ([self getChildByTag:18]) {
        id hide = [CCFadeTo actionWithDuration:2 opacity:0];
        id removeDressSprite = [CCCallFunc actionWithTarget:self selector:@selector(removeDress)];

        [[self getChildByTag:18] runAction:[CCSequence actions:hide,removeDressSprite,nil]];
        
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page13_3_Dresstearoff.mp3"];

    }
       
}

-(void) animateBottle
{
    id rotate = [CCRotateBy actionWithDuration:4 angle:360];
    [[self getChildByTag:10] runAction: [CCRepeatForever actionWithAction: rotate]];
}

-(void) animateDroplets
{
    id scaleup = [CCScaleTo actionWithDuration:2 scale:0.5];
    id scaledown = [CCScaleTo actionWithDuration:0 scale:0];
    id gap = [CCActionInterval actionWithDuration:3];
    id droplet_action=[CCRepeatForever actionWithAction: [CCSequence actions:scaleup, scaledown, gap, nil]];
    [[self getChildByTag:3] runAction:droplet_action];

}

-(void)crabSound
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"P3_3_crab.mp3"];
    
}


-(void) animateCrab
{
    NSMutableArray *runFrames = [NSMutableArray array];
    
    for(int i = 5; i <= 20; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"crab%i.png", i]]];
	}
    
    
    id animateCrab = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    
    [[self getChildByTag:5] runAction:animateCrab];

    
    [self performSelector:@selector(crabSound) withObject:nil afterDelay:0.5f];
}



//-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
//{
//    CCLOG(@"x = %f y = %f z = %f",acceleration.x,acceleration.y,acceleration.z);
//    static float prevX=0, prevY=0;
//    
//   
//    #define kFilterFactor 1.0f    // don't use filter. the code is here just as an example
//    
//    float accelX = (float) acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
//    float accelY = (float) acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
//    
//    prevX = accelX;
//    prevY = accelY;
//    
//    // accelerometer values are in "Portrait" mode. Change them to Landscape left
//    // multiply the gravity by 10
//    b2Vec2 gravity( -accelY * 10, accelX * 10);
//    
//    world->SetGravity( gravity );
//
//    
//}

-(void)createDroplets:(CGPoint)pos
{ 
    
    if (drop == NO) {
        drop = YES;
        return;
    }
    
    
    TRBox2D *item;
    int i = arc4random()%4+1;

    item = [[[TRBox2D alloc] initWithSpriteFrameName:[NSString stringWithFormat:@"water_particle%d.png",i]] autorelease];
    [item setVertexZ:4] ;
    item.position = pos;
    item.scale = 0.8;
    item.tag = 20;
    item.opacity=150;
    //item.isTouchEnabled=false;
    //		item.flipX = YES;
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true
              fixedRotation:true
                     bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"water_particle"];
    
    [self addChild:item z:7];
    
    [item body]->ApplyLinearImpulse(b2Vec2(-0.05,0),b2Vec2(0,0));
    drop = NO;
}


-(void)tick:(ccTime)dt
{
    [super tick:dt];
    
    if(startDrops)
    {
     [self createDroplets:CGPointMake(930.00,623.00)];
        }
    
  	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
            if (item.tag == 20) {
                if (item.position.y<30 || item.position.x<30 || item.position.y>748 || item.position.x>1000){
                    [item destroyBodyInWorld:world];
                    [self removeChild:item cleanup:YES];
                }
            }
		}
    }
}



-(void)setBottlePosition
{
    TRBox2D *item;
    
    item = [[[TRBox2D alloc] initWithSpriteFrameName:@"bottle1.png"] autorelease] ;
    [item setVertexZ:6] ;
    item.position = CGPointMake(980, 670);
    item.scale = 1;
    item.tag = 21;
    //item.isTouchEnabled=false;
    //		item.flipX = YES;
    b2BodyType type = b2_staticBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:180.00
                 allowSleep:true
              fixedRotation:true
                     bullet:false
     ];

    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"bottle"];
    
    [self addChild:item z:6];
    startDrops=TRUE;
    
    
}


-(void)playDropsMusic
{

[[SimpleAudioEngine sharedEngine] playEffect:@"Page13_4_potion.mp3"];

}

-(void)setGameScene
{
    
    flag = TRUE;
    
     CCSprite *bottleSprite = (CCSprite *)[self getChildByTag:10];
    [[[CCDirector sharedDirector] actionManager] removeAllActionsFromTarget:bottleSprite];
        
    id gap = [CCActionInterval actionWithDuration:2];
    id move_bottle=[CCMoveTo actionWithDuration:2 position:ccp(1000,675)];
    id resize_bottle = [CCScaleTo actionWithDuration:2 scale:0.55];
    id action = [CCSpawn actions:move_bottle,resize_bottle, nil];
    
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:bottleSprite];
    id startDropsAction = [CCCallFunc actionWithTarget:self selector:@selector(setBottlePosition)];
    
    [bottleSprite runAction:[CCSequence actions:gap,action , remove, startDropsAction ,nil]];
   
    
     id gap2 = [CCActionInterval actionWithDuration:4];
    
     id DropsMusic = [CCCallFunc actionWithTarget:self selector:@selector(playDropsMusic)];
     id repeatDropMusic = [CCRepeatForever actionWithAction:[CCSequence actions:gap2,DropsMusic,nil]] ;
    
    [self runAction:repeatDropMusic];
}



-(void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node
{

  [super tapped:recognizer node:node];
    if(touchedSprite){
   switch(touchedSprite.tag) {
       case 10:{
           if(!flag){
               [self setGameScene];}
            [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_6_Fluxglitter.mp3"];
            break;
          }
       case 2:if((location.x<502&&location.y<638) || (location.x>908&&location.y>630)){}
       else{
           [self popFish];}
           break;
        case 5:
           if([touchedSprite numberOfRunningActions] <=0){
               [self animateCrab];}
            break;
        case 18:
        case 4:
           [self animateDress];
           break;
   }
    }
    touchedSprite=NULL;
    touchedBody = NULL;
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
   // world->SetGravity(b2Vec2(0,-1));
    [self waterripple];
    [self animateBottle];
    [self animateDroplets];
   // [self animateFlask];
}


- (void) dealloc
{

//    accelerometer = NULL;
	[super dealloc];
}



@end
