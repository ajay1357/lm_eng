//
//  HelloWorldLayer.mm
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "Page1.h"
//#import "ControlLayer.h"


// HelloWorldLayer implementation
@implementation Page1
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//ControlLayer *controlls = [[ControlLayer alloc] initWithLayer];
//	[scene addChild:controlls z:5];
	// 'layer' is an autorelease object.
	Page1 *layer = [Page1 node];

	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}


//PAGE 1 BEGINS

-(void)zoomOut{
    id scale = [CCScaleTo actionWithDuration:4 scale:1];
    id showall = [CCCallFunc actionWithTarget:self selector:@selector(showAll)];
    [[self getChildByTag:0] runAction:[CCSequence actions:scale, showall, nil]];
}

-(void)popup:(id)item{
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id scaleflower = [CCSequence actions:scaleup, scaledown, nil];
    [item runAction:scaleflower];
    
}

-(void)showAll{
    for (int i=3; i<9; i++) {
        id fadein = [CCFadeIn actionWithDuration:1];
        if (i==8) {
            id wait = [CCActionInterval actionWithDuration:10];
            id showdoughter = [CCCallFunc actionWithTarget:self selector:@selector(showDoughter)];
            id sparkling = [CCCallFunc actionWithTarget:self selector:@selector(sparkling)];
            [[self getChildByTag:i] runAction:[CCSequence actions:fadein, sparkling, wait, showdoughter, nil]];
        }else{
            [[self getChildByTag:i] runAction:fadein];
        }
        
    }
}

-(void)showDoughter{
    id fadein = [CCFadeIn actionWithDuration:0.5];
    [[self getChildByTag:2] runAction:fadein];
}


-(void)sparkling{
    [self sparkling:CGPointMake(250, 125) tag:20 sprite:(TRBox2D *)[self getChildByTag:3] startSize:40 posvar:CGPointMake(50, 65) life:5 erate:15 tItem:50]; 
    for (int i=15; i<34; i++) {
        id fadein = [CCFadeTo actionWithDuration:0 opacity:255];
        id scalel = [CCScaleTo actionWithDuration:1 scale:1];
        id scales = [CCScaleTo actionWithDuration:1 scale:0];
        id scale = [CCSequence actions:fadein, scalel, scales, nil];
        [[[self getChildByTag:3] getChildByTag:i] runAction:[CCRepeatForever actionWithAction: scale]];
    }
    
}

-(void)popBubble:(CGPoint)pos{
    TRBox2D *item;
    item = [[TRBox2D alloc] initWithSpriteFrameName:@"bubble.png"] ;
    [item setVertexZ:14] ;
    item.position = pos;
    item.scale = 0.5;
    item.tag = 20;
    //		item.flipX = YES;
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true 
              fixedRotation:true
                     bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"bubble"];
    
    [self addChild:item z:14];
    
    b2Vec2 position = b2Vec2(pos.x/PTM_RATIO, touchedSprite.position.y/PTM_RATIO);
    //          [self throwBodies:touched direction:b2Vec2(arc4random()%5,arc4random()%10) position:position tbody:[item body] impulse:b2Vec2((arc4random()%5)-10,(arc4random()%15)+10)];
    [self throwBodies:b2Vec2(1,1) position:position tbody:[item body] impulse:b2Vec2(float(arc4random()%1)-1.3,(float)(arc4random()%2)+0.8)];
    
    id scale = [CCScaleTo actionWithDuration:2 scale:1];
    [item runAction:scale];
    
}


//Page 1 ENDS

-(void) animateEyes{
	//NSMutableArray *animFrames = [NSMutableArray array];
    id show = [CCBlink actionWithDuration:1 blinks:1];
    //[self addChild:sprite];
    id hide = [CCHide action];
	[[[self getChildByTag:4] getChildByTag:12] runAction:[CCRepeatForever actionWithAction:[CCSequence actions:show, hide, nil]]];
    //	[[SimpleAudioEngine sharedEngine] playEffect:@"01boy.mp3"];
    //    [self removeChildByTag:5 cleanup:YES];
}

-(void)chorus{
	CCParticleSmoke *chorus = [[[CCParticleSmoke alloc]init] autorelease] ;
	chorus.texture = [[CCSprite spriteWithFile:@"fire.png"] texture] ;
	chorus.autoRemoveOnFinish = YES;
	chorus.position = ccp(500, 5);
	chorus.posVar = ccp(200, 5);
	chorus.totalParticles = 300;
	chorus.emissionRate = 50;
	ccColor4F strtcolor = {0xFF/255.0,0x48/255.0,0x2D/255.0,0xFF/255.0};
	chorus.startColor = strtcolor;
	chorus.endColor = strtcolor;
	ccColor4F strtcolorv = {0xFF/255.0, 0xC2/255.0, 0xA0/255.0, 0xFF/255.0};
	chorus.startColorVar = strtcolorv;
	chorus.endColorVar = strtcolorv;
	chorus.life = 3;
	chorus.lifeVar = 3;
	chorus.startSize = 0;
	chorus.startSizeVar = 0;
	chorus.endSize = 10;
	chorus.endSizeVar = 5;
	chorus.gravity = ccp(0,0.5);
	[chorus setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
	[self addChild:chorus z:9];
}






// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:1])) {
//        self.scale = 2;
        
		// enable touches
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
//    [self chorus];
    
    [self water:ccp(-30, 240) gravity:30];
    [self water:ccp(1054, 540) gravity:-30];
    [self zoomOut];
}

-(void) tick: (ccTime) dt{
    [super tick:dt];
//    int32 velocityIterations = 8;
//	int32 positionIterations = 1;
//	// Instruct the world to perform a single step of simulation. It is
//	// generally best to keep the time step and iterations fixed.
//	world->Step(dt, velocityIterations, positionIterations);
//	//Iterate over the bodies in the physics world
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
            if (item.tag == 20) {
                if (item.position.x < 55 || item.position.y<55 || item.position.y>715) {
                    [item destroyBodyInWorld:world];
                    [self removeChild:item cleanup:YES];
                }
            }
		}	
    }
}

- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
	if (touchedSprite) {
        if (touchedSprite.tag==9) {
            [self popBubble:CGPointMake(touchedSprite.position.x, touchedSprite.position.y+300)];
        }else if (touchedSprite.tag==10) {
            [self popBubble:CGPointMake(location.x, touchedSprite.position.y+300)];
        }else if (touchedSprite.tag==11) {
            [self popBubble:CGPointMake(touchedSprite.position.x-30, touchedSprite.position.y+240)];
        }else if (touchedSprite.tag == 20) {
            [self world]->DestroyBody(touchedBody);
            [self removeChild:touchedSprite cleanup:YES];
        }
        
        touchedBody = NULL;
        touchedSprite = NULL;
    }
    if (CGRectContainsPoint(CGRectMake(180, 0, 440, 70), location)) {
        [self popBubble:CGPointMake(location.x, 100)];
    }
}


- (void) dealloc
{

	[super dealloc];
}
@end
