//
//  Page0.m
//  Little Mermaid
//
//  Created by Pipedream on 08/10/12.
//
//

#import "Page50.h"
#import "GlobalSet.h"


@implementation Page50


+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	Page50 *layer = [Page50 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	
    
    // return the scene
    
	return scene;
}


-(id) init
{
	if( (self=[super initWithPage:50])) {
        
    }
    
	return self;
}



-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"intro" ofType:@"mp4"];
    
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    player = [[MPMoviePlayerController alloc] initWithContentURL: fileURL];
    
    [[(CCDirectorIOS*)[CCDirector sharedDirector] view] addSubview:player.view];
    
    player.fullscreen = YES;
    player.controlStyle = MPMovieControlStyleNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDone:) name: MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    
    [player play];
}




- (void) movieDone:(NSNotification*)notification
{
    MPMoviePlayerController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];
    [moviePlayerController.view removeFromSuperview];
    moviePlayerController = nil;
    player.fullscreen = NO;
    [player.view removeFromSuperview];
    player = nil;
    canChangeScene = YES;
    
    [self sceneChangeTo:0];
}




- (void) dealloc
{
    
	[super dealloc];
}

@end
