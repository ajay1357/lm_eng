//
//  Page23.m
//  Little Mermaid
//
//  Created by Pipedream on 28/09/12.
//
//

#import "Page18.h"

@implementation Page18

BOOL reset;
CCSprite *burnSprite,*scratchBg;
CCLayer *scratchLayer;
CGPoint _lastPosition , _currentPosition;
int ch;
BOOL rainbowRepainted;
bool rainbow;
id rainbowRepaintAction;
//SimpleAudioEngine *sae ;

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page18 *layer = [Page18 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:18])) {
        ch=0;
        rainbowRepainted=YES;
        rainbow = NO;
               
//         sae = [[SimpleAudioEngine alloc] init];     
             
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
	}
	return self;
}

-(void)initEraser
{
    if(ch==1)
    {
//        [scratchLayer removeAllChildrenWithCleanup:YES];
//        CGSize size = [[CCDirector sharedDirector] winSize];
//        scratchLayer = [CCRenderTexture renderTextureWithWidth:size.width height:size.height];
//        scratchLayer.position =  ccp(size.width/2 ,size.height/2);
        
        [[scratchLayer sprite] setBlendFunc: (ccBlendFunc) { GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA }];
        [scratchLayer begin];
        [scratchBg visit];
        [scratchLayer end];
         rainbowRepainted = YES;
        return;
    }
    
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    [self setTouchEnabled:YES];
    reset = YES;

    
    //Set up the burn sprite that will "knock out" parts of the darkness layer depending on the
    //alpha value of the pixels in the image.
    burnSprite = [CCSprite spriteWithFile:@"bubble.png"];
    [burnSprite setBlendFunc:(ccBlendFunc) { GL_ZERO, GL_ONE_MINUS_SRC_ALPHA }];
    [burnSprite retain];
    burnSprite.scale = 0.7f;
    
    
    // Scratch Background
    scratchBg = [[CCSprite spriteWithFile:@"bwrainbow1.png"] retain];
    scratchBg.position = ccp(576,485);
    
    // Scratch Layer
    scratchLayer = [CCRenderTexture renderTextureWithWidth:size.width height:size.height];
    scratchLayer.position =  ccp(size.width/2 ,size.height/2);
    [[scratchLayer sprite] setBlendFunc: (ccBlendFunc) { GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA }];

    
    [self addChild:scratchLayer z:21];
    [scratchLayer begin];
    [scratchBg visit];
    [scratchLayer end];
    
    [[self getChildByTag:7] runAction:[CCFadeIn actionWithDuration:0] ];
    
        
    rainbowRepainted = YES;
    
}



- (void) drawTexture {
    // Update the render texture
    [scratchLayer begin];
    
    // Limit drawing to the alpha channel
    glColorMask(0.0f, 0.0f, 0.0f, 1.0f);
    
    // Draw
    [burnSprite visit];
    
    // Reset color mask
    glColorMask(1.0f, 1.0f, 1.0f, 1.0f);
    
    [scratchLayer end];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_2_rainow_v1.mp3"];
    
} //



-(void)animateFairies
{
    
    NSMutableArray *runFrames1 = [NSMutableArray array];
    NSMutableArray *runFrames2 = [NSMutableArray array];
    
    for(int i = 1; i <= 6; i++) {
        [runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"fairyLeft%i.png", i]]];
	}
    id fairyLeftAnimate = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.2]];
    
    
    for(int i = 1; i <= 7; i++) {
        [runFrames2 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"fairyRyt%i.png", i]]];
	}
    id fairyRytAnimate = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames2 delay:0.2]];
    
    
    [[self getChildByTag:11] runAction:[CCRepeatForever actionWithAction:fairyRytAnimate]];
    [[self getChildByTag:10] runAction:[CCRepeatForever actionWithAction:fairyLeftAnimate]];
    
    id gap = [CCActionInterval actionWithDuration:10];
    id fairyRytMove = [CCMoveTo actionWithDuration:5 position:ccp(825,346)];
    id fairyLeftMove = [CCMoveTo actionWithDuration:5 position:ccp(150,490)];
    
    [[self getChildByTag:11] runAction:[CCSequence actions:gap,fairyLeftMove, nil]];
    [[self getChildByTag:10] runAction:[CCSequence actions:gap,fairyRytMove, nil]];
   
//     [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_3_fairyflying.mp3"];
    
}

-(void)clearClouds
{
    [[self getChildByTag:16] runAction:[CCMoveTo actionWithDuration:5 position:ccp(-650,220)]];
    [[self getChildByTag:17] runAction:[CCMoveTo actionWithDuration:5 position:ccp(1650,372)]];
    [[self getChildByTag:18] runAction:[CCMoveTo actionWithDuration:5 position:ccp(-650,535)]];
    [[self getChildByTag:19] runAction:[CCMoveTo actionWithDuration:5 position:ccp(1650,641)]];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_2_Rainbow_v1.mp3"];
   
    
}


-(void)flux:(int)ringTag parent:(int)parentTag{
    CCSprite * sprite = [CCSprite spriteWithSpriteFrameName:@"ring.png"];
   
    CGPoint pos;
    switch(parentTag)
    {
        case 8: pos = ccp(389,384);
            break;
            
        case 9: pos = ccp(730,620);
            break;
            
        case 10: pos = ccp(699,369);
            break;
            
        case 11: pos = ccp(220,552);
            break;
    
    }
    
    sprite.position = pos;
    [self addChild:sprite z:100];
    id zoom = [CCScaleBy actionWithDuration:0.5 scale:1.1];
    id move = [CCMoveBy actionWithDuration:8 position:ccp(0, 100)];
    id zoomBack = [CCScaleBy actionWithDuration:0.2 scale:0.9];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)sprite];
    
    [sprite runAction:[CCSequence actions:[CCRepeat actionWithAction:[CCSequence actions:zoom,zoomBack,nil] times:6],remove,nil]];
    [sprite runAction:move];
    
  [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_6_Fluxglitter.mp3"];  
    
   
}

-(void)openGates
{
    id foo = [CCActionTween actionWithDuration:10.0 key:@"scaleX" from:1.0 to:0]; 
    [[self getChildByTag:14] runAction:foo];
    id foo1 = [CCActionTween actionWithDuration:10.0 key:@"scaleX" from:1.0 to:0];
    [[self getChildByTag:15] runAction:foo1];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_1_Gateopens.mp3"];
    
//    [self performSelector:@selector(clearClouds) withObject:nil afterDelay:10.0f];
    id gapc = [CCActionInterval actionWithDuration:10];
    id playc = [CCCallFuncN actionWithTarget:self selector:@selector(clearClouds)];
    [self runAction:[CCSequence actions:gapc, playc, nil]];
    
    
    [self performSelector:@selector(clearGates) withObject:nil afterDelay:10.0f];
    id gap = [CCActionInterval actionWithDuration:10];
    id play = [CCCallFuncN actionWithTarget:self selector:@selector(initEraser)];
    [self runAction:[CCSequence actions:gap, play, nil]];
    

}
-(void)clearGates{
    [self removeChild:[self getChildByTag:12] cleanup:YES]; 
    [self removeChild:[self getChildByTag:13] cleanup:YES];
}

//
//-(void)bgSoundFunc:(ccTime)dt
//{    
////    [sae setEffectsVolume:5];
////    [sae playEffect:@"Page21_3_fairyflying.mp3"];
// [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_3_fairyflying.mp3"];
//}


-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    [self animateFairies];
    
    id flipRytGate = [CCFlipX actionWithFlipX:TRUE];
    [[self getChildByTag:15] runAction:flipRytGate];
    [self openGates];
    
//    [self schedule:@selector(bgSoundFunc:) interval:10.0f ];
//    world->SetGravity(b2Vec2(0.0f,0.0f));
    
}



- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super ccTouchesBegan:touches withEvent:event];
    
    if (touchedSprite.tag >= 120) {
             
    }else{
        burnSprite.position = CGPointMake(location.x, location.y);
        _lastPosition = _currentPosition = burnSprite.position;
    
    }
}



- (void)ccTouchesMoved:(NSSet*) touches withEvent:(UIEvent*) event {
    [super ccTouchesMoved:touches withEvent:event];
	_currentPosition = CGPointMake(location.x, location.y);
    
	// for extra points, we'll draw this smoothly from the last position and vary the sprite's
	// scale/rotation/offset
	float distance = ccpDistance(_lastPosition, _currentPosition);
	if ( distance > 1 ) {
		int d = (int)distance;
		for (int i = 0; i < d; ++i) {
            
			float difx = _currentPosition.x - _lastPosition.x;
			float dify = _currentPosition.y - _lastPosition.y;
			float delta = (float)i / distance;
			[burnSprite setPosition:ccp(_lastPosition.x + (difx * delta), _lastPosition.y + (dify * delta))];
			[burnSprite setRotation:rand() % 360];
			float r = ((float)(rand() % 50) / 50.f) + 0.25f;
			[burnSprite setScale:r];
            
			[self drawTexture];
            
		}
       
	}
    
    burnSprite.position = _currentPosition;
    
    _lastPosition = _currentPosition;
    
    if(CGRectContainsPoint(CGRectMake(317, 581, 525,100), location) || CGRectContainsPoint(CGRectMake(317, 313, 290,270), location))
    {
        rainbow = YES;
    }
    
}


-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(touchedSprite.tag==8)
    {
        [[SimpleAudioEngine sharedEngine] playEffect:@"P1_2 hinge_spring_2.mp3"];
        
    }
    if (rainbow) {
        rainbow = NO;
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_2_Rainbow_v1.mp3"];
        ch=1;

        
        
        if(rainbowRepainted){
        id gapr = [CCActionInterval actionWithDuration:15];
        id playr = [CCCallFuncN actionWithTarget:self selector:@selector(initEraser)];

        rainbowRepaintAction= [CCSequence actions:gapr, playr, nil];
        rainbowRepainted = NO;
        [self runAction:rainbowRepaintAction];
        }

       
    }
    
    [super ccTouchesEnded:touches withEvent:event];
    
  
}


-(void)removeitem:(id)sender data:(CCSprite*)item{
    [self removeChild:item cleanup:YES];

}


-(void)animateApple:(CGPoint)pos andtag:(int)tag
{
    TRBox2D *item;
    switch(tag){
        case 1:
             item = [[[TRBox2D alloc] initWithSpriteFrameName:@"apple1.png"] autorelease] ;
             item.tag = 120;
             
             break;
            
        case 2:
            item = [[[TRBox2D alloc] initWithSpriteFrameName:@"apple2.png"] autorelease] ;
            item.tag = 121;
            break;
            
        case 3:
            item = [[[TRBox2D alloc] initWithSpriteFrameName:@"apple3.png"] autorelease] ;
            item.tag = 122;
            break;
            
        case 4:
            item = [[[TRBox2D alloc] initWithSpriteFrameName:@"apple4.png"] autorelease] ;
            item.tag = 123;
            break;
            
            
    }

   
    item.position = pos;
    item.scale = 1;
   
    
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true
              fixedRotation:true
                     bullet:false
     ];
    [item body]->ApplyLinearImpulse(b2Vec2(0, -5), b2Vec2(0,0));
      
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"apple1"];
    
    [self addChild:item z:200];
    
  
}



-(void) tick: (ccTime) dt{
    [super tick:dt];
  	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
            if (item.tag >= 120) {
                if (item.position.x < 30 || item.position.y<50 || item.position.x>990 ) {
                    if (touchedSprite) {
                        if (touchedSprite.tag == item.tag) {
                            touchedSprite = NULL;
                            touchedBody = NULL;
                        }
                    }
                    
                    [item destroyBodyInWorld:world];
                    [self removeChild:item cleanup:YES];
                }
                
            }
		}
    }
}


-(void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    //static int flag=1;
	if (touchedSprite) {
        switch(touchedSprite.tag){
            case 81:
            case 8:
                [self flux:touchedSprite.tag parent:8];
                break;
                        
            case 91:
            case 9:
                [self flux:touchedSprite.tag parent:9];
                break;
                
            case 110:
            case 10:
                [self flux:touchedSprite.tag parent:10];
                break;
                
            case 11:
            case 1110:
                [self flux:touchedSprite.tag parent:11];
            break;
                
            case 0:
                if (location.x < 30 || location.y<80 || location.x>990 ) {
                }else{
                [self animateApple:location andtag:1];
                  [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_4_AppleHidden_v1.mp3"];
                }
                break;
                
            case 120:
                              
                [self world]->DestroyBody(touchedBody);
                [self animateApple:touchedSprite.position andtag:2];
                [self removeChild:touchedSprite cleanup:YES];
                [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_5_appleeating.mp3"];
                break;
                
            case 121:
                 [self animateApple:touchedSprite.position andtag:3];
                [self world]->DestroyBody(touchedBody);
                [self removeChild:touchedSprite cleanup:YES];
                [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_5_appleeating.mp3"];
                break;
                
            case 122:
                   [self animateApple:touchedSprite.position andtag:4];
                  [self world]->DestroyBody(touchedBody);
                  [self removeChild:touchedSprite cleanup:YES];
                [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_5_appleeating.mp3"];
                break;
                
            case 123:
               
                [self world]->DestroyBody(touchedBody);
                [self removeChild:touchedSprite cleanup:YES];
                [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_5_appleeating.mp3"];
                break;
                
                
            case 7:
                [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_2_rainow_v2.mp3"];
                break;
        }
    }
    

    
    touchedSprite = NULL;
    touchedBody = NULL;
}


- (void) dealloc
{
//    
//    [sae release];
//    sae=NULL;
//
 //   [burnSprite release];
    burnSprite = NULL;
//    [scratchBg release];
    scratchBg=NULL;
//    [scratchLayer release];
    [scratchLayer removeAllChildrenWithCleanup:YES];
    [self removeChild:scratchLayer cleanup:YES];
    scratchLayer = NULL;
    rainbowRepaintAction = NULL;
    
	[super dealloc];
}





@end
