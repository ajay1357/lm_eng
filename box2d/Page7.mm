//
//  Page7.m
//  Little Mermaid
//
//  Created by Pipedream on 03/09/12.
//
//

#import "Page7.h"

@implementation Page7


+(CCScene *) scene
{
        // 'scene' is an autorelease object.
        CCScene *scene = [CCScene node];
        
        // 'layer' is an autorelease object.
        Page7 *layer = [Page7 node];
        
        // add layer as a child to scene
        [scene addChild: layer];
        // return the scene
        return scene;
   }


-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:7])) {
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}


-(float)getAngle:(CGPoint)startPos endPos:(CGPoint)endPos currentAngle:(float)currentAngle{
    float theta = atan((startPos.y-endPos.y)/(startPos.x-endPos.x)) * 180 * 7 /22;
    
    float calculatedAngle = 0;
    
    if(startPos.y - endPos.y > 0)
    {
        if(startPos.x - endPos.x < 0)
        {
            calculatedAngle = (-90-theta);
        }
        else if(startPos.x - endPos.x > 0)
        {
            calculatedAngle = (90-theta);
        }       
    }
    else if(startPos.y - endPos.y < 0)
    {
        if(startPos.x - endPos.x < 0)
        {
            calculatedAngle = (270-theta);
        }
        else if(startPos.x - endPos.x > 0)
        {
            calculatedAngle = (90-theta);
        }
    }
    calculatedAngle -= 90;
    
    return calculatedAngle;
    
   }


-(void)animateFish
{
    id pink_scaleup = [CCScaleTo actionWithDuration:4 scale:1.7];
    id pink_scaledown = [CCScaleTo actionWithDuration:4 scale:1];
    id enlarge_pink = [CCSequence actions:pink_scaleup, pink_scaledown, nil];
    id gap = [CCActionInterval actionWithDuration:2];
    [[self getChildByTag:3] runAction:enlarge_pink];
    id fish_scaledown = [CCScaleTo actionWithDuration:4 scale: 0.1];
    id fish_scaleup = [CCScaleTo actionWithDuration:0 scale: 1];
    
    
    CGPoint pos1 = ccp(arc4random()%300,arc4random()%768);
    float angle1 = [self getAngle:ccp(896,552) endPos:pos1 currentAngle:0];
    CGPoint pos2 = ccp(arc4random()%1024,arc4random()%768);
    float angle2 = [self getAngle:pos1 endPos:pos2 currentAngle:angle1];
    CGPoint pos3 = ccp(arc4random()%1024,arc4random()%768);
    float angle3 = [self getAngle:pos2 endPos:pos3 currentAngle:angle2];
    CGPoint pos4 = ccp(1350,552);
    float angle4 = [self getAngle:pos3 endPos:pos4 currentAngle:angle3];
    CGPoint pos5 = ccp(896,552);
    float angle5 = 0;
    
    id move1=[CCMoveTo actionWithDuration:0.8 position:pos1];
    id rotate1 = [CCRotateTo actionWithDuration:0 angle:angle1];
    id move2=[CCMoveTo actionWithDuration:0.8 position:pos2];
    id rotate2 = [CCRotateTo actionWithDuration:0 angle:angle2];
    id move3=[CCMoveTo actionWithDuration:0.8 position:pos3];
    id rotate3 = [CCRotateTo actionWithDuration:0 angle:angle3];
    id move4=[CCMoveTo actionWithDuration:1 position:pos4];
    id rotate4 = [CCRotateTo actionWithDuration:0 angle:angle4];
    id move5=[CCMoveTo actionWithDuration:6 position:pos5];
    id rotate5 = [CCRotateTo actionWithDuration:0 angle:angle5];
    
    [[self getChildByTag:4] runAction:[CCSequence actions:gap,gap,rotate1, move1,rotate2, move2,rotate3,move3,rotate4,move4,rotate5,move5,nil]];
    [[self getChildByTag:4] runAction:[CCSequence actions:gap,gap,fish_scaledown,fish_scaleup,nil]];
    
   
    
    [self performSelector:@selector(fishSound) withObject:nil afterDelay:4.0f];
    
}

-(void)fishSound
{
 [[SimpleAudioEngine sharedEngine] playEffect:@"P9_3_Balloon deflating sound.mp3"];
}

-(void)waterripple{
    id waves = [CCWaves actionWithDuration:5 size:ccg(32, 24) waves:3 amplitude:3 horizontal:YES vertical:NO];
    [[self getChildByTag:1] runAction: [CCRepeatForever actionWithAction: waves]];
}


-(void)jellyripple{
    id waves = [CCWaves actionWithDuration:5 size:ccg(32, 24) waves:3 amplitude:3 horizontal:YES vertical:NO];
    [[self getChildByTag:10] runAction: [CCRepeatForever actionWithAction: waves]];
     [[self getChildByTag:11] runAction: [CCRepeatForever actionWithAction: waves]];
}


-(void)animateTurtle{
    NSMutableArray *runFrames1 = [NSMutableArray array];
    
    for(int i = 1; i <= 21; i++) {
		[runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"turtle%i.png", i]]];
	}
    
        
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.4]];
    id animate_turtle = [CCSpeed actionWithAction:[CCRepeat actionWithAction:forward times:4] speed:2.0];
    
    id move_ahead = [CCMoveTo actionWithDuration:10 position:ccp(1080,600)];
    id gap = [CCActionInterval actionWithDuration:2];
    id reposition_turtle1 = [CCMoveTo actionWithDuration:0 position:ccp(-100,301)];
    id reposition_turtle2 = [CCMoveTo actionWithDuration:4 position:ccp(114,301)];
        
    id turtle_move = [CCSequence actions:move_ahead,gap,reposition_turtle1,reposition_turtle2, nil];

    [[self getChildByTag:2] runAction:turtle_move];
        
    [[self getChildByTag:2]  runAction:animate_turtle];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P9_1_tortoise swimmimg_tapped.mp3"];
    
    id gap1 = [CCActionInterval actionWithDuration:1.5];
    id play = [CCCallFuncN actionWithTarget:self selector:@selector(swimSound)];
    [self runAction:[CCSequence actions:gap1, play,gap1, play,gap1, play, nil]];
//    [self performSelector:@selector(swimSound) withObject:nil afterDelay:1.5f];
//    [self performSelector:@selector(swimSound) withObject:nil afterDelay:3.0f];
//    [self performSelector:@selector(swimSound) withObject:nil afterDelay:4.5f];
}

-(void)swimSound
{

[[SimpleAudioEngine sharedEngine] playEffect:@"P9_1_tortoise swimmimg_tapped.mp3"];
}


-(void)animateOctopus{
    NSMutableArray *runFrames1 = [NSMutableArray array];
       
    for(int i = 2; i<= 20; i++) {
     
		[runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"octo%i.png", i]]];
	}
    
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.1]];
        
    id animate_octopus = [CCSpeed actionWithAction:forward speed:1.0];
     
    [[self getChildByTag:5]  runAction:animate_octopus ];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P9_2_red octopus.mp3"];
}


-(void)animateHair{
    NSMutableArray *runFrames1 = [NSMutableArray array];
    
    for(int i = 1; i<= 19; i++) {
        int j= i;
        if (i>10) {
            j = 21-i;
        }
		[runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"cutting%i.png", j]]];
	}
    
    id animate_hair = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.1]];
    [[self getChildByTag:9]  runAction:[CCRepeatForever actionWithAction: animate_hair]];
}


-(void)eyeBlink
{
    id show = [CCBlink actionWithDuration:3 blinks:3];
    id gap = [CCActionInterval actionWithDuration:arc4random()%5 + 10];
    
    
    
    id blink = [CCRepeatForever actionWithAction:[CCSequence actions:show,gap,nil]];
    

    
    [[[self getChildByTag:8] getChildByTag:21] runAction:blink];
    
}


-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    [self waterripple];
//    [self jellyripple];
    id fadein = [CCFadeTo actionWithDuration:7 opacity:255];
    [[self getChildByTag:7] runAction:fadein ];
    [self sparkling:[self getChildByTag:8] from:1 till:20];
    [self sparkling:CGPointMake(405, 207) tag:20 sprite:NULL startSize:40 posvar:CGPointMake(40, 40) life:5 erate:10 tItem:50];    
    [self sparkling:CGPointMake(588, 223) tag:20 sprite:NULL startSize:40 posvar:CGPointMake(30, 40) life:10 erate:15 tItem:80];
    
    [self animateHair];
    
    [self eyeBlink];
    
    world->SetGravity(b2Vec2(0.0f,0.0f));
       
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super ccTouchesMoved:touches withEvent:event];
    if (touchedSprite.tag == 15 && touchedSprite.opacity > 1) {
        touchedSprite.opacity -= 2;
        
    }
}

-(void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    
	if (touchedSprite) {
        
        if ((touchedSprite.tag == 2)&&([touchedSprite numberOfRunningActions]<= 0)) {    //Turtle tapped
            
            [self animateTurtle];
        }
        else if((touchedSprite.tag==5)&&([touchedSprite numberOfRunningActions]<= 0))  //Ocopus tapped
        {   [self animateOctopus];}
        
        else if((touchedSprite.tag==3)&&([touchedSprite numberOfRunningActions]<= 0))  //Pink tabbed
        {
            [self animateFish];
          
        }
    }
    
    if(location.x>300 && location.x<500 && location.y>250&&location.y<600){
        [self popHeart:CGPointMake([self getChildByTag:8].position.x, [self getChildByTag:8].position.y)];
    }
    
    touchedSprite = NULL;
    touchedBody = NULL;
   

}




-(void)popHeart:(CGPoint)pos{
    
    TRBox2D *item;
    item = [[[TRBox2D alloc] initWithSpriteFrameName:@"heart.png"] autorelease] ;
    [item setVertexZ:14] ;
    item.position = pos;
    item.scale = 0.4;
    item.tag = 20;
    //item.isTouchEnabled=false;
    //		item.flipX = YES;
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true
              fixedRotation:true
                     bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"heart"];
    
    [self addChild:item z:9];
    
    b2Vec2 position = b2Vec2(pos.x/PTM_RATIO, [self getChildByTag:8].position.y/PTM_RATIO);
   
    [self throwBodies:b2Vec2(float(arc4random()%5 -2.5),(float)(arc4random()%5 - 2.5)) position:position tbody:[item body] impulse:b2Vec2(float(arc4random()%5 - 2.5),(float)(arc4random()%5 - 2.5))];
    
    id scale_up = [CCScaleTo actionWithDuration:0.3 scale:0.4];
    id scale_down = [CCScaleTo actionWithDuration:0.3 scale:0.3];
    id heart_beat=[CCSequence actions:scale_up,scale_down,nil];
    [item runAction:[CCRepeat actionWithAction:heart_beat times:20]];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P9_4_heartbeat.mp3"]; 
   
    
}



-(void) tick: (ccTime) dt{
    [super tick:dt];
  	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
            if (item.tag == 20) {
                if (item.position.x < 115 || item.position.y<115 || item.position.x>900 || item.position.y>650 ) {
                    [item destroyBodyInWorld:world];
                    [self removeChild:item cleanup:YES];
                }
            }
		}	
    }
}

- (void) dealloc
{
    
	[super dealloc];
}



@end
