//
//  Hero.h
//  Box2DSwipe
//
//  Created by MacBook on 25/12/11.
//  Copyright 2011 Tim Roadley. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "TRBox2D.h"
#import "baseLevel.h"
#import "GameConfig.h"
#import "ControlLayer.h"
#import "B2UserData.h"
#import "VRope.h"

@interface Hero :  TRBox2D{
	ControlLayer* controls;
	b2World* world;
	CCSpriteBatchNode* ropeSpriteSheet;
	VRope *newRope;
}
-(void)updateHero: (ccTime) dt;
+(Hero*)sharedHero;



@end
