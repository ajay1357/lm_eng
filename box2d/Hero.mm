//
//  Hero.m
//  Box2DSwipe
//  Created by MacBook on 25/12/11.
//  Copyright 2011 Tim Roadley. All rights reserved.
//

#import "Hero.h"
#import "GameConfig.h"
#import "ControlLayer.h"
#import "baseLevel.h"
#define DEGTORAD 0.0174532925

@implementation Hero
static Hero* hero;

NSMutableDictionary *actions ;
NSMutableDictionary *currentActions;
NSMutableArray * currentContacts;
NSMutableArray * inRange;
NSMutableArray * currentJoints;
int activeState;
b2PrismaticJointDef wallJd;
b2PrismaticJoint* wallJoint;

-(void)setCurrentContacts{
	[currentContacts removeAllObjects];
	[inRange removeAllObjects];
	if(self->body->GetContactList() != NULL){
		b2ContactEdge* list = self.body->GetContactList();
		b2Contact* contact;
		b2Fixture* a;
		b2Fixture* b;
		CCSprite* sprite;
		b2Filter filter;
		while (list != NULL) {
			contact = list->contact;
//			NSLog(@"contact POINT (%f, %f)", contact->GetManifold()->localPoint.x, contact->GetManifold()->localPoint.y) ;
//			NSLog(@"CONTACT NORMAL(%f, %f)", contact->GetManifold()->localNormal.x , contact->GetManifold()->localNormal.y);
//			NSLog(@"point Count: %d", contact->GetManifold()->pointCount);
			
			a = contact->GetFixtureA();
			b = contact->GetFixtureB();
			if (a->GetBody()->GetUserData()) {
				sprite = (CCSprite *)a->GetUserData();
				if (sprite.tag != ISHERO) {
					filter =  a->GetFilterData();
				}
			}else {
				filter = b->GetFilterData();
			}
			if (a->IsSensor() || b->IsSensor()) {
				[inRange addObject:[NSNumber numberWithInt:filter.categoryBits]];
			}else{
				[currentContacts addObject:[NSNumber numberWithInt:filter.categoryBits]];
			}
			
			list = list->next;
		}
	}
}

-(void)setCurrentJoints{
	[currentJoints removeAllObjects];
	if(self->body->GetJointList() != NULL){
		b2JointEdge* list = self.body->GetJointList();
		b2Joint* temp;
		while (list != NULL) {
			temp = list->joint;
			if (temp->GetType() != e_mouseJoint) {
				jointUserData* jud = (jointUserData *)temp->GetUserData();
				[currentJoints addObject:[NSNumber numberWithInt:jud->tag]];
				list = list->next;
			}
		}
	}
}

-(BOOL)destroyJoint:(b2Joint*)joint{
	if (joint->GetUserData()!=NULL) {
		world->DestroyJoint(joint);
		return YES;
	}
	return NO;
}


-(void)destroyAllJoints{
	if(self->body->GetJointList() != NULL){
		b2Joint* temp = self.body->GetJointList()->joint;
		b2JointEdge* list = self.body->GetJointList();
		while (temp != NULL) {
			[self destroyJoint:temp];
			if(list->next !=NULL){
				temp = list->next->joint;
			}else {
				temp = NULL;
			}
		}
	}
}

-(b2Body *)getContactWithCategory:(int)category{
	if(self->body->GetContactList() != NULL){
		b2ContactEdge* list = body->GetContactList();
		b2Body* temp;
		b2Contact* contact;
		b2Fixture* a;
		b2Fixture* b;
		while (list != NULL) {
			temp = list->other;
			contact = list->contact;
			a = contact->GetFixtureA();
			b = contact->GetFixtureB();
			if (a->GetFilterData().categoryBits == category || b->GetFilterData().categoryBits == category) {
				return temp;
				break;
			}
//			CCSprite* sprite = (CCSprite *)temp->GetUserData();
//			if (sprite.tag == tag ){
//				return temp;
//				break;
//			}
			list = list->next;
		}
	}
	return NULL;
}

-(b2Joint *)getJointWithTag:(int)tag{
	if(self->body->GetJointList() != NULL){
		b2JointEdge* list = body->GetJointList();
		b2Joint* temp;
		while (list != NULL) {
			temp = list->joint;
			jointUserData* jud = (jointUserData *)temp->GetUserData();
			if (jud->tag == tag ){
				return temp;
				break;
			}
			list = list->next;
		}
	}
	return NULL;
}

-(BOOL)createPullJoint:(b2Body*)other anchorPoint:(b2Vec2)anchor{
	jointUserData *pulljud = new jointUserData;
	pulljud->tag = PULL_ACTOR;
	b2RopeJointDef jd;
	jd.bodyA=body; //define bodies
	jd.bodyB=other;
	jd.localAnchorA = b2Vec2(0,0); //define anchors
	jd.localAnchorB = anchor;
	jd.userData = pulljud;
	jd.maxLength= 2; //define max length of joint = current distance between bodies
	world->CreateJoint(&jd); //create joint
//	newRope = [[VRope alloc] init:body body2:other spriteSheet:ropeSpriteSheet];
//	[vRopes addObject:newRope];

//	b2DistanceJointDef pullJd;
//	pullJd.Initialize(other, body, anchor, b2Vec2(0,0));
//	pullJd.collideConnected = true;
//	pullJd.length = b2Abs(2);
//	pullJd.userData = pulljud;
//	world->CreateJoint(&pullJd);
	return YES;
}

-(BOOL)createRopeJoint{
	jointUserData *ropejud = new jointUserData;
	ropejud->tag = ROPE_ACTOR;
	return NO;
}

-(BOOL)createHangJoint:(b2Body*)other anchorPoint:(b2Vec2)anchor{
	jointUserData *hangjud = new jointUserData;
	hangjud->tag = PULL_ACTOR;
	CCSprite *sprite = (CCSprite*)other->GetUserData();
	if (sprite.tag == RIGHT_HANG) {
		hangjud->direction = 'R';
	}else if(sprite.tag == LEFT_HANG) {
		hangjud->direction = 'L';
	}
	
	b2PrismaticJointDef pjd;
	[self body]->SetFixedRotation(NO);
	b2Vec2 axis(0.0f,1.0f);
	//axis.Normalize();
	pjd.Initialize(other, [self body], b2Vec2(700/PTM_RATIO, 400/PTM_RATIO), axis);
	
	// Non-bouncy limit
	//pjd.Initialize(ground, body, b2Vec2(-10.0f, 10.0f), b2Vec2(1.0f, 0.0f));
	
	pjd.localAnchorA.Set(0,0);
	pjd.localAnchorB = anchor;
	pjd.localAxisA = axis;
	pjd.motorSpeed = 10.0f;
	pjd.maxMotorForce = 100.0f;
	//pjd.enableMotor = true;
	pjd.lowerTranslation = 0.4f;
	pjd.upperTranslation = 0.5f;
	pjd.enableLimit = true;
	pjd.userData = hangjud;
	//pjd.collideConnected = true;
	world->CreateJoint(&pjd);
	return YES;
}

-(BOOL)createWallJoint:(b2Body*)other anchorPoint:(b2Vec2)anchor{
	[self body]->SetFixedRotation(NO);
	b2Vec2 axis(0.0f,1.0f);
	//axis.Normalize();
	wallJd.Initialize(other, [self body], b2Vec2(700/PTM_RATIO, 400/PTM_RATIO), axis);
	wallJd.localAnchorA.Set(0,0);//a little outside the bottom right corner
	wallJd.localAnchorB = anchor;
	wallJd.localAxisA = axis;
	wallJd.motorSpeed = 10.0f;
	wallJd.maxMotorForce = 100.0f;
	//pjd.enableMotor = true;
	wallJd.lowerTranslation = -5.0f;
	wallJd.upperTranslation = 5.0f;
	wallJd.enableLimit = true;
	jointUserData* pjdUserData = new jointUserData;
	pjdUserData->tag = WALL_ACTOR;
	wallJd.userData=pjdUserData;
	wallJoint = (b2PrismaticJoint*)world->CreateJoint(&wallJd);
	[currentActions setValue:[NSNumber numberWithBool: YES] forKey: @"climbAction"];
	return YES;
}

-(void)acctionRun:(Float32) speed{
	if (![currentActions valueForKey: @"acctionRun"] || [self numberOfRunningActions]==0) {
		[self runAction:[actions objectForKey:@"acctionRun"]];
		[currentActions setValue:[NSNumber numberWithBool: YES] forKey: @"acctionRun"];
	}
	if (speed <0 ) {
		self.flipX = NO;
	}else{
		self.flipX = YES;
	}
	body->SetLinearVelocity(b2Vec2(speed, body->GetLinearVelocity().y));
}

-(void)climbAction:(Float32)speed{
	if (![currentActions valueForKey: @"climbAction"]  || [self numberOfRunningActions]==0){
		[self runAction:[actions objectForKey:@"acctionRun"]];
		[currentActions setValue:[NSNumber numberWithBool: YES] forKey: @"climbAction"];
	}
	if (controls.joyStickPos.x <0 ) {
		self.flipX = NO;
	}else{
		self.flipX = YES;
	}
	body->SetLinearVelocity(b2Vec2(0, speed));
}

-(void)jump{
	if ([currentActions valueForKey: @"jumpAction"] == [NSNumber numberWithBool:NO]) {
		[self body]->ApplyLinearImpulse(b2Vec2(0, 4), b2Vec2(body->GetPosition().x, body->GetPosition().y));
		[currentActions setValue:[NSNumber numberWithBool: YES] forKey:@"jumpAction"];
//		NSLog(@"jump");
	}
}

-(void)initActions{
	actions = [[NSMutableDictionary alloc]init] ;
	currentActions = [[NSMutableDictionary alloc] init];
	NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = 1; i <= 12; ++i) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"%i", i]]];
	}
	id acctionRun = [CCSpeed actionWithAction:
									 [CCRepeatForever actionWithAction:
										[CCAnimate actionWithAnimation: 
										 [CCAnimation animationWithFrames:runFrames delay:0.083]]] speed:1];
	[currentActions setValue:[NSNumber numberWithBool: NO] forKey:@"acctionRun"];
	[actions setObject:acctionRun forKey:@"acctionRun"];
	
	id jumpAction = [CCSpeed actionWithAction:
									 [CCRepeatForever actionWithAction:
										[CCAnimate actionWithAnimation: 
										 [CCAnimation animationWithFrames:runFrames delay:0.083]]] speed:1];
	[currentActions setValue:[NSNumber numberWithBool: NO] forKey:@"jumpAction"];
	[actions setObject:jumpAction forKey:@"jumpAction"];
}


-(void)addFixture:(NSString *)fixtureName{
	b2CircleShape	head;
	head.m_radius = 0.3;
	b2PolygonShape tummy;
	tummy.SetAsBox(0.4, 0.4);
	b2PolygonShape leg;
	if (fixtureName == @"stand") {
		head.m_p = b2Vec2(0.05, 0.7);
		leg.SetAsBox(0.3, 0.2, b2Vec2(0.05, -0.55), 0);
	}
	
	b2FixtureDef  fdHead;
	fdHead.shape = &head;
	fdHead.friction = 0.1;
	fdHead.restitution = 0.4;
	fdHead.density = 1;
	fdHead.isSensor = false;
	fdHead.filter.categoryBits = HERO;
	fdHead.filter.maskBits = 0xffff;
	self.body->CreateFixture(&fdHead);
	
	b2FixtureDef  fdTummy;
	fdTummy.shape = &tummy;
	fdTummy.friction = 0.1;
	fdTummy.restitution = 0.4;
	fdTummy.density = 1;
	fdTummy.isSensor = false;
	fdTummy.filter.categoryBits = HERO;
	fdTummy.filter.maskBits = 0xffff;
	self.body->CreateFixture(&fdTummy);
	
	b2FixtureDef  fdLeg;
	fdLeg.shape = &leg;
	fdLeg.friction = 0.4;
	fdLeg.restitution = 0;
	fdLeg.density = 1;
	fdLeg.isSensor = false;
	fdLeg.filter.categoryBits = HERO;
	fdLeg.filter.maskBits = 0xffff;
	self.body->CreateFixture(&fdLeg);
	
	//add semicircle radar sensor to tower
	float radius = 1;
	b2Vec2 vertices[8];
	vertices[0].Set(0,0);
	for (int i = 0; i < 7; i++) {
		float angle = i / 6.0 * 90 * DEGTORAD;
		vertices[i+1].Set( radius * cosf(angle), radius * sinf(angle) );
	}
	b2PolygonShape radarShape;
	b2FixtureDef radarFixtureDef;
	radarShape.Set(vertices, 8);
	radarFixtureDef.shape = &radarShape;
	radarFixtureDef.isSensor = true;
	radarFixtureDef.density = 0;  
	[self body]->CreateFixture(&radarFixtureDef);
	
}


-(id)init{
	if (self == [super init]) {
		world = [baseLevel sharedBaseLevel].world;
		controls = [ControlLayer getInstance];
		currentContacts = [[NSMutableArray alloc] init] ;
		currentJoints = [[NSMutableArray alloc] init];
		inRange = [[NSMutableArray alloc] init];
		self.tag = ISHERO;
		self.flipX = YES;
		[self setPosition:ccp(100, 600)];
		activeState = IN_AIR;
		[self setScale:01];
		[self createBodyInWorld:world
								 b2bodyType:b2_dynamicBody
											angle:0.0 
								 allowSleep:true 
							fixedRotation:true
										 bullet:false];
		[self addFixture:@"stand"];
		self.body->SetLinearDamping(1);  // slow the ball a bit so it doesn't fly around the screen like crazy
		ropeSpriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"rope.png" ];
		[self addChild:ropeSpriteSheet];
		
		[self initActions];
		
		
		
	}
	return self;
}

+(Hero*)sharedHero{
	if (!hero) {
		hero = [[self alloc]init];
	}
	return hero;
}

-(void)connectToRope{
	activeState = ON_ROPE;
}

-(void)connectToWall{
	b2Body *other = [self getContactWithCategory:WALL];
	if (other==NULL) {
		return;
	}
	b2Vec2 anchor;
	if(other->GetPosition().x < [self body]->GetPosition().x){
		anchor =  b2Vec2(-0.81f,0);
	}else anchor = b2Vec2(0.81f,0);
	[self createWallJoint:other anchorPoint:anchor];
	activeState = ON_WALL;
}

-(void)connectToMovable{
	b2Body *other = [self getContactWithCategory:MOVEABLE];
	if (other==NULL) {
		return;
	}
	b2Vec2 anchor=b2Vec2(0,0);
	[self createPullJoint:other anchorPoint:anchor];
}

-(void)disconnectMovable{
	b2Joint *joint = [self getJointWithTag:PULL_ACTOR];
	if (joint==NULL) {
		return;
	}
	[self destroyJoint:joint];
}


-(void)throwBody:(b2Vec2)direction{
	
	b2BodyDef bodyDef7;
	bodyDef7.type = b2_dynamicBody;
	bodyDef7.allowSleep = YES;
	bodyDef7.position.Set(body->GetPosition().x,body->GetPosition().y+1);
	
	b2Body *body7 = world->CreateBody(&bodyDef7);
	
	b2PolygonShape kinematicBox7;
	kinematicBox7.SetAsBox(0.1f, 0.1f);
	
	b2FixtureDef fixtureDef7;
	fixtureDef7.shape = &kinematicBox7;	
	fixtureDef7.density = 1.0f;
	fixtureDef7.friction = 0.4f;
	fixtureDef7.filter.categoryBits = 16;
	body7->CreateFixture(&fixtureDef7);
	
	body7->ApplyLinearImpulse(direction, b2Vec2(body->GetPosition().x,body->GetPosition().y+1));
}



-(void)updateHero: (ccTime) dt{
	if ([currentContacts indexOfObject:[NSNumber numberWithInt: ENEMY]] != NSNotFound && currentContacts!= NULL) {
		// DESTROY ALL THE JOINTS
		[self destroyAllJoints];
		// KILL OR DEDUCT THE HEALTH
	}else{
		//UPDATE CONACTS
		[self setCurrentContacts];
//		NSLog(@"CONTACTS: %@", currentContacts);
//		NSLog(@"INRANGE : %@", inRange);
		//		UPDATE JOINTS
		[self setCurrentJoints];
		if (currentContacts ==NULL && currentJoints == NULL) {
			activeState = IN_AIR;
			return;
		}
		if (activeState == IN_AIR || [currentActions valueForKey: @"jumpAction"] == [NSNumber numberWithBool:YES]) {
			if (controls.taskBtn) {
				[self throwBody: b2Vec2(controls.joyStickPos.x, controls.joyStickPos.y)];
			}
			if (controls.joyStickPos.x != 0 ) {
				[self acctionRun:4*controls.joyStickPos.x];
			}
			if(self.position.y<-20){
				[currentActions setValue:[NSNumber numberWithBool: NO] forKey:@"jumpAction"];
				body->SetTransform(b2Vec2(700/PTM_RATIO, 600/PTM_RATIO), 0);
				self.position = CGPointMake( 700, 600);
				return;
			}
			else if ([currentContacts indexOfObject:[NSNumber numberWithInt: PLATFORM]] != NSNotFound ) {
				[currentActions setValue:[NSNumber numberWithBool: NO] forKey:@"jumpAction"];
				activeState = ON_PLATFORM;
				if (controls.joyStickPos.x != 0 ) {
					[self acctionRun:4*controls.joyStickPos.x];
				}
			}else if ([currentContacts indexOfObject:[NSNumber numberWithInt: L_HANG]] != NSNotFound) {
				[currentActions setValue:[NSNumber numberWithBool: NO] forKey:@"jumpAction"];
				b2Body *other = [self getContactWithCategory:L_HANG];
				b2Vec2 anchor = b2Vec2(+0.81, 0);
				[self createHangJoint:other anchorPoint:anchor];
				activeState = HANGING;
				
			}else if([currentContacts indexOfObject:[NSNumber numberWithInt: R_HANG]] != NSNotFound){
				[currentActions setValue:[NSNumber numberWithBool: NO] forKey:@"jumpAction"];
				b2Body *other = [self getContactWithCategory:R_HANG];
				b2Vec2 anchor = b2Vec2(-0.81, 0);
				[self createHangJoint:other anchorPoint:anchor];
				activeState = HANGING;
			}
		}else
			if (activeState == ON_PLATFORM) {
				if (controls.taskBtn) {
					[self throwBody: b2Vec2(controls.joyStickPos.x, controls.joyStickPos.y)];
				}
				// Check for rope contact
				if ([currentContacts indexOfObject:[NSNumber numberWithInt: ROPE]] != NSNotFound) {
					[self connectToRope];
				}else
					//	 Check for wall contact
				if ([currentContacts indexOfObject:[NSNumber numberWithInt: WALL]] != NSNotFound) {
					[self connectToWall];
				}else {
					// Check for controlls
					if (controls.joyStickPos.x != 0 ) {
						if ([currentContacts indexOfObject:[NSNumber numberWithInt: PLATFORM]] != NSNotFound) {
								[self acctionRun:4*controls.joyStickPos.x];;
						}
					}
					if(controls.jmpBtn ){
						if ([currentJoints indexOfObject:[NSNumber numberWithInt:PULL_ACTOR]] == NSNotFound) {
							[self jump];
						}
					}else if (controls.actBtn) {
						if ([currentJoints indexOfObject:[NSNumber numberWithInt:PULL_ACTOR]] != NSNotFound) {
							[self disconnectMovable];
						}
							//					check for pickable
						else if ([inRange indexOfObject:[NSNumber numberWithInt: PICKABLE]] != NSNotFound) {
						}
							//					check for moveable
						else if ([inRange indexOfObject:[NSNumber numberWithInt: MOVEABLE]] != NSNotFound) {
							[self connectToMovable];
						}
					}
				}
			}
			else if(activeState == ON_WALL){
				if (controls.joyStickPos.y!=0) {
					[self climbAction:4*controls.joyStickPos.y];
				}else{
					body->SetLinearVelocity(b2Vec2(0, 0.2f));
				}
				if (controls.joyStickPos.x != 0) {
					if (controls.joyStickPos.x<0) {
						wallJd.localAnchorB.Set(0.81f,0);
					}else if(controls.joyStickPos.x>0){
						wallJd.localAnchorB.Set(-0.81f,0);
					}
					[self destroyJoint:wallJoint];
					wallJoint = (b2PrismaticJoint*)world->CreateJoint(&wallJd);
				}
				if (controls.jmpBtn) {
					[self destroyJoint:wallJoint];
					[self jump];
				}
			}
			else if(activeState == ON_ROPE){
				
			}else if(activeState == HANGING){
				if ([currentJoints indexOfObject:[NSNumber numberWithInt:HANG_ACTOR]] == NSNotFound) {
					// Check FOR STATUS CHANGE
					if ([currentContacts indexOfObject:[NSNumber numberWithInt: L_HANG]] == NSNotFound || [currentContacts indexOfObject:[NSNumber numberWithInt: L_HANG]] == NSNotFound) {
						activeState = IN_AIR;
					}
				}else{
//					GEt Direction
						b2Joint *joint = [self getJointWithTag:HANG_ACTOR];
						jointUserData* jud = (jointUserData *)joint->GetUserData();
//				CHeck for climb
					if (controls.joyStickPos.y > 0) {
						// Climb
						if (jud->direction == 'L' && controls.joyStickPos.x<0) {
							[self destroyJoint:	joint];
							[self body]->SetFixedRotation(YES);
							body->SetTransform((b2Vec2(body->GetPosition().x - 1.0f, body->GetPosition().y + 0.6f)), 0);
						}else if(jud->direction == 'R' && controls.joyStickPos.x>0){
							[self destroyJoint:	joint];
							[self body]->SetFixedRotation(YES);
							body->SetTransform((b2Vec2(body->GetPosition().x + 1.0f, body->GetPosition().y + 0.6f)), 0);
						}
					}else
//				check for drop
					if (controls.actBtn) {
						[self destroyJoint:	joint];
						[self body]->SetFixedRotation(YES);
						activeState = IN_AIR;
					}else
//				CHeck for jump
					if (controls.jmpBtn) {
						[self destroyJoint:	joint];
						[self body]->SetFixedRotation(YES);
						[self jump];
						activeState = IN_AIR;
					}
				}
			}
	}
}



@end
