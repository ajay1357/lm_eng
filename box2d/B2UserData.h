

struct jointUserData {
	int tag;
	char direction;
};

#define DEGTORAD 0.0174532925199432957f

enum _entityCategory {
	BOUNDARY =  0x0001,
	HERO = 0x0002,
	ROPE = 0x0004,
	ENEMY = 0x0008,
	PLATFORM = 0x0010,
	SIDES = 0x0020,
	R_HANG = 0x0040,
	L_HANG = 0x0080,
	WALL = 0x0100,
	MOVEABLE = 0x0200,
	PICKABLE = 0x400,
};

enum _activeState{
	ON_PLATFORM =  1,
	ON_ROPE = 2,
	ON_WALL = 3,
	IN_AIR = 4,
	HANGING = 5,
};

enum _JointTag{
	ROPE_ACTOR =  1,
	ROPE_ROPE = 2,
	PULL_ACTOR = 3,
	CUTTABLE = 4,
	HANG_ACTOR =  5,
	WALL_ACTOR = 6,
	PICK_ACTOR = 7,
	CUT_ACTOR = 8,
	ROOF_ACTOR = 9,
};


enum _BodyTag{
	ISWALL =  1,
	ISHERO = 2,
	ISPLATFORM = 3,
	ISENEMY = 4,
	ISPICKABLE =  5,
	ISMOVEABLE = 6,
	ISROOF=7,
	ISROPE =8,
	ISNONE =9,
	RIGHT_HANG = 10,
	LEFT_HANG = 11,
};




