			//
//  HelloWorldLayer.mm
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "Page4.h"
//#import "ControlLayer.h"


// HelloWorldLayer implementation
@implementation Page4
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//ControlLayer *controlls = [[ControlLayer alloc] initWithLayer];
//	[scene addChild:controlls z:5];
	// 'layer' is an autorelease object.
	Page4 *layer = [Page4 node];

	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}



// PAGE 4

-(void)eyeblink{
    id blink = [CCBlink actionWithDuration:2 blinks:1];
    [[[self getChildByTag:3] getChildByTag:15] runAction:[CCRepeatForever actionWithAction: blink]];
}

-(void)moveHand{
    id rotate = [CCRepeatForever actionWithAction: [CCSequence actions:[CCRotateBy actionWithDuration:1 angle: -10], [CCRotateBy actionWithDuration:1 angle: 10], nil]];
    [[[self getChildByTag:3] getChildByTag:18] runAction:rotate];
}

-(void)animateFin{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = 1; i <= 12; i++) {
        
            [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"fin%i.png", i]]];
       		
	}
    

    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.083]];
    id animate = [CCSpeed actionWithAction:
                  [CCRepeatForever actionWithAction:[CCSequence actions:forward,[forward reverse],nil]] speed:1];
    
    [[[self getChildByTag:3] getChildByTag:16] runAction:animate];
    
    
    
}

-(void)animatejelly{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = 1; i <= 12; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"jelly%i.png", i]]];
	}

    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    id animate = [CCSpeed actionWithAction:
                  [CCRepeatForever actionWithAction:forward] speed:1.2];
    id up = [CCMoveBy actionWithDuration:1 position:ccp(-20, 20)];
    id down = [CCMoveBy actionWithDuration:1 position:ccp(20, -20)];
    id move = [CCRepeatForever actionWithAction:[CCSequence actions:up, down, nil]];
    [[self getChildByTag:4] runAction:move];
    [[self getChildByTag:4] runAction:animate];
    
   
}

-(void)sparkling{
    for (int i=6; i<=10; i++) {
        float pscale = ((float)(arc4random()%100+150)) / 100.0f;
        
        id scalel = [CCScaleTo actionWithDuration:pscale/2 scale:1];
        id scales = [CCScaleTo actionWithDuration:pscale/2 scale:0];
        id scale = [CCSequence actions:scalel, scales, nil];
        [[[self getChildByTag:2] getChildByTag:i] runAction:[CCRepeatForever actionWithAction: scale]];
       
    }
    for (int i=35; i<=39; i++) {
        float pscale = ((float)(arc4random()%100+150)) / 100.0f;
       
        id scalel = [CCScaleTo actionWithDuration:pscale/2 scale:1];
        id scales = [CCScaleTo actionWithDuration:pscale/2 scale:0];
        id scale = [CCSequence actions:scalel, scales, nil];
        [[[self getChildByTag:2] getChildByTag:i] runAction:[CCRepeatForever actionWithAction: scale]];
        
    }
    for (int i=20; i<=34; i++) {
        float pscale = ((float)(arc4random()%100+150)) / 100.0f;
        
        id scalel = [CCScaleTo actionWithDuration:pscale/2 scale:1];
        id scales = [CCScaleTo actionWithDuration:pscale/2 scale:0];
        id scale = [CCSequence actions:scalel, scales, nil];
        [[[self getChildByTag:3] getChildByTag:i] runAction:[CCRepeatForever actionWithAction: scale]];
        
    }
    
}

-(void)arkMove{
    id move = [CCSequence actions:[CCRotateBy actionWithDuration:3 angle: -20], [CCRotateBy actionWithDuration:3 angle: 20], nil];

    [[self getChildByTag:2] runAction:move];
    [[self getChildByTag:3] runAction:[[move copy] autorelease]];
}

-(void)popup:(id)item{
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id scaleflower = [CCSequence actions:scaleup, scaledown, nil];
    [item runAction:scaleflower];
    
}

-(void)popflower{
    TRBox2D *item;
    item = [[[TRBox2D alloc] initWithSpriteFrameName:@"flower.png"] autorelease] ;
    [item setVertexZ:14] ;
    item.position = location;
    item.scale = 0.2;
    
    //		item.flipX = YES;
    b2BodyType type = b2_dynamicBody;
    
    [item createBodyInWorld:world
                 b2bodyType:type
                      angle:0.0
                 allowSleep:true 
              fixedRotation:false
                     bullet:false
     ];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"flower"];
    
    item.tag = -202;
    [self addChild:item z:14];
    
    b2Vec2 position = b2Vec2(touchedSprite.position.x/PTM_RATIO, touchedSprite.position.y/PTM_RATIO);
    
    [self throwBodies:b2Vec2(1,1) position:position tbody:[item body] impulse:b2Vec2(int(arc4random()%3)-1,(arc4random()%3+1))];
    [self popup:item];
    [interections addObject:item];
}



-(void)followTouch:(int)tag{
    CCSprite* fish  = (CCSprite*)[self getChildByTag:tag];
    CGPoint fpos = fish.position;
    float theta = atan((fpos.y-location.y)/(fpos.x-location.x)) * 180 * 7 /22;
    
    float calculatedAngle = 0;
    
    if(fpos.y - location.y > 0)
    {
        if(fpos.x - location.x < 0)
        {
            calculatedAngle = (-90-theta);
        }
        else if(fpos.x - location.x > 0)
        {
            calculatedAngle = (90-theta);
        }       
    }
    else if(fpos.y - location.y < 0)
    {
        if(fpos.x - location.x < 0)
        {
            calculatedAngle = (270-theta);
        }
        else if(fpos.x - location.x > 0)
        {
            calculatedAngle = (90-theta);
        }
    }
    calculatedAngle -= 90;
    

    NSLog(@"Angle: %f", calculatedAngle - fish.rotation);
    

    int dis = (12-tag)*50;

    
    CGPoint pos = CGPointMake(location.x - dis, location.y-dis);
    float distance = ccpDistance(fpos, pos);
    
    id rotate = [CCRotateTo actionWithDuration:(calculatedAngle*0.001) angle:calculatedAngle];
    id move = [CCMoveTo actionWithDuration:distance*1/150 position:pos];
    [[self getChildByTag:tag] runAction:[CCSequence actions:rotate, move, nil]];
}


-(void)fishes{
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
   
    CCParticleSystemQuad *fishes = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    fishes.tag = 25;
	fishes.texture = [[CCSprite spriteWithFile:@"fish.png"] texture] ;
	fishes.emitterMode = kCCParticleModeGravity;
	fishes.duration=-1.00;
	fishes.startColor = startColor;
	fishes.endColor = startColor;
    fishes.startColorVar = startColorVar;
	fishes.endColorVar = startColorVar;
	fishes.gravity = ccp(-1,0);
	fishes.autoRemoveOnFinish = YES;
	fishes.position = ccp(1100, 600);
	fishes.posVar = ccp(200, 100);
	fishes.totalParticles = 10;
	fishes.emissionRate = 2;
	fishes.life = 60;
	fishes.lifeVar = 10;
	fishes.startSize = 50;
	fishes.startSizeVar = 30;
	fishes.endSize = 10;
	fishes.endSizeVar = 5;
    fishes.angleVar =20;
 
	[self addChild:fishes z:1];
}


// PAGE 4 ENDS


// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:4])) {
//        self.scale = 2;
        
		// enable touches
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)waterRipple
{
    id waves = [CCWaves actionWithDuration:5 size:ccg(32, 24) waves:5 amplitude:5 horizontal:YES vertical:NO];
    [[self getChildByTag:17] runAction: [CCRepeatForever actionWithAction: waves]];

}

-(void)glowStingRay
{
    
    id fadein = [CCFadeIn actionWithDuration:0.3];
    id fadeout = [CCFadeOut actionWithDuration:0.3];
    
    id glow = [CCRepeat actionWithAction:[CCSequence actions:fadein,fadeout,nil] times:10];
    
    
    [[[self getChildByTag:5] getChildByTag:1] runAction:glow];

}




-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
 
//    [self sparkling:CGPointMake(130, -30) tag:20 sprite:(TRBox2D *)[self getChildByTag:3] startSize:40 posvar:CGPointMake(30, 60) life:10 erate:15 tItem:80];
//    [self water:ccp(-30, 240) gravity:30];
//    [self water:ccp(1054, 540) gravity:-30];
//    [self eyeblink];
//    [self moveHand];
//    [self animateFin];
//    [self animatejelly];
//    [self sparkling];
//
//    [self createBubbles:ccp(300, 200) index:2 sprite:NULL endsize:30];
//    [self createBubbles:ccp(550, 0) index:1 sprite:NULL endsize:30];
//    [self createBubbles:ccp(750, 300) index:2 sprite:NULL endsize:30];
//
//    
//    [self waterRipple];
    
    
    
}

-(void) tick: (ccTime) dt{
    [super tick:dt];

	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
            
			
//			CCSprite *myActor = (CCSprite*)b->GetUserData();
//            if (myActor.tag==1 || myActor.tag==5 || myActor.tag==6) {
//                b->ApplyForce( b2Vec2(0.0,0.5*b->GetMass()),b->GetWorldCenter());
//            }
		}	
    }
    
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super ccTouchesBegan:touches withEvent:event];
//    
//    if (touchedSprite.tag == 2 || touchedSprite.tag == 3 || touchedSprite.tag == 6 || touchedSprite.tag == 7 || touchedSprite.tag == 8 || touchedSprite.tag == 9 || touchedSprite.tag == 10 || touchedSprite.tag == 15 || touchedSprite.tag == 16 || touchedSprite.tag == 21 || touchedSprite.tag == 18 || touchedSprite.tag == 19 || touchedSprite.tag == 20)
//    
//    
    switch(touchedSprite.tag){
     
       case 2: case 3: case 6: case 7: case 8: case 9: case 10: case 15: case 16: case 21: case 18:case 19: case 20: 
        {
        if ([[self getChildByTag:2] numberOfRunningActions]>0) {
//            [[self getChildByTag:11] stopAllActions];
            [self followTouch:11];
//            [[self getChildByTag:12] stopAllActions];
            [self followTouch:12];
//            [[self getChildByTag:13] stopAllActions];
            [self followTouch:13];
//            [[self getChildByTag:14] stopAllActions];
            [self followTouch:14];
            [self followTouch:15];
            [self followTouch:16];
            [self followTouch:1];
            [[SimpleAudioEngine sharedEngine] playEffect:@"P4_1_Fish point.mp3"];
            return;
            
            
        }
            break;
    }
    
        case 0: {
//        [[self getChildByTag:11] stopAllActions];
        [self followTouch:11];
//        [[self getChildByTag:12] stopAllActions];
        [self followTouch:12];
//        [[self getChildByTag:13] stopAllActions];
        [self followTouch:13];
//        [[self getChildByTag:14] stopAllActions];
        [self followTouch:14];
        [self followTouch:15];
        [self followTouch:16];
        [self followTouch:1];
        
         [[SimpleAudioEngine sharedEngine] playEffect:@"P4_1_pointing fish2.mp3"];
            break;
    }
  }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super ccTouchesMoved:touches withEvent:event];
    
    if(touchedSprite.tag==5)
    {
    [[SimpleAudioEngine sharedEngine] playEffect:@"P4_3_red jelly fish.mp3"];
        
        if([[[self getChildByTag:5] getChildByTag:1] numberOfRunningActions]<1){
            [self glowStingRay];}
    
    }
    
    if (!touchedBody && !touchedSprite) {
        [self getChildByTag:25].position = location;
    }
    if (touchedSprite.tag < -201) {
                touchedSprite.position = location;
                [self getChildByTag:100].position = location;
            }
}

- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
	if (touchedSprite) {
        
      
     switch(touchedSprite.tag){
                
        case 2: case 3: case 6: case 7: case 8: case 9: case 10: case 15: case 16: case 21: case 18:case 19: case 20: {
            if ([[self getChildByTag:2] numberOfRunningActions]>0) {
                break;
            }
            [self arkMove];
                break;
           }
             
        case 17: {
            if (touchedSprite.scale<0.5) {
                [self popup:touchedSprite];
            }else{
                //POP UP FLOWER
                [self popflower];
               [[SimpleAudioEngine sharedEngine] playEffect:@"P4_2_Flower.mp3"]; 
            }
            break;
        }
        
         case 4:  {
        [[SimpleAudioEngine sharedEngine] playEffect:@"P4_3_red jelly fish.mp3"]; 
             break;
        }
         case -202:{
             if(!(location.x>275&&location.y>445&&location.x<540&&location.y<675))
             {if(touchedBody){
                 [self world]->DestroyBody(touchedBody);
                 [interections removeObject:touchedSprite];
                 [self removeChild:touchedSprite cleanup:YES];}
            }
         }
             break;
        
        
     }
        
        touchedBody = NULL;
        touchedSprite = NULL;
    }
}


- (void) dealloc
{

	[super dealloc];
}
@end
