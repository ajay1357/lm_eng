//
//  baseLevel.h
//  box2d
//
//  Created by MacBook on 24/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "TRBox2D.h"
#import "GLES-Render.h"
#import "MyContactListener.h"
#import "GameConfig.h"
#import "GB2ShapeCache.h"
#import "B2UserData.h"
#import "CCParallaxScrollNode.h"
#import "SimpleAudioEngine.h"

#import <UIKit/UIViewController.h>
#import <UIKit/UIKit.h>
#import "ThumbImageView.h"


@interface baseLevel : CCLayerColor <CDLongAudioSourceDelegate,UIScrollViewDelegate,ThumbImageViewDelegate> {
	MyContactListener *_contactListener;
	GLESDebugDraw *m_debugDraw;
	b2Body* groundBody ;
	b2World* world;
	CGSize screenSize;
	b2MouseJoint * mouseJoint;
	CCArray *interections;
	CCLabelTTF *llable;
    CCLabelTTF *alable;
    b2Body* touchedBody;
    CCSprite *touchedSprite;
    CGPoint location;
    int currentPage;
    CCParallaxScrollNode *parallax;
    
    NSMutableDictionary *textData;
    NSDictionary *mediaData;
    NSString *narrationSound ,*bgSound ;
    char narrationArray[15];
    
    UIView *upperView;
    UIScrollView *thumbScrollView;
    UIView       *slideUpView;
    BOOL thumbViewShowing;
	NSTimer *autoscrollTimer;  // Timer used for auto-scrolling.
	float autoscrollDistance;  // Distance to scroll the thumb view when auto-scroll timer fires.
	CGPoint touchPoint;
  //  CCSprite* thumb ;
    float textXPos,textYPos;
    bool canChangeScene;

    
}


-(id)initWithPage:(int)pgNo;
-(void)addText;
-(void)addButtons;
-(void)NarrateMe;
-(void)tick: (ccTime) dt;
-(void)createBubbles:(CGPoint)loc index:(int)index sprite:(TRBox2D*)sprite endsize:(float)endSize;
-(void)evilBubbles:(CGPoint)loc index:(int)index sprite:(TRBox2D*)sprite endsize:(float)endSize;
- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node;
-(void)throwBodies:(b2Vec2)direction position:(b2Vec2)position tbody:(b2Body*)tbody impulse:(b2Vec2)impulse;
-(void)sparkling:(CGPoint)location tag:(int)tag sprite:(TRBox2D*)sprite startSize:(float)startSize posvar:(CGPoint)posvar life:(float)life erate:(float)erate tItem:(float)tItem;
-(void)sparkling:(CCNode*)parent from:(int)from till:(int)till;
-(void)animate:(CCNode*)node till:(int)till from:(int)from frame:(NSString*)frame forever:(BOOL)forever speed:(float)speed;
-(void)water:(CGPoint)location gravity:(float)gravity;
-(void)removeitem:(id)sender data:(CCSprite*)item;
-(void)smoke:(CGPoint)loc radius:(float)radius;
-(void)beforeSceneChange;
-(void)sceneChange:(CCSprite*)touchd;
-(void)sceneChangeTo:(int)pageNo;

-(void)makeBody:(NSMutableDictionary*)bodyData bodyTag:(int)tag parent:(CCSprite*)parent;
-(void)makeSprite:(NSMutableDictionary*)spriteData spriteTag:(int)tag parent:(CCSprite*)parent;
-(void)addParallax:(NSMutableDictionary*)spriteData spriteTag:(int)tag parent:(CCSprite*)parent;
-(void)setTouched:(CGPoint)point;

-(void)playSound;
-(void)toggleThumbView;
-(void)textMoveOut:(int)time;
-(void)textMoveIn;
-(void)createSlideUpViewIfNecessary;
//-(void)showMenu:(BOOL)show;
-(void)updateButtons;

-(void)hideHomeMenu;
-(void)showHomeMenu;
-(void)toggleCredits;
-(void)toggleText;
-(void)pressIcon:(int)tag;
-(void)menuSparkle;


@property (nonatomic, readwrite) b2World* world;
@property (nonatomic, retain) CCParallaxScrollNode *parallax;
@property(nonatomic, retain) CCArray *interections;
@property (nonatomic, readwrite) b2Body* groundBody;
@property (nonatomic, readwrite) CGPoint location;
@end
