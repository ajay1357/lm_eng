//
//  HelloWorldLayer.mm
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "Page2.h"
//#import "ControlLayer.h"


// HelloWorldLayer implementation
@implementation Page2
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//ControlLayer *controlls = [[ControlLayer alloc] initWithLayer];
//	[scene addChild:controlls z:5];
	// 'layer' is an autorelease object.
	Page2 *layer = [Page2 node];

	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}


-(void)fishes{
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    CCParticleSystemQuad *fishes = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    fishes.tag = 25;
	fishes.texture = [[CCSprite spriteWithFile:@"fish.png"] texture] ;
	fishes.emitterMode = kCCParticleModeGravity;
	fishes.duration=-1.00;
	fishes.startColor = startColor;
	fishes.endColor = startColor;
    fishes.startColorVar = startColorVar;
	fishes.endColorVar = startColorVar;
	fishes.gravity = ccp(-1.5,0);
	fishes.autoRemoveOnFinish = YES;
	fishes.position = ccp(1100, 600);
	fishes.posVar = ccp(200, 100);
	fishes.totalParticles = 10;
	fishes.emissionRate = 2;
	fishes.life = 60;
	fishes.lifeVar = 10;
	fishes.startSize = 50;
	fishes.startSizeVar = 30;
	fishes.endSize = 10;
	fishes.endSizeVar = 5;
    fishes.angleVar =20;
    //    bees3.rotation = -2434;
    //    [fishes setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
	[self addChild:fishes];
}



-(void) animateEyes{
    
	id show = [CCBlink actionWithDuration:3 blinks:4];
    id gap = [CCActionInterval actionWithDuration:arc4random()%5 + 5];
    
     
    id blink = [CCRepeatForever actionWithAction:[CCSequence actions:show,gap,nil]];
        
	[[[self getChildByTag:2] getChildByTag:15] runAction:blink];
    
    [[[self getChildByTag:3] getChildByTag:20] runAction:[[blink copy] autorelease]];
    
    [[[self getChildByTag:7] getChildByTag:1] runAction:[CCRepeatForever actionWithAction:[[show copy] autorelease]]];
    
    [[[self getChildByTag:8] getChildByTag:1] runAction:[CCRepeatForever actionWithAction:[[show copy] autorelease]]];
    
    
}



// on "init" you need to initialize your instance
-(id) init
{
	
	if( (self=[super initWithPage:2])) {

        
	}
	return self;
}



-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
    [self sparkling:CGPointMake(290, 100) tag:20 sprite:(TRBox2D *)[self getChildByTag:2] startSize:40 posvar:CGPointMake(40, 65) life:5 erate:10 tItem:50];
    
    [self sparkling:[self getChildByTag:2] from:14 till:14];
    [self sparkling:[self getChildByTag:3] from:15 till:19];
    [self sparkling:[self getChildByTag:12] from:1 till:5];
    
    [self animate:[[self getChildByTag:2] getChildByTag:13] till:10 from:1 frame:@"fin" forever:YES speed:1];
    [self animate:[self getChildByTag:9] till:9 from:1 frame:@"weedr" forever:YES speed:1.4];
    [self animate:[self getChildByTag:10] till:10 from:1 frame:@"weed" forever:YES speed:1.4];
    [self animate:[self getChildByTag:11] till:10 from:1 frame:@"Weed" forever:YES speed:2.0];
    
    [self water:ccp(-30, 240) gravity:30];
    [self water:ccp(1054, 540) gravity:-30];
    [self fishes];
    [self animateEyes];
}

-(void) tick: (ccTime) dt{
    [super tick:dt];

	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
           			
			CCSprite *myActor = (CCSprite*)b->GetUserData();
            if (myActor.tag==1 || myActor.tag==5 || myActor.tag==6) {
                b->ApplyForce( b2Vec2(0.0,0.5*b->GetMass()),b->GetWorldCenter());
            }
		}	
    }
    
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
	
    [super ccTouchesMoved:touches withEvent:event];
    
    if(touchedSprite.tag==8)
    {
        
        [[SimpleAudioEngine sharedEngine] playEffect:@"P2_2_bubbles.mp3"];
        
    }

}


-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    switch(touchedSprite.tag){
        case 2:
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"P1_2 hinge_spring_2.mp3"];
            break;
    
        }
        case 3:
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"P1_2_hinge_spring.mp3"];
            break;
            
        }
    
        case 8:
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"P1_2_hinge_spring.mp3"];
            break;
        }
    
        case 7:
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"P1_2_hinge_spring.mp3"];
            break;
        }
    
    }
    
    [super ccTouchesEnded:touches withEvent:event];
    

}


- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    
	if (touchedBody) {
        CCSprite* sprite = (CCSprite *)touchedBody->GetUserData();
        if (sprite.tag==4) {
            
            TRBox2D *item;
            item = [[[TRBox2D alloc] initWithSpriteFrameName:@"page.png"] autorelease] ;
            [item setVertexZ:10] ;
            item.position = touchedSprite.position;
            item.scale = 0.5;
            
            b2BodyType type = b2_dynamicBody;
            
            [item createBodyInWorld:world
                         b2bodyType:type
                              angle:0.0
                         allowSleep:true 
                      fixedRotation:true
                             bullet:false
             ];
            [[GB2ShapeCache sharedShapeCache] addFixturesToBody:[item body] forShapeName:@"page"];
            
            [self addChild:item z:10];
            
            b2Vec2 position = b2Vec2(touchedBody->GetWorldCenter().x, touchedBody->GetWorldCenter().y);
            [self throwBodies:b2Vec2(1,1) position:position tbody:[item body] impulse:b2Vec2(int(arc4random()%10)-5,(arc4random()%5+10))];
            
             [[SimpleAudioEngine sharedEngine] playEffect:@"P2_1_book paper.mp3"];
        }
      
      
    }
    if(touchedSprite)
    {
        if(touchedSprite.tag==0)
        {[[SimpleAudioEngine sharedEngine] playEffect:@"P2_3_Water ripple music bells.mp3"];}
        
    }
    touchedBody = NULL;
    touchedSprite = NULL;
    
}


- (void) dealloc
{

	[super dealloc];
}
@end
