//
//  Page20.m
//  Little Mermaid
//
//  Created by Pipedream on 18/09/12.
//
//

#import "Page15.h"

@implementation Page15

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page15 *layer = [Page15 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:15])) {
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}


-(void)animatePurpleBird
{
    
    NSMutableArray *runFrames = [NSMutableArray array];
    
    [[self getChildByTag:5] runAction:[CCFlipX actionWithFlipX:TRUE]];
    
    for(int i = 1; i <= 5; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"purple%i.png", i]]];
	}
    
    id wings_move1 = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.2]];
    id wings_move2 = [wings_move1 reverse];
    
    id birdFly = [CCSpeed actionWithAction:[CCRepeatForever actionWithAction:[CCSequence actions:wings_move1,wings_move2,nil] ] speed:2];
    
    
    [[self getChildByTag:5] runAction:birdFly];
    
    
  
}

-(void)animateSun
{
    CCSprite *sunSprite = touchedSprite;
    if ([sunSprite numberOfRunningActions]>0) {
        return;
    }
    
    CGPoint sun_position = CGPointMake(sunSprite.position.x, sunSprite.position.y);
    
    id sun_set = [CCMoveTo actionWithDuration:4 position:ccp(sun_position.x,sun_position.y-200)];
    id sun_rise = [CCMoveTo actionWithDuration:4 position:sun_position];
    
    //   flag=0;
    
    id action = [CCSequence actions:sun_set,sun_rise,nil];
    
    [sunSprite runAction:action];
  
    
    
}

-(void)splashSound
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"P5_3_WaterSplashFish.mp3"];
}

-(void)fishSplash
{
    id gap = [CCActionInterval actionWithDuration:1.5];
    id scaleout = [CCScaleTo actionWithDuration:0.7 scale:1];
    id fadeout = [CCFadeOut actionWithDuration:1];
    id scalein = [CCScaleTo actionWithDuration:0 scale:0];
    id fadein = [CCFadeIn actionWithDuration:0];
    id splash = [CCSequence actions:gap,scaleout,fadeout, scalein,fadein, nil];
    
    [[self getChildByTag:19] runAction:splash];

     [self performSelector:@selector(splashSound) withObject:nil afterDelay:1.5f];
}

-(void)animateFish
{
    ccBezierConfig bezier;
    bezier.controlPoint_1 = ccp(42, 200);
    bezier.controlPoint_2 = ccp(270, 350);
    bezier.endPosition = ccp(522, -100);
    id bezierAction = [CCBezierTo actionWithDuration:2 bezier:bezier];
    
    id rotate = [CCRotateTo actionWithDuration:1.5 angle:90];
    id interval = [CCActionInterval actionWithDuration:0.1];
    id fadeout = [CCFadeOut actionWithDuration:1];
    id moveback = [CCMoveTo actionWithDuration:0 position:ccp(42, 160)];
    id rotateback = [CCRotateBy actionWithDuration:0 angle:-90];
    id fadein = [CCFadeIn actionWithDuration:0.7];
    
    
    [[self getChildByTag:18] runAction:[CCSequence actions:interval, rotate, fadeout, moveback, rotateback, fadein, nil]];
    [[self getChildByTag:18] runAction:bezierAction];
    [self fishSplash];

}

-(void)popCrab
{
    
        
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id gap = [CCActionInterval actionWithDuration:1];
    id hide = [CCScaleTo actionWithDuration:0.2 scale:0];
       
    id pop = [CCSequence actions:scaleup, scaledown, gap, hide, nil];
    [[self getChildByTag:17] runAction:pop];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page16_3_hidden.mp3"];
    
}

-(void)animateScissors
{
   
    NSMutableArray *runFrames = [NSMutableArray array];
    
    for(int i = 1; i <= 6; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"scissor%i.png", i]]];
	}
    
    id action = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    id cut = [CCRepeat actionWithAction:action times:15];
    
    [[self getChildByTag:22] runAction:cut];
    [[self getChildByTag:23] runAction:[[cut copy] autorelease]];
    
    id moveScissor1= [CCMoveTo actionWithDuration:2 position:ccp(220,410)];
    id moveScissor1_back= [CCMoveTo actionWithDuration:0 position:ccp(-70,400)];
    id moveScissor2= [CCMoveTo actionWithDuration:2 position:ccp(118,500)];
    id moveScissor2_back= [CCMoveTo actionWithDuration:0 position:ccp(1100,800)];
    
    id gap = [CCActionInterval actionWithDuration:5];
    
    [[self getChildByTag:22] runAction:[CCSequence actions:moveScissor1,gap,moveScissor1_back,nil]];
    [[self getChildByTag:23] runAction:[CCSequence actions:moveScissor2,gap,moveScissor2_back,nil]];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"Page18_4_scissors.mp3"];
        
   }


-(void)animateMermaid
{

    
    NSMutableArray *runFrames = [NSMutableArray array];
    
    for(int i = 1; i <= 13; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"mermid%i.png", i]]];
    }
    
    id action = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    id sobing = [CCSequence actions:action,[action reverse], nil];
    
    [[self getChildByTag:15] runAction:sobing];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"Page18_5_sobbing mermaid.mp3"];
    



}

-(void)tick:(ccTime)dt{
    [super tick:dt];
    if (parallax!=NULL) {
        [parallax updateWithVelocity:ccp(-4,0) AndDelta:dt];
    }
}

- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    //static int flag=1;
    NSLog(@"%f, %f", location.x, location.y);
	if (touchedSprite) {
        switch(touchedSprite.tag){
            case 1:     //Sun Touched
                [self animateSun];
                break;
                
            case 18:if([touchedSprite numberOfRunningActions] == 0){
                [self animateFish];}
                break;
            
            case 12:
                if((location.x<650&&location.y<180)||(location.y>180 && location.x < 525)|| location.y<80)
                {}else{
                    [self popCrab];}
                break;
              
            case 21:
            case 10:
                [self animateScissors];
                break;
            
            case 15:
            case 24:
                if([[self getChildByTag:15] numberOfRunningActions] < 1){
                    [self animateMermaid];}
                break;
                
            case 5:
                 [[SimpleAudioEngine sharedEngine] playEffect:@"Page18_2_Birdsong_short.mp3"];
                break;
        }
        
    }
    touchedSprite = NULL;
    touchedBody = NULL;
}

-(void)rippleEffect
{
    id scaleout = [CCScaleTo actionWithDuration:2.1 scale:1];
    id fadeout = [CCFadeOut actionWithDuration:1.8];
    
    id scalein = [CCScaleTo actionWithDuration:0 scale:0.5];
    id fadein = [CCFadeIn actionWithDuration:0.3];
    id splash = [CCRepeatForever actionWithAction:[CCSequence actions:scaleout,scalein, nil]];
    id fade =   [CCRepeatForever actionWithAction:[CCSequence actions:fadein,fadeout, nil]];
    
    [[self getChildByTag:13] runAction:fade];
    [[self getChildByTag:13] runAction:splash];
 
   // id mermaid_side_ripple = [CCSequence actions:scaleout,scalein, nil];
    
    [[self getChildByTag:9] runAction:[[splash copy] autorelease] ];
    [[self getChildByTag:9] runAction:[[fade copy] autorelease]];
    [[self getChildByTag:11] runAction:[[splash copy] autorelease]];
    [[self getChildByTag:11] runAction:[[fade copy] autorelease]];
}


-(void)floatShip
{
    CCSprite *ship = (CCSprite *)[self getChildByTag:4];
    id rotate = [CCRotateBy actionWithDuration:2 angle:6];
    id rotateback = [CCRotateBy actionWithDuration:2 angle:-6];
    [ship runAction:[CCRepeatForever actionWithAction:[CCSequence actions:rotate, rotateback, nil]]];
    int x_pos= ship.position.x;
    int y_pos = ship.position.y;
    id move_left = [CCMoveTo actionWithDuration:50 position:ccp( x_pos-900, y_pos)];
    id move_right= [CCMoveTo actionWithDuration:0 position:ccp(x_pos , y_pos)];
    [ship runAction:[CCRepeatForever actionWithAction:[CCSequence actions:move_left,move_right, nil]]];
    
}

-(void)glowKnife
{
    id fadein=[CCFadeIn actionWithDuration:0.6];
    id fadeout=[CCFadeOut actionWithDuration:0.3];
    
    id glow = [CCRepeatForever actionWithAction:[CCSequence actions:fadein,fadeout,nil]];
    
    [[self getChildByTag:21] runAction:glow];

}

-(void)birds{
    
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    
    CCParticleSnow *bird1 = [[[CCParticleSnow alloc]init] autorelease];
	bird1.texture = [[CCSprite spriteWithFile:@"bird1.png"] texture] ;
	bird1.autoRemoveOnFinish = YES;
	bird1.position = ccp(200, 480);
	bird1.posVar = ccp(400, 0);
	bird1.totalParticles = 5;
	bird1.emissionRate = 2;
	bird1.life = 10;
	bird1.lifeVar = 5;
	bird1.startSize = 20;
	bird1.startSizeVar = 5;
	bird1.endSize = 30;
	bird1.endSizeVar = 5;
    bird1.startColor = startColor;
    bird1.startColorVar = startColorVar;
    bird1.endColor = startColor;
    bird1.endColorVar = startColorVar;
    bird1.gravity = ccp(1, 5);
	[self addChild:bird1 z:1];
    
    CCParticleSnow *bird2 = [[[CCParticleSnow alloc]init] autorelease];
	bird2.texture = [[CCSprite spriteWithFile:@"bird2.png"] texture] ;
	bird2.autoRemoveOnFinish = YES;
	bird2.position = ccp(200, 480);
	bird2.posVar = ccp(400, 0);
	bird2.totalParticles = 5;
	bird2.emissionRate = 2;
	bird2.life = 20;
	bird2.lifeVar = 5;
	bird2.startSize = 30;
	bird2.startSizeVar = 5;
    
    bird2.startColor = startColor;
    bird2.startColorVar = startColorVar;
    bird2.endColor = startColor;
    bird2.endColorVar = startColorVar;
	bird2.endSize = 0;
	bird2.endSizeVar = 5;
    bird2.gravity = ccp(-1, 5);
	[self addChild:bird2 z:1];
    
    CCParticleSnow *bird3 = [[[CCParticleSnow alloc]init] autorelease];
	bird3.texture = [[CCSprite spriteWithFile:@"bird3.png"] texture] ;
	bird3.autoRemoveOnFinish = YES;
	bird3.position = ccp(200, 480);
	bird3.posVar = ccp(400, 0);
	bird3.totalParticles = 5;
	bird3.emissionRate = 2;
	bird3.life = 20;
	bird3.lifeVar = 5;
	bird3.startSize = 10;
	bird3.startSizeVar = 5;
    
	bird3.endSize = 30;
	bird3.endSizeVar = 5;
    
    bird3.startColor = startColor;
    bird3.startColorVar = startColorVar;
    bird3.endColor = startColor;
    bird3.endColorVar = startColorVar;
    bird3.gravity = ccp(1, 3);
	[self addChild:bird3 z:1];
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
   
    [self animatePurpleBird];
    [self rippleEffect];
    [self floatShip];
    [self glowKnife];
    [self birds];
    
   
}

- (void) dealloc
{
    
	[super dealloc];
}


@end
