//
//  Page9.m
//  Little Mermaid
//
//  Created by Pipedream on 03/09/12.
//
//

#import "Page9.h"

@implementation Page9


+(CCScene *) scene
{
    // 'scene' is an autorelease object.
    CCScene *scene = [CCScene node];
    
    // 'layer' is an autorelease object.
    Page9 *layer = [Page9 node];
    
    // add layer as a child to scene
    [scene addChild: layer];
    // return the scene
    return scene;
}


// PAGE 9 BEGINS
-(void)tears:(CGPoint)pos{
    CCParticleSnow *tear = [[[CCParticleSnow alloc]init] autorelease];
    tear.duration = 5;
    tear.texture = [[CCSprite spriteWithFile:@"teardrop.png"] texture];
    tear.autoRemoveOnFinish = YES;
    tear.position = pos;
    tear.posVar = ccp(0,0);
    tear.totalParticles = 5;
    tear.emissionRate = 0.5;
    tear.life = 7;
    tear.lifeVar = 1;
    tear.startSize = 10;
    tear.startSizeVar = 1;
    tear.gravity = ccp(0,-10);
    tear.endSize = 10;
    tear.endSizeVar = 1;
    [self addChild:tear z:10];
    
    CCParticleSnow *tear1 = [[[CCParticleSnow alloc]init] autorelease];
    tear1.duration = 5;
    tear1.texture = [[CCSprite spriteWithFile:@"teardrop.png"] texture];
    tear1.autoRemoveOnFinish = YES;
    tear1.position = ccp(pos.x+100, pos.y-20);
    tear1.posVar = ccp(0,0);
    tear1.totalParticles = 2;
    tear1.emissionRate = 0.3;
    tear1.life = 7;
    tear1.lifeVar = 1;
    tear1.startSize = 10;
    tear1.startSizeVar = 1;
    tear1.gravity = ccp(0,-10);
    tear1.endSize = 10;
    tear1.endSizeVar = 1;
    [self addChild:tear1 z:10];
    
    
}


-(void)ball{
    ccColor4F startColor;
    startColor.r = 0.45f;
    startColor.g = 0.45f;
    startColor.b = 0.3f;
    startColor.a = 0.8f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    CCParticleSmoke *smoke = [[[CCParticleSmoke alloc] init] autorelease];
    smoke.texture = [[CCSprite spriteWithFile:@"smoke.png"] texture] ;
	smoke.autoRemoveOnFinish = YES;
	smoke.position = ccp(527, 415);
    smoke.startColor = startColor;
    smoke.startColorVar = startColorVar;
    smoke.endColor = startColorVar;
    smoke.endColorVar = startColorVar;
	smoke.posVar = ccp(20, 20);
	smoke.totalParticles = 200;
	smoke.emissionRate = 20;
    smoke.speed = 5;
	smoke.life = 6;
	smoke.lifeVar = 9;
	smoke.startSize = 30;
	smoke.startSizeVar = 0;
    smoke.angleVar = 180;
	smoke.endSize = 50;
	smoke.endSizeVar = 5;
	[self addChild:smoke z:3];
    
}


-(void)evilSmoke:(CGPoint)pos{
    
    ccColor4F startColor;
    startColor.r = 0.45f;
    startColor.g = 0.45f;
    startColor.b = 0.3f;
    startColor.a = 0.5f;
    
    ccColor4F startColorVar;
    startColorVar.r = 1.0f;
    startColorVar.g = 1.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    ccColor4F endcolor;
    endcolor.r = 0.0f;
    endcolor.g = 0.0f;
    endcolor.b = 0.0f;
    endcolor.a = 0.0f;
    
    
    
    CCParticleSmoke *evilsmoke = [[[CCParticleSmoke alloc] init] autorelease];
    evilsmoke.texture = [[CCSprite spriteWithFile:@"smoke.png"] texture] ;
	evilsmoke.autoRemoveOnFinish = YES;
	evilsmoke.position = pos;
    evilsmoke.startColor = startColor;
    evilsmoke.startColorVar = startColorVar;
    evilsmoke.endColor = endcolor;
    evilsmoke.endColorVar = endcolor;
	evilsmoke.posVar = ccp(110, 30);
	evilsmoke.totalParticles = 150;
	evilsmoke.emissionRate = 35;
	evilsmoke.life = 5;
	evilsmoke.lifeVar = 4;
	evilsmoke.startSize = 20;
	evilsmoke.startSizeVar = 0;
    
	evilsmoke.endSize = 100;
	evilsmoke.endSizeVar = 5;
	[self addChild:evilsmoke z:10];
}

-(void)waterripple{
    id waves = [CCWaves actionWithDuration:5 size:ccg(32, 24) waves:3 amplitude:3 horizontal:YES vertical:NO];
    [[self getChildByTag:0] runAction: [CCRepeatForever actionWithAction: waves]];
    id tint = [CCTintTo actionWithDuration:0.2 red:0 green:255 blue:20];
    [[self getChildByTag:0] runAction:tint];
}

-(void)flashy{
    id tint1 = [CCTintBy actionWithDuration:0.2 red:10 green:-30 blue:50];
    id tint2 = [CCTintBy actionWithDuration:0.3 red:50 green:-50 blue:10];
    id tint3 = [CCTintBy actionWithDuration:0.3 red:30 green:50 blue:20];
    
    [[self getChildByTag:0] runAction:[CCSequence actions:tint1, tint2, tint3, [tint3 reverse],[tint2 reverse],[tint1 reverse], nil]];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P11_3_change color.mp3"];
    
}


-(void)witch{
        NSMutableArray *runFrames = [NSMutableArray array];
        for(int i = 2; i <= 9; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"witch%i.png", i]]];
        }
        id animate = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.2]];
    
        [[self getChildByTag:7] runAction:[CCSpeed actionWithAction:
                                           [CCRepeatForever actionWithAction:[CCSequence actions:animate,[animate reverse],nil]] speed:3.2]];
    
   
    
}

-(void)jevel{
    float pscale = ((float)(arc4random()%100+150)) / 100.0f;
    id scalel = [CCScaleTo actionWithDuration:pscale/2 scale:1];
    id scales = [CCScaleTo actionWithDuration:pscale/2 scale:0];
    id scale = [CCSequence actions:scalel, scales, nil];
    [[self getChildByTag:18] runAction:[CCRepeatForever actionWithAction: scale]];
}

-(void)weeds{
    id waves = [CCWaves actionWithDuration:2 size:ccg(32,24) waves:1 amplitude:5 horizontal:YES vertical:NO];
    [[self getChildByTag:16] runAction: [CCRepeatForever actionWithAction: waves]];
}

// PAGE 9 ENDS


-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:9])) {
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)moveOctopus
{
    id scalein = [CCScaleTo actionWithDuration:50 scale:0];
    [[self getChildByTag:10] runAction:scalein];

}


-(void)eyeblink{
    id blink = [CCBlink actionWithDuration:2 blinks:1];
    [[[self getChildByTag:6] getChildByTag:1] runAction:[CCRepeatForever actionWithAction: blink]];
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    
    id flipMermaid = [CCFlipX actionWithFlipX:TRUE];
    [[self getChildByTag:13] runAction:flipMermaid];
    
    [self ball];
    [self evilSmoke:ccp(897, 15)];
    [self evilSmoke:ccp(697, 15)];
    [self waterripple];
    [self witch];
    [self jevel];
    [self evilBubbles:ccp(50, 200) index:12 sprite:NULL endsize:30];
    [self evilBubbles:ccp(550, 0) index:11 sprite:NULL endsize:30];
    [self evilBubbles:ccp(900, 100) index:12 sprite:NULL endsize:30];
    
    [self moveOctopus];
    
    [self eyeblink];
    
}

-(void)shakingEffect:(int)tag
{
    CCSprite *body = (CCSprite *)[self getChildByTag:tag];
    
    if([body numberOfRunningActions] <=0){
    
    id move1= [CCMoveBy actionWithDuration:0.1 position:ccp(10,0)];
    id move2= [CCMoveBy actionWithDuration:0.1 position:ccp(-10,0)];
    
    id scalein = [CCScaleTo actionWithDuration:0.5 scale:0.9];
    id scaleout = [CCScaleTo actionWithDuration:0.5 scale:1];
    
    id scaleAction = [CCRepeat actionWithAction:[CCSequence actions:scalein,scaleout, nil] times:10];
    
    id shakeBody = [CCRepeat actionWithAction:[CCSequence actions:move1,move2,nil] times:30];
    
     [body runAction:shakeBody];
     [body runAction:scaleAction];
    
        [[SimpleAudioEngine sharedEngine] playEffect:@"P11_1_Scull rattle.mp3"];}
    
    }



- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
    
    if(touchedSprite){
	switch(touchedSprite.tag) {

        
        case 0: {
            [self flashy];
            break;
        }
            
        case 19:
        { [self shakingEffect:19];
          break;
        }
            
        case 6:
            [[SimpleAudioEngine sharedEngine] playEffect:@"P11__2_Evil witch.mp3"];
            break;
            
        
        default:
            if(location.x>680 && location.x<950 && location.y>209 && location.y<300)
            {
                [self shakingEffect:20];
            }
            else if(location.x>700 && location.x<940 && location.y>145 && location.y<240)
            {
                [self shakingEffect:21];
            }
            else if(location.x>590 && location.y>180)
                {[self tears:ccp(630, 570)];
                  [[SimpleAudioEngine sharedEngine] playEffect:@"P11_5_sobbing mermaid.mp3"];
                }
            
       }
    }
    touchedBody = NULL;
    touchedSprite = NULL;

}
    
    
    
-(void) dealloc
    {
        [super dealloc];
    
    }
    


@end
