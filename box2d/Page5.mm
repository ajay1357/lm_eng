//
//  HelloWorldLayer.mm
//  box2d
//
//  Created by MacBook on 21/12/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "Page5.h"
//#import "ControlLayer.h"


// HelloWorldLayer implementation
@implementation Page5
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//ControlLayer *controlls = [[ControlLayer alloc] initWithLayer];
//	[scene addChild:controlls z:5];
	// 'layer' is an autorelease object.
	Page5 *layer = [Page5 node];

	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}




// PAGE 5

-(void)moveCloud:(int)tag{
    id move = [CCMoveBy actionWithDuration:8 position:CGPointMake(200, 0)];
    id action = [CCRepeatForever actionWithAction:move];
    [[self getChildByTag:tag] runAction:action];
    
}


-(void)splashSound
{
[[SimpleAudioEngine sharedEngine] playEffect:@"P5_3_WaterSplashFish.mp3"];
}

-(void) dolphinSplash{
    
    id gap = [CCActionInterval actionWithDuration:1.4];
    id fadein = [CCScaleTo actionWithDuration:0.4 scale:1.5];
    id fadeout = [CCScaleTo actionWithDuration:0.4 scale:0];
    id splash = [CCSequence actions:gap,fadein, fadeout, nil];
    [[self getChildByTag:5] runAction:splash];
   
    [self performSelector:@selector(splashSound) withObject:nil afterDelay:1.5f];
        
    
    
}

-(void)dolphinDive{
    
    //bezier, splash move, fade dolphin, reappiar dolphin and splash
    
    ccBezierConfig bezier;
    bezier.controlPoint_1 = ccp(42, 200);
    bezier.controlPoint_2 = ccp(270, 350);
    bezier.endPosition = ccp(522, -100);
    id bezierAction = [CCBezierTo actionWithDuration:2 bezier:bezier];
    
    id rotate = [CCRotateTo actionWithDuration:1.5 angle:90];
    id interval = [CCActionInterval actionWithDuration:0.1];
    id fadeout = [CCFadeOut actionWithDuration:1];
    id moveback = [CCMoveTo actionWithDuration:0 position:ccp(42, 200)];
    id rotateback = [CCRotateBy actionWithDuration:0 angle:-90]; 
    id fadein = [CCFadeIn actionWithDuration:0.7];
    
    
    [[self getChildByTag:6] runAction:[CCSequence actions:interval, rotate, fadeout, moveback, rotateback, fadein, nil]];
    [[self getChildByTag:6] runAction:bezierAction];
    [self dolphinSplash];
    
}

-(void)popFish{
    CCSprite * fish = [CCSprite spriteWithSpriteFrameName:@"fish.png"];
    fish.position = location;
    fish.scale = 0;
    
    [self addChild:fish z:6];
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id gap = [CCActionInterval actionWithDuration:1];
    id hide = [CCScaleTo actionWithDuration:0.2 scale:0];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)fish];
    
    id popfish = [CCSequence actions:scaleup, scaledown, gap, hide, remove, nil];
    [fish runAction:popfish];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"P5_3_Fish out from water.mp3"];
    
    
}




-(void)eyeblink{
    
    
    id show = [CCBlink actionWithDuration:3 blinks:3];
    id gap = [CCActionInterval actionWithDuration:arc4random()%5 + 5];
    
    
    
    id blink = [CCRepeatForever actionWithAction:[CCSequence actions:show,gap,nil]];
    
	[[[self getChildByTag:10] getChildByTag:11] runAction:blink];
    
}

-(void)animateboat{
    id rotate = [CCRotateBy actionWithDuration:2 angle:10];
    id rotateback = [CCRotateBy actionWithDuration:2 angle:-10];
    [[self getChildByTag:3] runAction:[CCRepeatForever actionWithAction:[CCSequence actions:rotate, rotateback, nil]]];
}

-(void)waterripple{
    id waves = [CCWaves actionWithDuration:5 size:ccg(32, 24) waves:3 amplitude:3 horizontal:YES vertical:NO];
    [[self getChildByTag:2] runAction: [CCRepeatForever actionWithAction: waves]];
}

-(void)birds{
    
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    
    CCParticleSnow *bird1 = [[[CCParticleSnow alloc]init] autorelease];
	bird1.texture = [[CCSprite spriteWithFile:@"bird1.png"] texture] ;
	bird1.autoRemoveOnFinish = YES;
	bird1.position = ccp(200, 580);
	bird1.posVar = ccp(400, 0);
	bird1.totalParticles = 5;
	bird1.emissionRate = 2;
	bird1.life = 10;
	bird1.lifeVar = 5;
	bird1.startSize = 20;
	bird1.startSizeVar = 5;
	bird1.endSize = 30;
	bird1.endSizeVar = 5;
    bird1.startColor = startColor;
    bird1.startColorVar = startColorVar;
    bird1.endColor = startColor;
    bird1.endColorVar = startColorVar;
    bird1.gravity = ccp(1, 5);
	[self addChild:bird1 z:1];
    
    CCParticleSnow *bird2 = [[[CCParticleSnow alloc]init] autorelease];
	bird2.texture = [[CCSprite spriteWithFile:@"bird2.png"] texture] ;
	bird2.autoRemoveOnFinish = YES;
	bird2.position = ccp(200, 580);
	bird2.posVar = ccp(400, 0);
	bird2.totalParticles = 5;
	bird2.emissionRate = 2;
	bird2.life = 20;
	bird2.lifeVar = 5;
	bird2.startSize = 30;
	bird2.startSizeVar = 5;
    
    bird2.startColor = startColor;
    bird2.startColorVar = startColorVar;
    bird2.endColor = startColor;
    bird2.endColorVar = startColorVar;
	bird2.endSize = 0;
	bird2.endSizeVar = 5;
    bird2.gravity = ccp(-1, 5);
	[self addChild:bird2 z:1];
    
    CCParticleSnow *bird3 = [[[CCParticleSnow alloc]init] autorelease];
	bird3.texture = [[CCSprite spriteWithFile:@"bird3.png"] texture] ;
	bird3.autoRemoveOnFinish = YES;
	bird3.position = ccp(200, 580);
	bird3.posVar = ccp(400, 0);
	bird3.totalParticles = 5;
	bird3.emissionRate = 2;
	bird3.life = 20;
	bird3.lifeVar = 5;
	bird3.startSize = 10;
	bird3.startSizeVar = 5;
    
	bird3.endSize = 30;
	bird3.endSizeVar = 5;
    
    bird3.startColor = startColor;
    bird3.startColorVar = startColorVar;
    bird3.endColor = startColor;
    bird3.endColorVar = startColorVar;
    bird3.gravity = ccp(1, 3);
	[self addChild:bird3 z:1];
}

-(void)animateturtle{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = 1; i <= 12; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"turtle%i.png", i]]];
	}
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
//    id delay = [CCActionInterval actionWithDuration:5];
    id animate = [CCSpeed actionWithAction:forward speed:1.2];
    [[self getChildByTag:13] runAction:animate];
    
}

-(void)animatebird{
    NSMutableArray *runFrames = [NSMutableArray array];
    
	
	for(int i = 1; i <= 12; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"purple%i.png", i]]];   
	}
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    id animate = [CCRepeatForever actionWithAction:forward];
 
    
    [[self getChildByTag:9] runAction:animate];
    
  
}

-(void)fastWind:(ccTime)dt
{
[[SimpleAudioEngine sharedEngine] playEffect:@"P5_1_waves_shore.mp3"];
}


-(void)animateman{
    NSMutableArray *runFrames = [NSMutableArray array];
	
	for(int i = 1; i <= 12; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"man%i.png", i]]];
	}

    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.2]];
    id animate = [CCSpeed actionWithAction:forward speed:2.4];
    
    [[self getChildByTag:14] runAction:animate];
    
   
    
   
}



-(void)arkMove{
    id kingmove = [CCSequence actions:[CCRotateBy actionWithDuration:3 angle: -20], [CCRotateBy actionWithDuration:3 angle: 20], nil];
    id mermaidmove = [CCSequence actions:[CCRotateBy actionWithDuration:3 angle: -20], [CCRotateBy actionWithDuration:3 angle: 20], nil];
    [[self getChildByTag:2] runAction:kingmove];
    [[self getChildByTag:3] runAction:mermaidmove];
}

-(void)popup:(id)item{
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id scaleflower = [CCSequence actions:scaleup, scaledown, nil];
    [item runAction:scaleflower];
    
}






// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:5])) {
//        self.scale = 2;
        
		// enable touches
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];

    [self moveCloud:0];
    [self moveCloud:12];
    [self birds];
    [self animateboat];
    [self waterripple];
    [self animatebird];
    [self eyeblink];
    
    [self schedule:@selector(fastWind:) interval:30.0f ];
    
}

-(void) tick: (ccTime) dt{
    [super tick:dt];
    if ([self getChildByTag:0].boundingBox.origin.x>=1024) {
        NSLog(@"0 added");
        CCSprite * background = [CCSprite spriteWithSpriteFrameName:@"cloud.png"];
        background.tag = 0;
        background.position = ccp([self getChildByTag:12].boundingBox.origin.x+30 - background.boundingBox.size.width/2, 614);
        [self removeChildByTag:0 cleanup:YES];
        [self addChild:background z:0];
        [self moveCloud:0];
    }
    if ([self getChildByTag:12].boundingBox.origin.x>=1024) {
        NSLog(@"12 added");
        CCSprite * background = [CCSprite spriteWithSpriteFrameName:@"cloud.png"];
        background.tag = 12;
        background.position = ccp([self getChildByTag:0].boundingBox.origin.x+30 - background.boundingBox.size.width/2, 614);
        [self removeChildByTag:12 cleanup:YES];
        [self addChild:background z:0];
        [self moveCloud:12];
    }

    
    
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super ccTouchesMoved:touches withEvent:event];
//    if (!touchedBody && !touchedSprite) {
//        [self getChildByTag:25].position = location;
//    }
}


-(void)eggSound
{
[[SimpleAudioEngine sharedEngine] playEffect:@"P5_4_egg_splat.mp3"];

}
-(void)animateEgg
{
    NSMutableArray *runFrames = [NSMutableArray array];
    
    id fadein = [CCFadeIn actionWithDuration:0];
    id gap = [CCActionInterval actionWithDuration:1.2];
    id fadeout = [CCFadeOut actionWithDuration:0];
    
    id fadeAction = [CCSequence actions:fadein,gap,fadeout,nil];
    
    
    for(int i = 1; i <= 12; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"egg%i.png", i]]];   
	}
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    
    id animate = [CCSpeed actionWithAction:forward speed:1.2];

    
	[[self getChildByTag:16] runAction:fadeAction];
    
    [[self getChildByTag:16] runAction:animate];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"P5_2_Birdsong.mp3"];
    
    [self performSelector:@selector(eggSound) withObject:nil afterDelay:0.7f];
    

}


- (void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node{
    [super tapped:recognizer node:node];
	if (touchedSprite) {
        
        if (touchedSprite.tag == 6) {
            if ([touchedSprite numberOfRunningActions]>0) {
                touchedSprite = NULL;
                return;
            }
            [self dolphinDive];
        }else if(touchedSprite.tag == 2){
            if(location.x>650&&location.x<760&&location.y>270&&location.y<350){
                return;
            }else if(location.x>550&&location.x<750&&location.y>100&&location.y<270){
                return;
                    }else{
                [self popFish];
                 
                    }
        }else if(touchedSprite.tag == 9){
            
            [self animateEgg];
            [self animateturtle];
        }else if(touchedSprite.tag == 14 || touchedSprite.tag == 3){
            [self animateman];
        }
        
        touchedBody = NULL;
        touchedSprite = NULL;
    }
}


- (void) dealloc
{

	[super dealloc];
}
@end
