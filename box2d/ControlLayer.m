//
//  ControlLayer.m
//  Tutorial1
//
//  Created by Alex Brown on 29/06/2011.
//  Copyright 2011 none. All rights reserved.
//

#import "ControlLayer.h"


@implementation ControlLayer
ControlLayer* _instance;
@synthesize jmpBtn;
@synthesize actBtn;
@synthesize taskBtn;
@synthesize joyStickPos;
@synthesize joyStickRotation;

+(id)control
{
    //returns an instance of the class using a method specified by the user,
    //(in this case initWithLayer). This overrides the standard init method
    return [[[self alloc] initWithLayer]autorelease];
}

-(id)initWithLayer
{
    if ((self = [super init]))
        {
            //calls the addJoystick method that creates the main joystick used
            [self addJoystick];
            
            [self addJBtn];
            [self addABtn];
            [self addTBtn];
            
            [self scheduleUpdate];
        }
		_instance = self;
        return self;
}

-(void)addJBtn{
    jBtn = [SneakyButton buttonWithRect:CGRectMake(0, 0, 50, 50)];
    jBtn.isHoldable =YES;
    jBtn.scale = 0.2;
    jBtnSkin = [SneakyButtonSkinnedBase skinnedButton];
    jBtnSkin.position = CGPointMake(980, 50);
    jBtnSkin.defaultSprite = [CCSprite spriteWithFile:@"redbutton.png"];
    jBtnSkin.defaultSprite.scale = 0.2;
    jBtnSkin.defaultSprite.opacity = 200;
    jBtnSkin.button = jBtn;
    [self addChild:jBtnSkin z:1];
    //        bu = YES;
}

-(void)addABtn{
    aBtn = [SneakyButton buttonWithRect:CGRectMake(0, 0, 50, 50)];
    aBtn.isHoldable =NO;
    aBtn.scale = 0.2;
    aBtnSkin = [SneakyButtonSkinnedBase skinnedButton];
    aBtnSkin.position = CGPointMake(920, 50);
    aBtnSkin.defaultSprite = [CCSprite spriteWithFile:@"redbutton.png"];
    aBtnSkin.defaultSprite.scale = 0.2;
    aBtnSkin.defaultSprite.color = ccBLUE;
    aBtnSkin.defaultSprite.opacity = 200;
    aBtnSkin.button = aBtn;
    [self addChild:aBtnSkin z:1];
    //        bu = YES;
}

-(void)addTBtn{
    tBtn = [SneakyButton buttonWithRect:CGRectMake(0, 0, 50, 50)];
    tBtn.isHoldable =NO;
    tBtn.scale = 0.2;
    tBtnSkin = [SneakyButtonSkinnedBase skinnedButton];
    tBtnSkin.position = CGPointMake(980, 110);
    tBtnSkin.defaultSprite = [CCSprite spriteWithFile:@"redbutton.png"];
    tBtnSkin.defaultSprite.scale = 0.2;
    tBtnSkin.defaultSprite.opacity = 200;
    tBtnSkin.defaultSprite.color = ccGREEN;
    tBtnSkin.button = tBtn;
    [self addChild:tBtnSkin z:1];
    //        bu = YES;
}

//Declares and creates the joystick used in our game
-(void)addJoystick
{
    //the stickRadius is simply the radius of the circle defined by the user
    float stickRadius = 50.0;

    //creates an instance of the SneakyJoystick class, via the initialisers declared in our
    //SneakyExtensions class
    joystick = [SneakyJoystick joystickWithRect:CGRectMake(0, 0, stickRadius, stickRadius)];
    //the deadradius is the area in which the joystick is non responsive. All analogue
    //joysticks have a deadzone, and therefore it feels more real to the user
    joystick.deadRadius = 5;
    //boolean value that is set to yes, meaning our joystick will have a 
    //deadzone (defined by the deadradius)
    joystick.hasDeadzone = YES;

    //Creates a 'skin' for our joystick. It is simply what we want the joystick to look like
    //calls an initialiser from the SneakyJoystickSkinnedBase class
    JSSkin = [SneakyJoystickSkinnedBase skinnedJoystick];
    //set the position of the joystick
    JSSkin.position = CGPointMake(stickRadius * 1.5f, stickRadius * 1.5f);
    //sets the main background (or larger) sprite of the joystick
    JSSkin.backgroundSprite = [CCSprite spriteWithFile:@"JoystickButton.png"];
    //set the thumb (or smaller) sprite of the joystick
    JSSkin.thumbSprite = [CCSprite spriteWithFile:@"JoystickButtonThumb.png"];
    //Defines which joystick our skinned joystick belongs to
    JSSkin.joystick = joystick;
    //adds the joystick (as a whole) to the layer
    [self addChild:JSSkin z:1];
}


//This method updates every frame and controls our movement
-(void)update:(ccTime)delta
{
	jmpBtn = jBtn.active;
	if (jBtn.active) {
//		jBtn.active = false;
	}
	actBtn = aBtn.active;
	if (aBtn.active) {
		aBtn.active = false;
	}
	
	taskBtn =  tBtn.active;
	
	joyStickPos = joystick.velocity;
	joyStickRotation = joystick.degrees;
	
}

+(ControlLayer*) getInstance{
	return _instance;
}


//apple recommends using super dealloc on the deallocation of every class
-(void)dealloc
{
    [super dealloc];
}

@end
