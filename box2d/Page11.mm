//
//  Page14.m
//  Little Mermaid
//
//  Created by Pipedream on 10/09/12.
//
//

#import "Page11.h"

@implementation Page11
NSMutableArray *ButterflyFrames;
int no_of_butterflies;

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page11 *layer = [Page11 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:11])) {
        no_of_butterflies=0;
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)removeitem:(id)sender data:(CCSprite*)item{
    [self removeChild:item cleanup:YES];
    no_of_butterflies--;
}

-(void)animateButterfly
{

    
    ButterflyFrames = [NSMutableArray array];
    
    for(int i = 1; i <= 30; i++) {
		[ButterflyFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"butterfly%i.png", i]]];
	}
    
   
    CCSprite *item;
    
    item = [[[CCSprite alloc] initWithSpriteFrameName:@"butterfly1.png"] autorelease];
     
    item.position = location;
    item.scale = 1;
    item.tag = 21;
    [interections addObject:item];
   
    [self addChild:item z:13];
    
       
    ccBezierConfig bezier1;
    bezier1.controlPoint_1 = ccp(item.position.x, item.position.y);
    bezier1.controlPoint_2 = ccp(item.position.x+300, item.position.y+200);
    
    bezier1.endPosition = ccp(item.position.x-50,item.position.y+300);
    
    id bezierAction1 = [CCBezierTo actionWithDuration:4 bezier:bezier1];
    

    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)item];
    [item runAction:[CCSequence actions:bezierAction1, remove, nil]];
  
  
  
    id forward = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:ButterflyFrames delay:0.1]];
    id animate_butterfly = [CCSpeed actionWithAction:[CCRepeat actionWithAction:forward times:20] speed:5];
  
    [item  runAction:[[animate_butterfly copy] autorelease]];
    
    
     no_of_butterflies++;
    
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page14_2_Butterfly.mp3"];
    

}


-(void)flowerFall{
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    int i = arc4random()%3 + 1;
    CCParticleSystemQuad *jevel = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    jevel.tag = 25;
    jevel.texture = [[CCSprite spriteWithFile:[NSString stringWithFormat:@"Jflower%i.png",i]] texture] ;
	jevel.emitterMode = kCCParticleModeGravity;
	jevel.duration=1.00;
	jevel.startColor = startColor;
	jevel.endColor = startColor;
    jevel.startColorVar = startColorVar;
	jevel.endColorVar = startColorVar;
	jevel.gravity = ccp(0,-10);
	jevel.autoRemoveOnFinish = YES;
	jevel.position = ccp(820, 750);
	jevel.posVar = ccp(200, 0);
	jevel.totalParticles = 10;
	jevel.emissionRate = 5;
	jevel.life = 70;
	jevel.lifeVar = 20;
	jevel.startSize = 20;
	jevel.startSizeVar = 10;
	jevel.endSize = 50;
	jevel.endSizeVar = 5;
    jevel.angle = 40;
    jevel.angleVar =20;
    
	[self addChild:jevel z:1];
}

-(void)flowerFall1{
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    int i = arc4random()%3 + 1;
    CCParticleSystemQuad *jevel = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    jevel.tag = 25;
    jevel.texture = [[CCSprite spriteWithFile:[NSString stringWithFormat:@"Jflower%i.png",i]] texture] ;
	jevel.emitterMode = kCCParticleModeGravity;
	jevel.duration=1.00;
	jevel.startColor = startColor;
	jevel.endColor = startColor;
    jevel.startColorVar = startColorVar;
	jevel.endColorVar = startColorVar;
	jevel.gravity = ccp(0,-10);
	jevel.autoRemoveOnFinish = YES;
	jevel.position = ccp(300, 650);
	jevel.posVar = ccp(300, 100);
	jevel.totalParticles = 10;
	jevel.emissionRate = 5;
	jevel.life = 70;
	jevel.lifeVar = 20;
	jevel.startSize = 20;
	jevel.startSizeVar = 10;
	jevel.endSize = 50;
	jevel.endSizeVar = 5;
    jevel.angle = 40;
    jevel.angleVar =20;
    
	[self addChild:jevel z:9];
}


-(void)changeColor:(CCSprite *)sprite
{
    id tint = [CCTintBy actionWithDuration:1 red:(arc4random()%127 + 127) green:(arc4random()%127 + 127) blue:(arc4random()%127 + 127)];

    [sprite runAction:tint];
    
    switch(arc4random()%3)
    {
        case 0:
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page14_3_Ladybug_v1.mp3"];
            break;
      
        case 1:
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page14_3_Ladybug_v2.mp3"];
            break;
        case 2:
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page14_3_ladybug_v3.mp3"];
            break;
            
    }
    
}

-(void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node
{
    
    
    [super tapped:recognizer node:node];
    if (touchedSprite) {
        switch(touchedSprite.tag){
            case 3:
            {
                [self flowerFall];
                [self flowerFall1];
                [[SimpleAudioEngine sharedEngine] playEffect:@"P3_2_pearl mussels.mp3"];
                break;
            }
                
            case 5:
            case 6:                
                [self changeColor:touchedSprite];
                break;
            
            case 9:
                 [[SimpleAudioEngine sharedEngine] playEffect:@"Page14_4_mermaidsigh.mp3"];
                break;
                
            case 21:
                [self animateButterfly];
                break;
                
                
            default:
                if(no_of_butterflies==0){
                    [self animateButterfly];
                }
        }
    }
    
    touchedSprite=NULL;
    touchedBody = NULL;
}


-(void)animateMermaidFace
{
    NSMutableArray *runFrames = [NSMutableArray array];
    
    for(int i = 1; i <= 8; i++) {
		[runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"facial%i.png", i]]];
	}
    
    
    id animateFace = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.2]];
    id actionRepeat = [CCRepeatForever actionWithAction:[CCSequence actions:animateFace,[animateFace reverse],nil]];
    [[self getChildByTag:9] runAction:actionRepeat];
    
    
}


-(void)animateSoldiers
{

    NSMutableArray *runFrames = [NSMutableArray array];
    
    for(int i = 1; i <= 12; i++) {
    [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"soldier%i.png", i]]];
	}
        
    id action = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.2]];
    id soldierMarch = [CCRepeatForever actionWithAction:action];
    
    for(int i=11;i<=16;i++)
    {
    [[self getChildByTag:i] runAction:[soldierMarch copy]];
    }
   

}


-(void)eyeblink{
        
    id blink = [[CCBlink actionWithDuration:1 blinks:1] reverse];
    id action =[CCRepeatForever actionWithAction:blink];
    [[[self getChildByTag:7] getChildByTag:11] runAction:action];
}

-(void)soldierMarchSound:(ccTime)dt
{
[[SimpleAudioEngine sharedEngine] playEffect:@"Page14_1_marching soldiers.mp3"];
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:1.5];
}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    [self animateMermaidFace];
    [self animateSoldiers];
    [self schedule:@selector(soldierMarchSound:) interval:5.0f ];
    [self eyeblink];
    
}


- (void) dealloc
{
    
    ButterflyFrames = NULL;
	[super dealloc];
}


@end
