//
//  Page22.m
//  Little Mermaid
//
//  Created by Pipedream on 27/09/12.
//
//

#import "Page17.h"

@implementation Page17

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page17 *layer = [Page17 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:17])) {
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}

-(void)birds{
    
    
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    
    CCParticleSnow *bird1 = [[[CCParticleSnow alloc]init] autorelease];
	bird1.texture = [[CCSprite spriteWithFile:@"bird1.png"] texture] ;
	bird1.autoRemoveOnFinish = YES;
	bird1.position = ccp(500, 380);
	bird1.posVar = ccp(400, 0);
	bird1.totalParticles = 5;
	bird1.emissionRate = 2;
	bird1.life = 10;
	bird1.lifeVar = 5;
	bird1.startSize = 20;
	bird1.startSizeVar = 5;
	bird1.endSize = 30;
	bird1.endSizeVar = 5;
    bird1.startColor = startColor;
    bird1.startColorVar = startColorVar;
    bird1.endColor = startColor;
    bird1.endColorVar = startColorVar;
    bird1.gravity = ccp(2, 5);
	[self addChild:bird1 z:1];
    
    CCParticleSnow *bird2 = [[[CCParticleSnow alloc]init] autorelease];
	bird2.texture = [[CCSprite spriteWithFile:@"bird2.png"] texture] ;
	bird2.autoRemoveOnFinish = YES;
	bird2.position = ccp(400, 480);
	bird2.posVar = ccp(400, 0);
	bird2.totalParticles = 5;
	bird2.emissionRate = 2;
	bird2.life = 20;
	bird2.lifeVar = 5;
	bird2.startSize = 30;
	bird2.startSizeVar = 5;
    
    bird2.startColor = startColor;
    bird2.startColorVar = startColorVar;
    bird2.endColor = startColor;
    bird2.endColorVar = startColorVar;
	bird2.endSize = 0;
	bird2.endSizeVar = 5;
    bird2.gravity = ccp(-2, 5);
	[self addChild:bird2 z:1];
    
//    CCParticleSnow *bird3 = [[[CCParticleSnow alloc]init] autorelease];
//	bird3.texture = [[CCSprite spriteWithFile:@"bird3.png"] texture] ;
//	bird3.autoRemoveOnFinish = YES;
//	bird3.position = ccp(400, 580);
//	bird3.posVar = ccp(400, 0);
//	bird3.totalParticles = 5;
//	bird3.emissionRate = 2;
//	bird3.life = 20;
//	bird3.lifeVar = 5;
//	bird3.startSize = 10;
//	bird3.startSizeVar = 5;
//    
//	bird3.endSize = 30;
//	bird3.endSizeVar = 5;
//    
//    bird3.startColor = startColor;
//    bird3.startColorVar = startColorVar;
//    bird3.endColor = startColor;
//    bird3.endColorVar = startColorVar;
//    bird3.gravity = ccp(2, 3);
//	[self addChild:bird3 z:1];
}

-(void)tears{
    CCParticleSnow *tear = [[[CCParticleSnow alloc]init] autorelease];
    tear.texture = [[CCSprite spriteWithFile:@"teardrop.png"] texture];
    tear.autoRemoveOnFinish = YES;
    tear.position = ccp(531, 300);
    tear.posVar = ccp(40,20);
    tear.totalParticles = 5;
    tear.emissionRate = 0.5;
    tear.life = 7;
    tear.lifeVar = 1;
    tear.startSize = 10;
    tear.startSizeVar = 1;
    tear.gravity = ccp(0,-10);
    tear.endSize = 10;
    tear.endSizeVar = 1;
    [self addChild:tear z:10];
    
    CCParticleSnow *tear1 = [[[CCParticleSnow alloc]init] autorelease];
    tear1.duration = 5;
    tear1.texture = [[CCSprite spriteWithFile:@"teardrop.png"] texture];
    tear1.autoRemoveOnFinish = YES;
    tear1.position = ccp(600, 250);
    tear1.posVar = ccp(60,20);
    tear1.totalParticles = 2;
    tear1.emissionRate = 0.3;
    tear1.life = 7;
    tear1.lifeVar = 1;
    tear1.startSize = 10;
    tear1.startSizeVar = 1;
    tear1.gravity = ccp(0,-10);
    tear1.endSize = 10;
    tear1.endSizeVar = 1;
    [self addChild:tear1 z:10];
}


-(void)playFlyingSound
{
 [[SimpleAudioEngine sharedEngine] playEffect:@"Page20_1_Fairy_flying_v1.mp3"];

}

-(void)animateFairies
{
    
    NSMutableArray *runFrames1 = [NSMutableArray array];
    for(int i = 1; i <= 5; i++) {
        [runFrames1 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"fairyLeft%i.png", i]]];
    }
    
    id fairy1Animate = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames1 delay:0.3]];
   
        
    NSMutableArray *runFrames2 = [NSMutableArray array];
    
    for(int i = 1; i <= 6; i++) {
        [runFrames2 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"fairyRyt%i.png", i]]];
    }
    
    id fairy2Animate = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames2 delay:0.3]];
    [[self getChildByTag:5] runAction:[CCRepeatForever actionWithAction:fairy1Animate]];
    [[self getChildByTag:6] runAction:[CCRepeatForever actionWithAction:fairy2Animate]];
    
//    id gap2 = [CCActionInterval actionWithDuration:7];
//    id FlySound = [CCCallFunc actionWithTarget:self selector:@selector(playFlyingSound)];
//    id repeatFlySound = [CCRepeatForever actionWithAction:[CCSequence actions:gap2,FlySound,nil]] ;
//    
//    [[self getChildByTag:6] runAction:repeatFlySound];
    
}


-(void)starShow{
    ccColor4F startColor;
    startColor.r = 1.0f;
    startColor.g = 1.0f;
    startColor.b = 1.0f;
    startColor.a = 1.0f;
    ccColor4F startColorVar;
    startColorVar.r = 0.0f;
    startColorVar.g = 0.0f;
    startColorVar.b = 0.0f;
    startColorVar.a = 0.0f;
    
    CCParticleSystemQuad *jevel = [[[CCParticleSystemQuad alloc]initWithTotalParticles:1000] autorelease];
    jevel.tag = 25;
    jevel.texture = [[CCSprite spriteWithFile:[NSString stringWithFormat:@"sparkle.png"]] texture] ;
	jevel.emitterMode = kCCParticleModeGravity;
	jevel.duration=kCCParticleDurationInfinity;
	jevel.startColor = startColor;
	jevel.endColor = startColor;
    jevel.startColorVar = startColorVar;
	jevel.endColorVar = startColorVar;
	jevel.gravity = ccp(0,0);
	jevel.autoRemoveOnFinish = YES;
	jevel.position = ccp(512, 384);
	jevel.posVar = ccp(500, 380);
	jevel.totalParticles = 100;
	jevel.emissionRate = 50;
	jevel.life = 5;
	jevel.lifeVar = 1;
	jevel.startSize = 10;
	jevel.startSizeVar = 20;
	jevel.endSize = 0;
	jevel.endSizeVar = 5;
    jevel.angle = 40;
    jevel.angleVar =20;
    
	[self addChild:jevel z:0];
       
}


-(void)animatePurpleBird
{
    
    NSMutableArray *runFrames = [NSMutableArray array];
    
        
    for(int i = 1; i <= 5; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"purple%i.png", i]]];
	}
    
    id wings_move1 = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    id wings_move2 = [wings_move1 reverse];
    
    id birdFly = [CCSpeed actionWithAction:[CCRepeatForever actionWithAction:[CCSequence actions:wings_move1,wings_move2,nil] ] speed:2];
    
    
    [[self getChildByTag:7] runAction:birdFly];
    
    
    
}

-(void)moveBird:(ccTime)dt
{
    
   
    id gap = [CCActionInterval actionWithDuration:4];
    
       
    
    CGPoint pos1 = ccp(-100.00,arc4random()%700+20);
    
//    CGPoint pos2 = ccp(arc4random()%1024,arc4random()%768);
   
    CGPoint pos3 = ccp(1100.00,arc4random()%700+20);
    
    id flipBird1 = [CCFlipX actionWithFlipX:TRUE];
    id flipBird2 = [CCFlipX actionWithFlipX:FALSE];
   
          
    id move1=[CCMoveTo actionWithDuration:4 position:pos1];
    
//    id move2=[CCMoveTo actionWithDuration:4 position:pos2];
   
    id move3=[CCMoveTo actionWithDuration:4 position:pos3];
    
    
    
    [[self getChildByTag:7] runAction:[CCSequence actions:gap,move1,flipBird1,move3,flipBird2,nil]];
  
     
    
      
}



-(void)groundDisappear
{
    
    CCSprite *groundSprite = (CCSprite *)[self getChildByTag:3];
       
    CGPoint groundPosition = CGPointMake(groundSprite.position.x, groundSprite.position.y);
    
    id ground_set = [CCMoveTo actionWithDuration:8 position:ccp(groundPosition.x,groundPosition.y-350)];

    [groundSprite runAction:ground_set];
    [[self getChildByTag:7] runAction:[[ground_set copy] autorelease]];
    
    [self starShow];
    
//    [self schedule: @selector(starShow:)];
}

-(void)flashy{
    id tint1 = [CCTintBy actionWithDuration:1 red:-40 green:-40 blue:-40];
    id tint2 = [CCTintBy actionWithDuration:1 red:-40 green:-40 blue:-40];
    id tint3 = [CCTintBy actionWithDuration:1 red:-40 green:-40 blue:-40];
    
    [[self getChildByTag:0] runAction:[CCRepeatForever actionWithAction:[CCSequence actions:tint1, tint2, tint3, [tint3 reverse],[tint2 reverse],[tint1 reverse], nil]]];
    
    id tint4 = [CCTintBy actionWithDuration:1 red:-40 green:-40 blue:-40];
    id tint5 = [CCTintBy actionWithDuration:1 red:-40 green:-40 blue:-40];
    id tint6 = [CCTintBy actionWithDuration:1 red:-40 green:-40 blue:-40];
    [[self getChildByTag:1] runAction:[CCRepeatForever actionWithAction:[CCSequence actions:tint4, tint5, tint6, [tint6 reverse],[tint5 reverse],[tint4 reverse], nil]]];
    
    id tint7 = [CCTintBy actionWithDuration:1 red:-30 green:-30 blue:-30];
    id tint8 = [CCTintBy actionWithDuration:1 red:-30 green:-30 blue:-30];
    id tint9 = [CCTintBy actionWithDuration:1 red:-30 green:-30 blue:-30];
    
    [[self getChildByTag:2] runAction:[CCRepeatForever actionWithAction:[CCSequence actions:tint7, tint8, tint9, [tint9 reverse],[tint8 reverse],[tint7 reverse], nil]]];
    

}


-(void)moveSky:(int)tag{
    id move = [CCMoveBy actionWithDuration:4 position:ccp(0, -50)];
    [[self getChildByTag:tag] runAction:[CCRepeatForever actionWithAction:move]];
}



-(void)bgSoundPlayFunc
{
    switch(arc4random()%2){
        case 0:
       [[SimpleAudioEngine sharedEngine] playEffect:@"Page20_2_StarTwinkling.mp3"];
            break;
        case 1:
            [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_3_fairyflying_low.mp3"];
            break;
    }
}


-(void)bgSoundFunc:(ccTime)dt
{
    id gap2 = [CCActionInterval actionWithDuration:((arc4random()%10)+5)];
    id bgSoundAction = [CCCallFunc actionWithTarget:self selector:@selector(bgSoundPlayFunc)];
    id bgSoundPlay = [CCRepeatForever actionWithAction:[CCSequence actions:gap2 ,bgSoundAction ,nil]] ;
    
    [self runAction:bgSoundPlay];
   
}


-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    [self animateFairies];
    [self animatePurpleBird];
    [self groundDisappear];
    [self flashy];
    [self moveSky:0];
    [self moveSky:1];
    [self birds];
    [self tears];
    
   
   [self schedule:@selector(bgSoundFunc:) interval:15.0f ];
    
    [self schedule:@selector(moveBird:) interval:10.0f ];
    
    
    }


-(void)tick:(ccTime)dt{
    [super tick:dt];
    if (parallax!=NULL) {
        [parallax updateWithVelocity:ccp(0,4) AndDelta:dt];
    }
    if ([self getChildByTag:0].position.y<-400) {
        [self getChildByTag:0].position = ccp(512, 1160);
        [self moveSky:0];
    }
    if ([self getChildByTag:1].position.y<-400) {
        [self getChildByTag:1].position = ccp(512, 1160);
        [self moveSky:1];
    }
}



-(void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node
{

    [super tapped:recognizer node:node];
    if(touchedSprite){
    if(touchedSprite.tag==0 || touchedSprite.tag==1)
    {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page20_2_StarTwinkling.mp3"];
    
    }
    else if(touchedSprite.tag==7)
    {   
        id pink_scaledown = [CCScaleTo actionWithDuration:0.05 scale:0.7];
        id pink_scaleup = [CCScaleTo actionWithDuration:0.05 scale:1];
        id birdAction = [CCSequence actions:pink_scaledown,pink_scaleup,nil];
        
        [[self getChildByTag:7] runAction:birdAction];
        
        [[SimpleAudioEngine sharedEngine] playEffect:@"Page20_3_Birds.mp3"];
        
        }
        
    }
    
    touchedSprite=NULL;
    touchedBody = NULL;
}



- (void) dealloc
{
	[super dealloc];
}


@end
