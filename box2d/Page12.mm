//
//  Page15.m
//  Little Mermaid
//
//  Created by Pipedream on 11/09/12.
//
//

#import "Page12.h"

@implementation Page12

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page12 *layer = [Page12 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:12])) {
        
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}


-(void)animateCape
{

    NSMutableArray *capeFrames = [NSMutableArray array];
    
    for(int i = 1; i<= 13; i++) {
        
		[capeFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"cape%i.png", i]]];
	}
    
    id forward= [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:capeFrames delay:0.1]];
    
    id backward = [forward reverse];
    
    id animate_cape = [CCRepeatForever actionWithAction:[CCSequence actions:forward,backward,nil]] ;
    
    [[self getChildByTag:7]  runAction:animate_cape ];

}


-(void)smoke{
	
	CCParticleSmoke *leaf_Particles = [[[CCParticleSmoke alloc]init] autorelease] ;
	leaf_Particles.texture = [[CCSprite spriteWithFile:@"fire.png"] texture] ;
	leaf_Particles.autoRemoveOnFinish = YES;
	leaf_Particles.position = ccp(448, 550);
	leaf_Particles.posVar = ccp(20, 5);
	leaf_Particles.totalParticles = 100;
	leaf_Particles.emissionRate = 15;
    ccColor4F strtcolor = {10,10,10,0};
    leaf_Particles.startColor = strtcolor;
	leaf_Particles.life = 2;
	leaf_Particles.lifeVar = 0;
	leaf_Particles.startSize = 0;
	leaf_Particles.startSizeVar = 0;
	leaf_Particles.endSize = 10;
	leaf_Particles.endSizeVar = 0;
    leaf_Particles.gravity = ccp(0,0.5);
    [leaf_Particles setBlendFunc:(ccBlendFunc) {GL_ONE_MINUS_SRC_ALPHA, GL_ONE}];
	[self addChild:leaf_Particles];
    
}


-(void)makeChicken{
    [self smoke];
    id hide = [CCFadeOut actionWithDuration:1];
    id show = [CCFadeIn actionWithDuration:1];
    id scale = [CCScaleBy actionWithDuration:0.1 scale:1.1];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)[self getChildByTag:4]];
    [[self getChildByTag:4] runAction:hide];
    [[self getChildByTag:13] runAction:[CCSequence actions:show,remove, scale, [scale reverse], nil]];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"Page15_1_Poof.mp3"];
}

-(void)animateCap{
    id up = [CCMoveBy actionWithDuration:0.2 position:ccp(0, 150)];
    id gap = [CCActionInterval actionWithDuration:2];
    id pop = [CCCallFunc actionWithTarget:self selector:@selector(popAnimal)];
    [[self getChildByTag:10] runAction:[CCSequence actions:up,pop, gap, [up reverse], nil]];
    [[SimpleAudioEngine sharedEngine] playEffect:@"Page15_3_Hatopens.mp3"];
   }


-(void)popAnimal{
    short sequence=rand()%3;
    NSString *Objects[3] = {@"mouse.png",@"Racoon.png",@"Squirrel.png"};
    CCSprite * random_sprite = [CCSprite spriteWithSpriteFrameName:Objects[sequence]];
    
    random_sprite.position=ccp(817, 257);
    random_sprite.scale = 1;
    
    [self addChild:random_sprite z:7];
    
    id scaleup = [CCScaleTo actionWithDuration:1 scale:1.2];
    id scaledown = [CCScaleTo actionWithDuration:0.1 scale:1];
    id gap = [CCActionInterval actionWithDuration:1];
    id hide = [CCScaleTo actionWithDuration:0.2 scale:0];
    id remove = [CCCallFuncND actionWithTarget:self selector:@selector(removeitem:data:) data:(CCSprite*)random_sprite];
    
    id pop_sprite = [CCSequence actions:scaleup, scaledown, gap, hide, remove, nil];
    [random_sprite runAction:pop_sprite];
}



-(void)spillcake
{
       
    for(int i=1;i<50;i++)
    {
        CCSprite * cakeSpill = [CCSprite spriteWithSpriteFrameName:@"cloud.png"];
        
                      
        cakeSpill.position = CGPointMake(arc4random()%1024,arc4random()%768);
        
        cakeSpill.tag=(0-i);
        [self addChild:cakeSpill z:(30+i)];
               
        [interections addObject:cakeSpill];
    
    }
  
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page15_2_Cakesplash.mp3"];

}

-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
    [self animateCape];
}

- (void) dealloc
{
	[super dealloc];
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super ccTouchesBegan:touches withEvent:event];
    if(touchedSprite)
    {
        if (touchedSprite.tag<0&&touchedSprite.tag>-52) {
            
           [[SimpleAudioEngine sharedEngine] playEffect:@"Remove_cream.mp3"];
        }
    }

}
-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super ccTouchesMoved:touches withEvent:event];
    if (touchedSprite) {
        if (touchedSprite.tag<0&&touchedSprite.tag>-52) {
            
            touchedSprite.position = location;
        }
      
    }
    
    
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    CCSprite *touchedPiece = touchedSprite;
   
    if (touchedPiece) {
        if (touchedPiece.tag<0&&touchedSprite.tag>-52) {
            
            [interections removeObject:touchedPiece];
            [self removeChild:touchedPiece cleanup:YES];
            
        }
        touchedPiece=NULL;
    }
    
     [super ccTouchesEnded:touches withEvent:event];
    
}

-(void)tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node
{
    [super tapped:recognizer node:node];
    if (touchedSprite) {
        if(touchedSprite.tag==4 || touchedSprite.tag==3)
        {
            [self makeChicken];
        }else if(touchedSprite.tag==9 || touchedSprite.tag ==10)
        {
            if ([[self getChildByTag:10] numberOfRunningActions]<=0) {
                [[SimpleAudioEngine sharedEngine] playEffect:@"Page15_1_Poof.mp3"];
                [self animateCap];
            }
            
        }
        else if (touchedSprite.tag>13 && touchedSprite.tag<=22) {
            [self removeChild:touchedSprite cleanup:YES];
        }
        else if (touchedSprite.tag==11) {
            [self spillcake];
        }
        else if (touchedSprite.tag<0&&touchedSprite.tag>-52) {
            [interections removeObject:touchedSprite];
            [self removeChild:touchedSprite cleanup:YES];
        }

        
        
    }
    
    touchedSprite=NULL;
    touchedBody = NULL;
    
}


@end
