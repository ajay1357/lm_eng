//
//  Page16.m
//  Little Mermaid
//
//  Created by Pipedream on 11/09/12.
//
//

#import "Page13.h"
#import "GlobalSet.h"


@implementation Page13
CCScene *scene;
CCSprite *prev;
CCSprite *next;
CGPoint flowerBoardPosition;
int flowerTag,flowerCount;
CCSprite *board;

BOOL activityRunning;


+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	Page13 *layer = [Page13 node];
    
	// add layer as a child to scene
	[scene addChild: layer];
	// return the scene
    
    
    
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithPage:13])) {
        flowerCount = 0;
        flowerTag = 0;
        activityRunning=YES;
		// enable touches  ---- Done in baseLevel.h
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        
	}
	return self;
}


-(void)animateSweeper
{
    
    NSMutableArray *runFrames = [NSMutableArray array];
    
    for(int i = 2; i <= 24; i++) {
        [runFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"Sweeper%i.png", i]]];
	}
    
    id action = [CCAnimate actionWithAnimation: [CCAnimation animationWithSpriteFrames:runFrames delay:0.1]];
    id sweep = [CCRepeatForever actionWithAction:action];
    
    [[self getChildByTag:29] runAction:sweep];
    
}

    
-(void)glowChandellar
{
    id fadein=[CCFadeIn actionWithDuration:0.6];
    id fadeout=[CCFadeOut actionWithDuration:0.3];
    
    id glow = [CCSequence actions:fadein,fadeout,nil];
    
    [[self getChildByTag:31] runAction:[CCRepeatForever actionWithAction:glow]];

}



- (void) clickedAtPoint: (CGPoint) point
{
    float newScale = 1.5;
    
    
    
    CGFloat deltaX = (point.x - self.anchorPoint.x * self.contentSize.width) * (newScale - self.scale);
    CGFloat deltaY = (point.y - self.anchorPoint.y * self.contentSize.height) * (newScale - self.scale);
    CGPoint position = ccp( - deltaX, - deltaY);
    id action = [CCSpawn actions: 
                 [CCScaleBy actionWithDuration:1 scale:newScale], 
                 [CCMoveBy actionWithDuration:1 position:position], 
                 nil];
    
    id rev_action = [action reverse];
    
    if([self numberOfRunningActions] == 0){
    [self runAction:[CCSequence actions:action,rev_action,nil]];


    }
       
}



-(void)darken{
    
//    
//    [self getChildByTag:0].visible=FALSE;
//    [self getChildByTag:29].visible=FALSE;
//    [self getChildByTag:2].visible=FALSE;
//    [self getChildByTag:32].visible=FALSE;
    
    
    id tint = [CCTintTo actionWithDuration:0 red:10 green:10 blue:10];
    [[self getChildByTag:0] runAction:tint];
    [[self getChildByTag:29] runAction:[[tint copy] autorelease]];
    [[self getChildByTag:2] runAction: [[tint copy] autorelease]];
    [[self getChildByTag:32] runAction:[[tint copy] autorelease]];
  
    
    
}

-(void)show{
    
//    
//    [self getChildByTag:0].visible=TRUE;
//    [self getChildByTag:29].visible=TRUE;
//    [self getChildByTag:2].visible=TRUE;
//    [self getChildByTag:32].visible=TRUE;
    
    id tint = [CCTintTo actionWithDuration:3 red:255 green:255 blue:255];
    [[self getChildByTag:0] runAction:tint];
    [[self getChildByTag:29] runAction:[[tint copy] autorelease]];
    [[self getChildByTag:2] runAction:[[tint copy] autorelease]];
    [[self getChildByTag:32] runAction:[[tint copy] autorelease]];
    
     [[SimpleAudioEngine sharedEngine] playEffect:@"Page16_4_Chandelier.mp3"];
}



-(void)launchActivity
{
//    [self addLabels];
    
    board = [CCSprite spriteWithSpriteFrameName:@"board.png"];
    board.position = ccp(600,500);
    board.tag = 55;
    [interections addObject:board];
    [self addChild:board z:11];
    
    
   NSMutableArray *aPoints = [NSMutableArray array];
    
    for(int i = 0; i <4 ; i++)
    {
        CGPoint p = ccp(440+(i*100),580);
        [aPoints addObject:[NSValue valueWithCGPoint:p]];
        
    }
    
    for(int i = 0; i <4 ; i++)
    {
        CGPoint p = ccp(440 + (i*100),510);
        [aPoints addObject:[NSValue valueWithCGPoint:p]];
        
    }
    for(int i = 0; i <4 ; i++)
    {
        CGPoint p = ccp(440 + (i*100),440);
        [aPoints addObject:[NSValue valueWithCGPoint:p]];
        
    }
    
//    x = 396 to 756  y = 401  to 590
    
    for(int i =0 ; i <(arc4random()%10 + 5);i++)
    {
        int j = arc4random()%12;
        
        CGPoint p = [[aPoints objectAtIndex:j] CGPointValue];
        
        [aPoints removeObjectAtIndex:j];
        
       [aPoints addObject:[NSValue valueWithCGPoint:p]];
    
    }
    
    
    
    int j=60;
    for(int i = 0; i <4 ; i++)
    {
        CCSprite *flower = [CCSprite spriteWithSpriteFrameName:@"redflower.png"];
        //flower.position = ccp(440+(i*100),580);
        flower.position = [[aPoints objectAtIndex:i] CGPointValue];
        flower.tag = j++;
        [interections addObject:flower];
        [self addChild:flower z:12];
        
    }
    for(int i = 4; i <8 ; i++)
    {
        CCSprite *flower = [CCSprite spriteWithSpriteFrameName:@"yellowflower.png"];
//        flower.position = ccp(440 + (i*100),510);
        flower.position = [[aPoints objectAtIndex:i] CGPointValue];
        flower.tag = j++;
        [interections addObject:flower];
        [self addChild:flower z:12];
        
    }
    for(int i = 8; i <12 ; i++)
    {
        CCSprite *flower = [CCSprite spriteWithSpriteFrameName:@"pinkflower.png"];
//        flower.position = ccp(440 + (i*100),440);
        flower.position = [[aPoints objectAtIndex:i] CGPointValue];
        flower.tag = j++;
        [interections addObject:flower];
        [self addChild:flower z:12];
        
    }
    
}

-(void)activityDone
{
    activityRunning=NO;
    
    [interections removeObject:board];
    [self removeChild:board cleanup:YES];
    
    id fadout = [CCFadeIn actionWithDuration:0];
    
    for(int i=7;i<=14;i++)  //Show Curtains
    {
        [[self getChildByTag:i] runAction:[[fadout copy] autorelease]];
        [interections addObject:[self getChildByTag:i]];
    }
    
    
    [[self getChildByTag:1] runAction:[[fadout copy] autorelease]];//Chandelier
    [[self getChildByTag:31] runAction:[[fadout copy] autorelease]];
    [interections addObject:[self getChildByTag:1]];
    
    [self animateSweeper];
    [self glowChandellar];
    
   GlobalSet * global = [GlobalSet globalSettings];
    if(global.TEXT){
        [self performSelector:@selector(textMoveIn) withObject:nil afterDelay:0.8f];
    }
    
    if (global.NARRATEME){
    id gap = [CCActionInterval actionWithDuration:2];
    id play = [CCCallFuncN actionWithTarget:self selector:@selector(NarrateMe)];
    [self runAction:[CCSequence actions:gap, play, nil]];
    }
    [self removeChild:[self getChildByTag:80] cleanup:YES];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"Page21_6_Fluxglitter.mp3"];
    
}



-(void)addLabels
{

    CCLabelTTF *RED = [CCLabelTTF labelWithString:@"RED" fontName:@"Doctor-Soos-Bold" fontSize:20 ];
    RED.position = ccp(520,225);
    RED.color = ccWHITE;
    RED.rotation=10.0;
    RED.visible=YES;
    
    
    CCLabelTTF *YELLOW = [CCLabelTTF labelWithString:@"YELLOW" fontName:@"Doctor-Soos-Bold" fontSize:20];
    YELLOW.position = ccp(740,200);
    YELLOW.color = ccWHITE;
    YELLOW.rotation=8.0;
    YELLOW.visible=YES;
    
    
    CCLabelTTF *PURPLE = [CCLabelTTF labelWithString:@"PURPLE" fontName:@"Doctor-Soos-Bold" fontSize:20];
    PURPLE.position = ccp(960,157);
    PURPLE.color = ccWHITE;
    PURPLE.rotation=12.0;
    PURPLE.visible=YES;
    
    
    GlobalSet *global3;
    global3 = [GlobalSet globalSettings];
    
    CCLabelTTF *Message;
    
 //   CCLabelTTF *Message=[CCLabelTTF labelWithString:@"Drag the colored flowers in their correct pots" fontName:@"Doctor-Soos-Bold" fontSize:20];
    
  //    CCLabelTTF *Message=[CCLabelTTF labelWithString:@"Drag the colored flowers in their correct pots" fontName:@"NevisonCasD" fontSize:30];
    
    switch(global3.language){
        case 1: ///English
            Message = [CCLabelTTF labelWithString:@"Drag the colored flowers in their correct pots" fontName:@"NevisonCasD" fontSize:40];
            break;
            
        case 2: //Dansk
            Message = [CCLabelTTF labelWithString:@"Træk de farvede planter hen til de korrekte potter" fontName:@"NevisonCasD" fontSize:40];
            break;
            
        case 3://Svensk
            Message = [CCLabelTTF labelWithString:@"Dra de färgade växterna till rätt blomkruka" fontName:@"NevisonCasD" fontSize:40];
            break;
            
        case 4://NORSK
            Message = [CCLabelTTF labelWithString:@"Trekk de fargede plantene bort til de riktige krukkene" fontName:@"NevisonCasD" fontSize:40];
            break;
         
        default:
            Message = [CCLabelTTF labelWithString:@"Drag the colored flowers in their correct pots" fontName:@"NevisonCasD" fontSize:40  ];
            break;
            
    }
    
    Message.position = ccp(606,731);
    Message.color = ccWHITE;
    Message.visible=YES;
    Message.tag=80;

    
    [self addChild:RED z:43];
    [self addChild:YELLOW z:43];
    [self addChild:PURPLE z:43];
    [self addChild:Message z:43];

}




-(void)onEnterTransitionDidFinish{
    [super onEnterTransitionDidFinish];
  
    [self darken];
    
    prev = [CCSprite spriteWithFile:@"arrow1.png"];
    prev.position = ccp(50, 30);
    prev.flipX = YES;
    prev.scale = 0.7;
    [scene addChild:prev z:100];
    prev.tag = PRE;
    [interections addObject:prev];
    
    
    next = [CCSprite spriteWithTexture:prev.texture];
    next = [CCSprite spriteWithFile:@"arrow1.png"];
    next.position = ccp(980, 30);
    next.scale = 0.7;
    [scene addChild:next z:100];
    [interections addObject:next];
    next.tag = NEXT;
    
    
    [self addLabels];
    [self launchActivity];
    [interections removeObject:[self getChildByTag:REPLAY]];
    
}




-(void) tick: (ccTime) dt{
    
    if(!activityRunning){
    [super tick:dt];
    
    BOOL destroyleft = NO;
    BOOL destroyright = NO;
    
    
    if ([self getChildByTag:18].position.x > 760 && [self getChildByTag:17].position.x > 760 && [self getChildByTag:16].position.x > 760) {
        destroyright = YES;
    }
    
    if ([self getChildByTag:19].position.x < 250 && [self getChildByTag:20].position.x < 250 && [self getChildByTag:21].position.x < 250) {
        destroyleft = YES;
    }
    
    
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			TRBox2D *item = (TRBox2D*)b->GetUserData();
                        
            if (((item.tag >=7 && item.tag <=10) || (item.tag >=15 && item.tag <=18)) && destroyright) {
                if (mouseJoint)
                {
                    world->DestroyJoint(mouseJoint);
                    mouseJoint = NULL;
                }
                NSInteger ind = [self.interections indexOfObject:(CCSprite *)[item body]->GetUserData()];
                if (ind != NSNotFound) {
                    [self.interections removeObject:(CCSprite *)[item body]->GetUserData()];
                }
                
                [item destroyBodyInWorld:world];
               
                [self removeChild:item cleanup:YES];
                
            }else
            if (((item.tag >=11 && item.tag <=14) || (item.tag >=19 && item.tag <=22)) && destroyleft) {
                if (mouseJoint)
                {
                    world->DestroyJoint(mouseJoint);
                    mouseJoint = NULL;
                } 
                NSInteger ind = [self.interections indexOfObject:(CCSprite *)[item body]->GetUserData()];
                if (ind != NSNotFound) {
                    [self.interections removeObject:(CCSprite *)[item body]->GetUserData()];
                }
                [item destroyBodyInWorld:world];
                [self removeChild:item cleanup:YES];
            }
            
		}	
      }
    
    }
    
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super ccTouchesBegan:touches withEvent:event];
    if (CGRectContainsPoint([prev boundingBox], location)) {
        [self sceneChange:prev];
    }else if(CGRectContainsPoint([next boundingBox], location)) {
        [self sceneChange:next];
        
    }
    
    if(activityRunning)
    {
        if (touchedSprite.tag>=60 && touchedSprite.tag<72)
        {
            flowerBoardPosition = touchedSprite.position;
            flowerTag = touchedSprite.tag;
            
        }
        
    }
    
}



-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
     [super ccTouchesMoved:touches withEvent:event];
    
    if(activityRunning)
    {
        
        if (touchedSprite.tag>=60 && touchedSprite.tag<72) {
            touchedSprite.position = location;
            [self getChildByTag:100].position = location;
        }
    }

   
}



-(void)verifyFlowerPosition
{
    
    CCSprite *touchedFlower = touchedSprite;
    CCSprite *FlowerPot;
    switch(touchedFlower.tag)
    {
            
        case 60:case 61:case 62: case 63:   //RED Flower
            FlowerPot = (CCSprite *)[self getChildByTag:5];
            break;
            
            
        case 64:case 65:case 66:case 67:    //Yellow flower
            FlowerPot = (CCSprite *)[self getChildByTag:6];
            break;
            
        case 68: case 69: case 70: case 71:  //Blue flower
            FlowerPot = (CCSprite *)[self getChildByTag:28];
            break;
            
    }
    
    if(CGRectContainsPoint([FlowerPot boundingBox], touchedFlower.position))
    {
        touchedFlower.position = ccp(location.x,FlowerPot.position.y + abs(location.y - FlowerPot.position.y));
        [interections removeObject:touchedFlower];
        flowerCount++;
    }else{
        touchedFlower.position = flowerBoardPosition;
        [[SimpleAudioEngine sharedEngine] playEffect:@"P7_4_Hidden pictures appears.mp3"];
        
    }
    
    
    if(flowerCount==12){
        
        [self activityDone];
        [interections addObject:[self getChildByTag:REPLAY]];
    }
    
    
}



-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
     
    
    if(activityRunning)
    {
        if(touchedSprite.tag>=60 && touchedSprite.tag<72)
        {
            [self verifyFlowerPosition];
            
        }
        
    }else if(touchedBody)
    {
        //        [[SimpleAudioEngine sharedEngine] playEffect:@"Page16_1_curtain.mp3"];
        [[SimpleAudioEngine sharedEngine] playEffect:@"P7_4_Hidden pictures appears.mp3"];
        
    }
    
    [super ccTouchesEnded:touches withEvent:event];
   
    
}





-(void) tapped:(UITapGestureRecognizer *)recognizer node:(CCNode *)node
{
    [super tapped:recognizer node:node];
    
   
    if (touchedSprite)
    {
        if (touchedSprite.tag == 1) {
            [self show];
        }else if(touchedSprite.tag == 29){
        [[SimpleAudioEngine sharedEngine] playEffect:@"Sweeeping.mp3"];
        }
        else if(!activityRunning){
            [self clickedAtPoint:location];
        }
    }
    touchedSprite=NULL;
    touchedBody=NULL;
}

        
        

- (void) dealloc
{
    
	[super dealloc];
}



@end
